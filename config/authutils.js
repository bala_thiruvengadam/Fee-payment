const dbUtil = require('../config/dbUtil');
const $q = require('q');
const cache = require('../cache/cache');
var _authConfig;
var authUtil = function() {
    var that = this;
    this.fetchRole = function(role) {
        console.log("role : "+role);
        var $promise = $q.defer();
        if (!_authConfig) {
            cache.get('roles.' + role, function(err, value) {
                if (value) {
                    _authConfig = value;
                    $promise.resolve(_authConfig);
                }
            });
        }
        if (!_authConfig) {
            dbUtil.getConnection(function(db) {
                var coll = db.collection('roles');
                coll.findOne({
                    role: role,
                }, function(err, config) {
                    if (err) {
                        console.log("Severe : Failed to fetch config files !", err);
                        $promise.reject("Failed to fetch the config files !");
                    } else {
                        _authConfig = config;
                        cache.set('roles.' + role, config, function(err, success) {
                            if (err) {
                                console.log("failed to set cache");
                            }
                        });
                        $promise.resolve(_authConfig);
                    }
                });
            });
        } else {
            $promise.resolve(_authConfig);
        }
        return $promise.promise;
    };
    this.serviceAllowed = function(role, service) {
        console.log('service/called = ',role , service);
        var servicePromise = $q.defer();
        that.fetchRole(role).then((roleData) => {
            console.log("RoleData ", roleData);
            if (roleData) {
                config.log("RoleData : ",roleData);
                if (roleData.services[service] && roleData.services[service] == 1) {
                    console.log('service found');
                    servicePromise.resolve(true);
                } else {
                    servicePromise.reject(false);
                }
            } else {
                servicePromise.reject(false);
            }
        }, (reject) => {
            servicePromise.reject(false);
        });
       return servicePromise.promise;
    };
};
module.exports = new authUtil();
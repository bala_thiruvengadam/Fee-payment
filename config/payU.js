const dbUtil = require('../config/dbUtil');
const $q = require('q');
var _pgConfig;
module.exports.payUConfig = function() {
    var $promise = $q.defer();
    if (!_pgConfig) {
    	dbUtil.getConnection(function(db) {
        	var coll = db.collection('config');
            coll.findOne({
                type: 'PG',
                name: 'PAYU'
            }, function(err, config) {
            	if (err) {
                    console.log("Severe : Failed to fetch config files !",err);
                    $promise.reject("Failed to fetch the config files !");
                } else {
                    _pgConfig = config;
                	$promise.resolve(_pgConfig);
                }
            });
        });
    } else {
        $promise.resolve(_pgConfig);
    }
    return $promise.promise;
};

var dbUtil = require('dbUtil');

module.exports = {
	save: function(collection, obj) {
		var savePromise = new Promise(function(resolve, reject) {
			dbUtil.getConnection(function(db) {
				if (!db) {
					reject("Unable to connect to database .. ");
				}
				var collection = db[collection];
				collection.insertOne(obj, function(err, result) {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			});
		});
		return savePromise;
	},
	saveMulti: function(collection, obj) {
		var saveMultiPromise = new Promise(function(resolve, reject) {
			if (instanceof obj != "Array") {
				reject("Data must be an instance of array");
			}
			dbUtil.getConnection(function(db) {
				if (!db) {
					reject("Unable to connect to database .. ");
				}
				var collection = db[collection];
				collection.insert(obj, function(err, result) {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});


			});
		});
		return saveMultiPromise;

	},
	update: function(collection, queryObj, obj, options) {
		var updatePromise = new Promise(function(resolve, reject) {
			dbUtil.getConnection(function(db) {
				if (!db) {
					reject("Unable to connect to database .. ");
				}
				var collection = db[collection];
				collection.update(queryObj, {
					$set: obj
				}, options, function(err, result) {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			});
		});
		return updatePromise;
	},
	findOne: function(collection, queryObj, options) {
		var updateMultiPromise = new Promise(function(resolve, reject) {
			dbUtil.getConnection(function(db) {
				if (!db) {
					reject("Unable to connect to database .. ");
				}
				var collection = db[collection];
				collection.findOne(queryObj, options, function(err, result) {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			});
		});
		return updateMultiPromise;
	},
	find: function(collection, queryObj, options) {
		var findMultiPromise = new Promise(function(resolve, reject) {
			
			dbUtil.getConnection(function(db) {
				if (!db) {
					reject("Unable to connect to database .. ");
				}
				var collection = db[collection];
				collection.find(queryObj, options).toArray(function(err, result) {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});


			});
		});
		return findMultiPromise;
	}

}
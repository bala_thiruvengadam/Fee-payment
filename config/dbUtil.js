var MongoClient = require('mongodb').MongoClient;
var config = require("./config");
var q = require('q');
var __ = require('lodash');

var db;

module.exports.getConnection = function(callback) {
    if (!db) {
        MongoClient.connect(config.mongoUrl, function(err, database) {
            if (err) {
                throw err;
            }
            console.log("Created database connection in db connection" + database);
            db = database;
            callback(db);
        });
    } else {
        console.log("Else block of get connection");
        callback(db);
    }
}
module.exports.getConn = function() {
    var connPromise = new Promise(function(resolve, reject) {
        if (!db) {
            MongoClient.connect(config.mongoUrl, function(err, database) {
                if (err) {
                    reject(err);
                }
                console.log("Created database connection in db connection (using promise)" + database);
                db = database;
                resolve(db);
            });
        } else {
            resolve(db);
        }
    });
    return connPromise;
};

module.exports.getTenantCollection = function(tenant,collectionName){

    return new Promise(function(resolve, reject) {
        if(__.isEmpty(tenant) || __.isEmpty(collectionName)){
            reject("Either Tenant name is empty or collection name is empty");
        }

        if (!db) {
            MongoClient.connect(config.mongoUrl, function(err, database) {
                if (err) {
                    reject(err);
                }
                console.log("Created database connection in db connection (using promise)" + database);
                db = database;
                resolve(db);
            });
        } else {
            resolve(db);
        }
    }).then((db)=>{
        return new Promise((resolve,reject)=>{
            var collection = db.collection(tenant+"_"+collectionName);
            resolve(collection);
        });
    },(err)=>{
        return err;
    });
};

module.exports.getCollection = function(collectionName){
    return new Promise(function(resolve, reject) {
        if(__.isEmpty(collectionName))
            reject("collection can't be empty");

        if (!db) {
            MongoClient.connect(config.mongoUrl, function(err, database) {
                if (err) {
                    reject(err);
                }
                console.log("Created database connection in db connection (using promise)" + database);
                db = database;
                resolve(db);
            });
        } else {
            resolve(db);
        }
    }).then((db)=>{
        return new Promise((resolve,reject)=>{
            var collection = db.collection(collectionName);
            resolve(collection);
        });
    },(err)=>{
        return err;
    });  
};
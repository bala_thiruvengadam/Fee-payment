var crypto = require('crypto');
const $q = require('q');

var mobiPay = function() {
    
    var genarateHMAC=function(toHash, secretKey){
       var hmac = crypto.createHmac('sha256', secretKey)
       hmac.update(toHash);
       return hmac.digest('hex');
    };

    var blankIfNull = function(value) {
        return value || "";
    };
    

    this.createHMAC = function(amount, buyerEmail, buyerFirstName, buyerPhoneNumber, currency, merchantIdentifier,
         orderId, secretKey,returnUrl, txnType, extra1, extra2, extra3, extra4, extra5) {
           
        var toHash = null;
        toHash = "amount="+ amount + "&buyerEmail=" + buyerEmail + "&buyerFirstName=" + buyerFirstName + 
        "&buyerPhoneNumber=" + buyerPhoneNumber + "&currency=" + currency + 
        "&merchantIdentifier=" + merchantIdentifier + "&orderId=" + orderId + 
        "&returnUrl=" + returnUrl + "&txnType=" + txnType + "&extra1=" + extra1 + "&extra2=" + extra2 + 
        "&extra3=" + extra3 + "&extra4=" + extra4 + "&extra5=" + extra5 + "&"
        
        console.log("toHash:  ", toHash)
        var value=genarateHMAC(toHash, secretKey);
        return value;
    };

    this.getAddionalCharges = function(data, mobiConfig, payment_mode) {
        var amount = data.amount;
        var add_charges = mobiConfig.additional_charges[payment_mode];
        var add_charges_str = "";

        if(add_charges.type == 'PERCENT')
            add_charges_str = add_charges.value + '%'
        else if(add_charges.type == 'FLAT')   
            add_charges_str = add_charges.value + '' 

        return add_charges_str;
    }

};
module.exports = new mobiPay();
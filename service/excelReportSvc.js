var dbUtil = require('../config/dbUtil');
var ctx = require('../config/ctxconfig');
var $q = require('q');
const pug = require('pug');
var ObjectID = require('mongodb').ObjectID;
var async = require('async');
var _ = require('lodash');

var ExcelReportSvc = function() {
        this.getRegReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Year"  + "\t" + "Unique Id"  + "\t" 
            + "Category" + "\t" + "Caste" + "\t" +  "Original resident of" + "\t" + "Board" + "\t" + "Email" + "\t" + "Sex" + "\t" 
            + "College Id" + "\t" + "Merit List No" + "\t"
            + "merit Serial No" + "\t" + "Registration No" + "\t" + "Merit Date" + "\t" + "Class" + "\t" + "Subject For MA" + "\t" + "Subject BA 1" + "\t" + "Subject BA 2"
            + "\t" + "Subject BA 3" + "\t" + "Aadhar No" + "\t" + "Candidate Name" + "\t" + "Father Name" + "\t" + "Father Occupation" + "\t" + "Mother Name" + "\t" 
            + "Mother Occupation" + "\t" + "Mobile No" + "\t" + "Date Of Birth" + "\t" + "Nationality" + "\t" + "Religion" + "\t"  
            + "Other Resident" + "\t" + "Hobbies Game Name" + "\t" + "Hobbies Cultural Activity" 
            + "\t" + "Other Board" + "\t" + "Address" + "\t" + "Parent/Guardian's Mobile No." + "\t" + "Student Mobile No." + "\t"  
            +"Other Weightage" + "\t" + "High School Year" 
            + "\t" + "High School Roll No." + "\t" + "High School School/Board" + "\t" + "High School Subject" + "\t" + "High School Marks Obtained" + "\t" 
            + "High School Max marks" + "\t" + "High School Percentage" + "\t" +"Intermediate Year" + "\t" + "Intermediate Roll No." 
            + "\t" + "Intermediate School/Board" + "\t" + "Intermediate Subject" + "\t" + "Intermediate Marks Obtained" + "\t" +  "Intermediate Max marks" + "\t" 
            + "Intermediate Percentage" + "\t" +"BA Year" + "\t" + "BA Roll No." + "\t" + "BA School/Board" + "\t" + "BA Subject" 
            + "\t" + "BA Marks Obtained" + "\t" + "BA Max marks" + "\t" + "BA Percentage" + "\t" +"BCom Year" + "\t" + "BCom Roll No." 
            + "\t" + "BCom School/Board" + "\t" + "BCom Subject" + "\t" + "BCom Marks Obtained" + "\t" + "BCom Max marks" + "\t" 
            + "BCom Percentage" + "\t" +"MA Year" + "\t" + "MA Roll No." + "\t" + "MA School/Board" + "\t" + "MA Subject" + "\t" + "MA Marks Obtained" + "\t" 
            + "MA Max marks" + "\t" + "MA Percentage" + "\t" +"MCom Year" + "\t" + "MCom Roll No." + "\t" + "MCom School/Board" 
            + "\t" + "MCom Subject" + "\t" + "MCom Marks Obtained" + "\t" + "MCom Max marks" + "\t" + "MCom Percentage" + "\t" 
            +"LLB Year" + "\t" + "LLB Roll No." + "\t" + "LLB School/Board" + "\t" + "LLB Subject" + "\t" + "LLB Marks Obtained" + "\t" + "LLB Max marks" + "\t" 
            + "LLB Percentage" + "\t" +"B.Ed./M.Ed. Year" + "\t" + "B.Ed./M.Ed. Roll No." + "\t" + "B.Ed./M.Ed. School/Board" + "\t" 
            + "B.Ed./M.Ed. Subject" + "\t" + "B.Ed./M.Ed. Marks Obtained" + "\t" + "B.Ed./M.Ed. Max marks" + "\t" 
            + "B.Ed./M.Ed. Percentage" + "\t" +"Other Year" + "\t" + "Other Roll No." + "\t" + "Other School/Board" + "\t" + "Other Subject" + "\t" 
            + "Other Marks Obtained" + "\t" + "Other Max marks" + "\t" + "Other Percentage" + "\t" +"In Service" + "\t" 
            + "Graduate From" + "\t" + "Spouse/Son/Daughter or Permanent employee " + "\t" 
            + "Rovers Rangers" + "\t" + "Scouts/Guides" + "\t" + "NSS" + "\t" + "NCC" + "\t" +"Caste category" + "\t" + "Sports Participant" + "\t"

            + "High School Marksheet Attached" + "\t" + "Intermediate Marksheet Attached" + "\t" + "Graduation Marksheet Attached" + "\t" 
            + "Category Marksheet Attached" + "\t" + "Character Marksheet Attached" + "\t" + "Transfer Marksheet Attached" + "\t" 
            + "Weightage Marksheet Attached" + "\t" + "Income Marksheet Attached" + "\t" + "Aadhar Card Attached" + "\t" + "Other Attachment" + "\t"
            
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.adm_year || " " ) + '\t' + 
                (data.uniqueId || " " ) + '\t' + (data.category || " " ) + '\t' + (data.caste || " " ) + '\t' + 
                (data.orginal_res_of || " " )+ '\t' + 
                (data.board || " " ) + '\t' + (data.email || " " ) + '\t' + 
                (data.sex || " " ) + '\t' + (data.college_id || " " ) + '\t' +	(data.merit_list_no || " " ) + '\t' + (data.merit_serial_no || " " ) + '\t' + 
                (data.en_no || " " ) + '\t' + (data.meritDate || " " ) + '\t' + (data.semester || " " ) + '\t' + (data.subject_for_ma || " " ) + '\t' + 
                (data.subject_ba_1 || " " ) + '\t' + (data.subject_ba_2 || " " ) + '\t' + (data.subject_ba_3 || " " ) + '\t' + 
                (data.aadhar_no || " " )+ '\t' + (data.name || " " ) + '\t' + (data.father_name || " " ) + '\t' + (data.father_occ || " " ) + '\t' + 
                (data.mother_name || " " ) + '\t' + (data.mother_occ || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.dob || " " ) + '\t' + 
                (data.nationality || " " ) + '\t' + (data.religion || " " ) + '\t' + (data.other_resident || " " )+ '\t' + 
                (data.hobbies_game_name || " " ) + '\t' + 
                (data.hobbies_cultural_activity || " " ) + '\t' + (data.other_board || " ") + '\t' + (data.address || " " ) + '\t' + 
                (data.addr_phone || " " ) + '\t' + (data.addr_mob || " " ) + '\t' + (data.other_weightage || " " ) + '\t' + 
                (data.hs_year || " " ) + '\t' + (data.hs_sno || " ") + '\t' + (data.hs_school || " " ) + '\t' + (data.hs_subject || " " ) + '\t' + 
                (data.hs_marks_obtained || " " )  + '\t' + (data.hs_marks || " " ) + '\t' + (data.hs_percentage || " " ) + '\t' + 
                (data.inter_year || " " ) + '\t' + (data.inter_sno || " " ) + '\t' + (data.inter_school || " " ) + '\t' + (data.inter_subject || " " ) + '\t' + 
                (data.inter_marks_obtained || " " ) + '\t' + (data.inter_marks || " " ) + '\t' + 
                (data.inter_percentage || " " ) + '\t' + (data.ba_year || " " ) + '\t' + (data.ba_sno || " " ) + '\t' + (data.ba_school || " " ) + '\t' + 
                (data.ba_subject || " " ) + '\t' + (data.ba_marks_obtained || " " ) + '\t' + (data.ba_marks || " " ) + '\t' + 
                (data.ba_percentage || " " ) + '\t' + (data.bcom_year || " " ) + '\t' + (data.bcom_sno || " " ) + '\t' + (data.bcom_school || " " ) + '\t' + 
                (data.bcom_subject || " " ) + '\t' + (data.bcom_marks_obtained || " " ) + '\t' + (data.bcom_marks || " " ) + '\t' +
                (data.bcom_percentage || " " ) + '\t' + (data.ma_year || " " ) + '\t' + (data.ma_sno || " " ) + '\t' + (data.ma_school || " " ) + '\t' + 
                (data.ma_subject || " " ) + '\t' + (data.ma_marks_obtained || " " ) + '\t' + (data.ma_marks || " " ) + '\t' + 
                (data.ma_percentage || " " ) + '\t' + (data.mcom_year  || " ") + '\t' + (data.mcom_sno || " " ) + '\t' + (data.mcom_school || " " ) + '\t' + 
                (data.mcom_subject || " " ) + '\t' + (data.mcom_marks_obtained || " " ) + '\t' + (data.mcom_marks || " " ) + '\t' + 
                (data.mcom_percentage || " " ) + '\t' + (data.llb_year || " " ) + '\t' + (data.llb_sno || " " ) + '\t' + (data.llb_school || " " ) + '\t' + 
                (data.llb_subject || " " ) + '\t' + (data.llb_marks_obtained || " ") + '\t' +	(data.llb_marks || " ") + '\t' + 
                (data.llb_percentage || " " ) + '\t' + (data.bed_year || " " ) + '\t' + (data.bed_sno || " " ) + '\t' + (data.bed_school || " " ) + '\t' + 
                (data.bed_subject || " " ) + '\t' + (data.bed_marks_obtained || " " ) + '\t' + (data.bed_marks || " " ) + '\t' +
                (data.bed_percentage || " " )  + '\t' +	(data.others_year || " " )  + '\t' + (data.others_sno || " " ) + '\t' + (data.others_school || " " ) + '\t' + 
                (data.others_subject || " " ) + '\t' + (data.others_marks_obtained || " " ) + '\t' + 
                (data.others_marks || " " ) + '\t' + (data.others_percentage || " " ) + '\t' + (data.inService || " " ) + '\t' + (data.grad_from || " " )  + '\t' +
                (data.perm_emp || " " )  + '\t' +
                (data.rovers || " " )  + '\t' +	(data.scouts || " " )  + '\t' + (data.nss || " " ) + '\t' + (data.ncc || " " ) + '\t' +
                (data.caste_category || " " )  + '\t' + (data.sports_participant || " " ) + '\t' + 

                (data.schoolMarkSheet || " " )  + '\t' + (data.interMarkSheet || " " ) + '\t' + 
                (data.gradMarkSheet || " " )  + '\t' + (data.categoryMarkSheet || " " ) + '\t' + 
                (data.characterMarkSheet || " " )  + '\t' + (data.transferMarkSheet || " " ) + '\t' + 
                (data.weightageMarkSheet || " " )  + '\t' + (data.incomeMarkSheet || " " ) + '\t' + 
                (data.aadhar_card || " " )  + '\t' + (data.otherAttachment || " " ) + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.kmggpgcGetRegReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Year"  + "\t" + "Unique Id"  + "\t" + "Category" + "\t" + "Original resident of" + "\t" + "Board" + "\t" + "Email" + "\t" + "Sex" + "\t" + "College Id" + "\t" + "Merit List No" + "\t"
            + "merit Serial No" + "\t" + "Registration No" + "\t" + "Merit Date" + "\t" + "Class" + "\t" + "Subject For MA" + "\t" + "Subject BA 1" + "\t" + "Subject BA 2"
            + "\t" + "Subject BA 3" + "\t" + "Aadhar No" + "\t" + "Candidate Name" + "\t" + "Father Name" + "\t" + "Father Occupation" + "\t"
            + "Father Income" + "\t" + "income Certificate" + "\t" + "Mother Name" + "\t" 
            + "Mother Occupation" + "\t" + "Mobile No" + "\t" + "Date Of Birth" + "\t" + "Nationality" + "\t" + "Religion" + "\t"  
            + "Other Resident" + "\t" + "Hobbies Game Name" + "\t" + "Hobbies Cultural Activity" 
            + "\t" + "Other Board" + "\t" + "Address" + "\t" + "Parent/Guardian's Mobile No." + "\t" + "Student Mobile No." + "\t"  
            +"Other Weightage" + "\t" + "High School Year" 
            + "\t" + "High School Roll No." + "\t" + "High School School/Board" + "\t" + "High School Subject" + "\t" + "High School Marks Obtained" + "\t" 
            + "High School Max marks" + "\t" + "High School Percentage" + "\t" +"Intermediate Year" + "\t" + "Intermediate Roll No." 
            + "\t" + "Intermediate School/Board" + "\t" + "Intermediate Subject" + "\t" + "Intermediate Marks Obtained" + "\t" +  "Intermediate Max marks" + "\t" 
            + "Intermediate Percentage" + "\t" 

            +"B.A./B.Sc./B.Com I Year" + "\t" + "B.A./B.Sc./B.Com I Roll No." + "\t" 
            + "B.A./B.Sc./B.Com I School/Board" + "\t" + "B.A./B.Sc./B.Com I Subject"  + "\t" + "B.A./B.Sc./B.Com I Marks Obtained" 
            + "\t" + "B.A./B.Sc./B.Com I Max marks" + "\t" + "B.A./B.Sc./B.Com I Percentage" 
            
            + "\t" +"B.A./B.Sc./B.Com II Year" + "\t" + "B.A./B.Sc./B.Com II Roll No."  + "\t" + "B.A./B.Sc./B.Com II School/Board" 
            + "\t" + "B.A./B.Sc./B.Com II Subject" + "\t" + "B.A./B.Sc./B.Com II Marks Obtained" + "\t" + "B.A./B.Sc./B.Com II Max marks" + "\t" 
            + "B.A./B.Sc./B.Com II Percentage" + "\t"
            
            + "\t" +"B.A./B.Sc./B.Com III Year" + "\t" + "B.A./B.Sc./B.Com III Roll No."  + "\t" + "B.A./B.Sc./B.Com III School/Board" 
            + "\t" + "B.A./B.Sc./B.Com III Subject" + "\t" + "B.A./B.Sc./B.Com III Marks Obtained" + "\t" + "B.A./B.Sc./B.Com III Max marks" + "\t" 
            + "B.A./B.Sc./B.Com III Percentage" + "\t"
            
            +"M.A./M.Sc./M.Com I Year" + "\t" + "M.A./M.Sc./M.Com I Roll No." + "\t" + "M.A./M.Sc./M.Com I School/Board" + "\t" 
            + "M.A./M.Sc./M.Com I Subject" + "\t" + "M.A./M.Sc./M.Com I Marks Obtained" + "\t" + "M.A./M.Sc./M.Com I Max marks" + "\t" 
            + "M.A./M.Sc./M.Com I Percentage" + "\t" 

            +"M.A./M.Sc./M.Com II Year" + "\t" + "M.A./M.Sc./M.Com II Roll No." + "\t" + "M.A./M.Sc./M.Com II School/Board" + "\t" 
            + "M.A./M.Sc./M.Com II Subject" + "\t" + "M.A./M.Sc./M.Com II Marks Obtained" + "\t" + "M.A./M.Sc./M.Com II Max marks" + "\t" 
            + "M.A./M.Sc./M.Com II Percentage" + "\t"

            +"M.A./M.Sc./M.Com III Year" + "\t" + "M.A./M.Sc./M.Com III Roll No." + "\t" + "M.A./M.Sc./M.Com III School/Board" + "\t" 
            + "M.A./M.Sc./M.Com III Subject" + "\t" + "M.A./M.Sc./M.Com III Marks Obtained" + "\t" + "M.A./M.Sc./M.Com III Max marks" + "\t" 
            + "M.A./M.Sc./M.Com III Percentage" + "\t"
            
            +"B.Ed./M.Ed. Year" + "\t" + "B.Ed./M.Ed. Roll No." + "\t" + "B.Ed./M.Ed. School/Board" + "\t" 
            + "B.Ed./M.Ed. Subject" + "\t" + "B.Ed./M.Ed. Marks Obtained" + "\t" + "B.Ed./M.Ed. Max marks" + "\t" 
            + "B.Ed./M.Ed. Percentage" + "\t" 
            
            +"In Service" + "\t" + "Graduate From" + "\t" + "Spouse/Son/Daughter or Permanent employee " + "\t" 
            + "Rovers Rangers" + "\t" + "Scouts/Guides" + "\t" + "NSS" + "\t" + "NCC" + "\t" +"Caste category" + "\t" + "Sports Participant" + "\t"

            + "High School Marksheet Attached" + "\t" + "Intermediate Marksheet Attached" + "\t" + "Graduation Marksheet Attached" + "\t" 
            + "Category Marksheet Attached" + "\t" + "Character Marksheet Attached" + "\t" + "Transfer Marksheet Attached" + "\t" 
            + "Weightage Marksheet Attached" + "\t" + "Income Marksheet Attached" + "\t" + "Aadhar Card Attached" + "\t" + "Other Attachment" + "\t"
            
            + "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.adm_year || " " ) + '\t' + (data.uniqueId || " " ) + '\t' + (data.category || " " ) + '\t' + (data.orginal_res_of || " " )+ '\t' + 
                (data.board || " " ) + '\t' + (data.email || " " ) + '\t' + 
                (data.sex || " " ) + '\t' + (data.college_id || " " ) + '\t' +	(data.merit_list_no || " " ) + '\t' + (data.merit_serial_no || " " ) + '\t' + 
                (data.en_no || " " ) + '\t' + (data.meritDate || " " ) + '\t' + (data.semester || " " ) + '\t' + (data.subject_for_ma || " " ) + '\t' + 
                (data.subject_ba_1 || " " ) + '\t' + (data.subject_ba_2 || " " ) + '\t' + (data.subject_ba_3 || " " ) + '\t' + 
                (data.aadhar_no || " " )+ '\t' + (data.name || " " ) + '\t' + (data.father_name || " " ) + '\t' + (data.father_occ || " " ) + '\t' + 
                (data.father_income || " " ) + '\t' + (data.income_cert || " " ) + '\t' +
                (data.mother_name || " " ) + '\t' + (data.mother_occ || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.dob || " " ) + '\t' + 
                (data.nationality || " " ) + '\t' + (data.religion || " " ) + '\t' + (data.other_resident || " " )+ '\t' + 
                (data.hobbies_game_name || " " ) + '\t' + 
                (data.hobbies_cultural_activity || " " ) + '\t' + (data.other_board || " ") + '\t' + (data.address || " " ) + '\t' + 
                (data.addr_phone || " " ) + '\t' + (data.addr_mob || " " ) + '\t' + (data.other_weightage || " " ) + '\t' + 
                (data.hs_year || " " ) + '\t' + (data.hs_sno || " ") + '\t' + (data.hs_school || " " ) + '\t' + (data.hs_subject || " " ) + '\t' + 
                (data.hs_marks_obtained || " " )  + '\t' + (data.hs_marks || " " ) + '\t' + (data.hs_percentage || " " ) + '\t' + 
                (data.inter_year || " " ) + '\t' + (data.inter_sno || " " ) + '\t' + (data.inter_school || " " ) + '\t' + (data.inter_subject || " " ) + '\t' + 
                (data.inter_marks_obtained || " " ) + '\t' + (data.inter_marks || " " ) + '\t' + 
                (data.inter_percentage || " " ) + '\t' + 
                
                (data.b1_year || " " ) + '\t' + (data.b1_sno || " " ) + '\t' + (data.b1_school || " " ) + '\t' + 
                (data.b1_subject || " " ) + '\t' + (data.b1_marks_obtained || " " ) + '\t' + (data.b1_marks || " " ) + '\t' + 
                (data.b1_percentage || " " ) + '\t' + 
                
                (data.b2_year || " " ) + '\t' + (data.b2_sno || " " ) + '\t' + (data.b2_school || " " ) + '\t' + 
                (data.b2_subject || " " ) + '\t' + (data.b2_marks_obtained || " " ) + '\t' + (data.b2_marks || " " ) + '\t' + 
                (data.b2_percentage || " " ) + '\t' +  

                (data.b3_year || " " ) + '\t' + (data.b3_sno || " " ) + '\t' + (data.b3_school || " " ) + '\t' + 
                (data.b3_subject || " " ) + '\t' + (data.b3_marks_obtained || " " ) + '\t' + (data.b3_marks || " " ) + '\t' + 
                (data.b3_percentage || " " ) + '\t' +  
                
                (data.m1_year || " " ) + '\t' + (data.m1_sno || " " ) + '\t' + (data.m1_school || " " ) + '\t' + 
                (data.m1_subject || " " ) + '\t' + (data.m1_marks_obtained || " " ) + '\t' + (data.m1_marks || " " ) + '\t' + 
                (data.m1_percentage || " " ) + '\t' + 

                (data.m2_year || " " ) + '\t' + (data.m2_sno || " " ) + '\t' + (data.m2_school || " " ) + '\t' + 
                (data.m2_subject || " " ) + '\t' + (data.m2_marks_obtained || " " ) + '\t' + (data.m2_marks || " " ) + '\t' + 
                (data.m2_percentage || " " ) + '\t' + 

                (data.m3_year || " " ) + '\t' + (data.m3_sno || " " ) + '\t' + (data.m3_school || " " ) + '\t' + 
                (data.m3_subject || " " ) + '\t' + (data.m3_marks_obtained || " " ) + '\t' + (data.m3_marks || " " ) + '\t' + 
                (data.m3_percentage || " " ) + '\t' + 
                
                (data.bed_year || " " ) + '\t' + (data.bed_sno || " " ) + '\t' + (data.bed_school || " " ) + '\t' + 
                (data.bed_subject || " " ) + '\t' + (data.bed_marks_obtained || " " ) + '\t' + (data.bed_marks || " " ) + '\t' +
                (data.bed_percentage || " " )  + '\t' +	
                
                (data.inService || " " ) + '\t' + (data.grad_from || " " )  + '\t' +
                (data.perm_emp || " " )  + '\t' + (data.rovers || " " )  + '\t' +	(data.scouts || " " )  + '\t' + (data.nss || " " ) + '\t' + 
                (data.ncc || " " ) + '\t' + (data.caste_category || " " )  + '\t' + (data.sports_participant || " " ) + '\t' + 

                (data.schoolMarkSheet || " " )  + '\t' + (data.interMarkSheet || " " ) + '\t' + 
                (data.gradMarkSheet || " " )  + '\t' + (data.categoryMarkSheet || " " ) + '\t' + 
                (data.characterMarkSheet || " " )  + '\t' + (data.transferMarkSheet || " " ) + '\t' + 
                (data.weightageMarkSheet || " " )  + '\t' + (data.incomeMarkSheet || " " ) + '\t' + 
                (data.aadhar_card || " " )  + '\t' + (data.otherAttachment || " " ) + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.getAdmReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Course" + "\t"  + "Class" + "\t" + "Aadhar No" + "\t" + "Name" + "\t" + "Email" + "\t" + "Gender" + "\t" 
            + "Date Of Birth" + "\t" + "Mobile No." + "\t" + "Father Name" + "\t" + "Mother Name" + "\t"
            + "Guardian Mobile" + "\t" + "Address" + "\t" + "Pin" + "\t" + "Jain Minority Status" + "\t" + "Resident Of" + "\t" 
            + "Caste Category"  + "\t" + "Religion" + "\t" + "PH"  + "\t" 
            + "Intermediate Subject Stream" + "\t" + "Father In Defence" + "\t" + "Staff Ward" + "\t" + "Sports Quota" + "\t" + "NCC" + "\t" + "NSS" + "\t" 
            + "Scouts" + "\t" + "Rovers Rangers" + "\t" + "Matriculation Board" + "\t" + "Matriculation Year" + "\t" + "Matriculation Roll" + "\t" + "Matriculation Max Marks" + "\t" 
            + "Matriculation Marks Obtained" + "\t" + "Matriculation Remarks" + "\t" + "Intermediate Board" + "\t" + "Intermediate Year" + "\t" + "Intermediate Roll" 
            + "\t" + "Intermediate Max Marks" + "\t" + "Intermediate Marks Obtained" + "\t" + "Intermediate Remarks" + "\t" 
            + "Graduation Board" + "\t" + "Graduation Year" + "\t"  +"Graduation Roll" + "\t" + "Graduation Max Marks" 
            + "\t" + "Graduation Marks Obtained" + "\t" + "Graduation Remarks" + "\t" 
            + "Post Graduation Board" + "\t" + "Post Graduation Year" + "\t"  +"Post Graduation Roll" + "\t" + "Post Graduation Max Marks" 
            + "\t" + "Post Graduation Marks Obtained" + "\t" + "Post Graduation Remarks" + "\t" 
            + "M.Ed. Board" + "\t" + "M.Ed. Year" + "\t"  +"M.Ed. Roll" + "\t" + "M.Ed. Max Marks" 
            + "\t" + "M.Ed. Marks Obtained" + "\t" + "M.Ed. Remarks" + "\t" 
            + "Other Board" + "\t" + "Other Year" + "\t" + "Other Roll" + "\t" + "Other Max Marks" + "\t" + "Other Marks Obtained" 
            + "\t" +"Other Remarks" + "\t" 

            + "Year1 theory marks" + "\t" + "Year1 practical marks" + "\t" 
            + "Year1 theory marks obtained" + "\t" + "Year1 practical marks obtained" + "\t" + "Year1 remarks"
            
            + "Year2 theory marks" + "\t" + "Year2 practical marks" + "\t" 
            + "Year2 theory marks obtained" + "\t" + "Year2 practical marks obtained" + "\t" + "Year2 remarks"

            + "Year3 theory marks" + "\t" + "Year3 practical marks" + "\t" 
            + "Year3 theory marks obtained" + "\t" + "Year3 practical marks obtained" + "\t" + "Year3 remarks"

            + "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXN ID" + "\t" + "Payment Date(DD-MM-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.course || " " ) + '\t' + (data.stu_class || " " ) + '\t' + (data.aadhar_no || " " )+ '\t' + 
                (data.name || " " ) + '\t' + (data.email || " " ) + '\t' + (data.gender || " " ) + '\t' + 
                (data.dob || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.father_name || " " ) + '\t' +	(data.mother_name || " " ) + '\t' + (data.guardian_mobile || " " ) + '\t' + 
                (data.address || " " ) + '\t' + (data.pin || " " ) + '\t' + (data.category || " " ) + '\t' + (data.res_of || " " ) + '\t' + 
                (data.caste_category || " " ) + '\t' + (data.sub_category || " " ) + '\t' + (data.ph || " " ) + '\t' + (data.sub_stream || " " ) + '\t' + 
                (data.father_in_defence || " " )+ '\t' + (data.staff_ward || " " ) + '\t' + (data.sports_quota || " " ) + '\t' + (data.ncc || " " ) + '\t' + 
                (data.nss || " " ) + '\t' + (data.scouts || " " ) + '\t' + (data.rovers || " " ) + '\t' + (data.mat_board || " " ) + '\t' + 
                (data.mat_year || " " ) + '\t' + (data.mat_roll || " " ) + '\t' + (data.mat_max_marks || " " ) + '\t' + (data.mat_marks_obtained || " " )+ '\t' + 
                (data.mat_remarks || " " ) + '\t' + (data.inter_board || " " ) + '\t' + (data.inter_year || " " ) + '\t' + 
                (data.inter_roll || " " ) + '\t' + (data.inter_max_marks || " ") + '\t' + (data.inter_marks_obtained || " " ) + '\t' + 
                (data.inter_remarks || " " )  + '\t' + (data.grad_board || " " ) + '\t' + (data.grad_year || " " ) + '\t' + (data.grad_roll || " " ) + '\t' + 
                (data.grad_max_marks || " " ) + '\t' + (data.grad_marks_obtained || " ") + '\t' + (data.grad_remarks || " " ) + '\t' + 
                (data.post_grad_board || " " ) + '\t' + (data.post_grad_year || " " ) + '\t' + (data.post_grad_roll || " " ) + '\t' + 
                (data.post_grad_max_marks || " " ) + '\t' + (data.post_grad_marks_obtained || " ") + '\t' + (data.post_grad_remarks || " " ) + '\t' + 
                (data.other_board || " " ) + '\t' + 
                (data.others_year || " " )  + '\t' + (data.others_roll || " " ) + '\t' + (data.others_max_marks || " " ) + '\t' + (data.others_marks_obtained || " " ) + '\t' + 
                (data.others_remarks || " " ) +	'\t' + 
                
                (data.bed_board || " " ) + '\t' +  (data.bed_year || " " )  + '\t' + (data.bed_roll || " " ) + '\t' + 
                (data.bed_max_marks || " " ) + '\t' + (data.bed_marks_obtained || " " ) + '\t' + 
                (data.bed_remarks || " " ) + '\t' +
                
                (data.m1_theory_marks || " " ) + '\t' +(data.m1_prac_marks || " " ) + '\t' +
                (data.m1_theory_obt || " " ) + '\t' +(data.m1_prac_obt || " " ) + '\t' +
                (data.m1_rem || " " ) + '\t' +

                (data.m2_theory_marks || " " ) + '\t' +(data.m2_prac_marks || " " ) + '\t' +
                (data.m2_theory_obt || " " ) + '\t' +(data.m2_prac_obt || " " ) + '\t' +
                (data.m2_rem || " " ) + '\t' +

                (data.m3_theory_marks || " " ) + '\t' +(data.m3_prac_marks || " " ) + '\t' +
                (data.m3_theory_obt || " " ) + '\t' +(data.m3_prac_obt || " " ) + '\t' +
                (data.m3_rem || " " ) + '\t' +
                
                (data.fee_amount || " " ) + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + 
                (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
            
        };

        this.kmggpgcGetAdmReport = (result) => {
            
            var report = "S.No." + "\t" + "Form for" + "\t" + "Course" + "\t"  + "Class" + "\t" + "Aadhar No" + "\t" + "Name" + "\t" + "Email" + "\t" + "Gender" + "\t" 
            + "Date Of Birth" + "\t" + "Mobile No." + "\t" + "Father Name" + "\t" + "Mother Name" + "\t"
            + "Guardian Mobile" + "\t" + "Address" + "\t" + "Pin" + "\t" + "Jain Minority Status" + "\t" + "Resident Of" + "\t" 
            + "Caste Category"  + "\t" + "Religion" + "\t" + "PH"  + "\t" 
            + "Intermediate Subject Stream" + "\t" + "Father In Defence" + "\t" + "Staff Ward" + "\t" + "Sports Quota" + "\t" + "NCC" + "\t" + "NSS" + "\t" 
            + "Scouts" + "\t" + "Rovers Rangers" + "\t" + "Matriculation Board" + "\t" + "Matriculation Year" + "\t" + "Matriculation Roll" + "\t" + "Matriculation Max Marks" + "\t" 
            + "Matriculation Marks Obtained" + "\t" + "Matriculation Remarks" + "\t" + "Intermediate Board" + "\t" + "Intermediate Year" + "\t" + "Intermediate Roll" 
            + "\t" + "Intermediate Max Marks" + "\t" + "Intermediate Marks Obtained" + "\t" + "Intermediate Remarks" + "\t" 
            + "Graduation Board" + "\t" + "Graduation Year" + "\t"  +"Graduation Roll" + "\t" + "Graduation Max Marks" 
            + "\t" + "Graduation Marks Obtained" + "\t" + "Graduation Remarks" + "\t" 
            + "Post Graduation Board" + "\t" + "Post Graduation Year" + "\t"  +"Post Graduation Roll" + "\t" + "Post Graduation Max Marks" 
            + "\t" + "Post Graduation Marks Obtained" + "\t" + "Post Graduation Remarks" + "\t" 
            + "B.Ed. Board" + "\t" + "B.Ed. Year" + "\t"  +"B.Ed. Roll" + "\t" + "B.Ed. Max Marks" 
            + "\t" + "B.Ed. Marks Obtained" + "\t" + "B.Ed. Remarks" + "\t" 
            + "Other Board" + "\t" + "Other Year" + "\t" + "Other Roll" + "\t" + "Other Max Marks" + "\t" + "Other Marks Obtained" 
            + "\t" +"Other Remarks" + "\t" + "Fee Amount" 
            + "\t" + "Fee Status" + "\t" + "TXN ID" + "\t" + "Payment Date(DD-MM-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.course || " " ) + '\t' + (data.stu_class || " " ) + '\t' + (data.aadhar_no || " " )+ '\t' + 
                (data.name || " " ) + '\t' + (data.email || " " ) + '\t' + (data.gender || " " ) + '\t' + 
                (data.dob || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.father_name || " " ) + '\t' +	(data.mother_name || " " ) + '\t' + (data.guardian_mobile || " " ) + '\t' + 
                (data.address || " " ) + '\t' + (data.pin || " " ) + '\t' + (data.category || " " ) + '\t' + (data.res_of || " " ) + '\t' + 
                (data.caste_category || " " ) + '\t' + (data.sub_category || " " ) + '\t' + (data.ph || " " ) + '\t' + (data.sub_stream || " " ) + '\t' + 
                (data.father_in_defence || " " )+ '\t' + (data.staff_ward || " " ) + '\t' + (data.sports_quota || " " ) + '\t' + (data.ncc || " " ) + '\t' + 
                (data.nss || " " ) + '\t' + (data.scouts || " " ) + '\t' + (data.rovers || " " ) + '\t' + (data.mat_board || " " ) + '\t' + 
                (data.mat_year || " " ) + '\t' + (data.mat_roll || " " ) + '\t' + (data.mat_max_marks || " " ) + '\t' + (data.mat_marks_obtained || " " )+ '\t' + 
                (data.mat_remarks || " " ) + '\t' + (data.inter_board || " " ) + '\t' + (data.inter_year || " " ) + '\t' + 
                (data.inter_roll || " " ) + '\t' + (data.inter_max_marks || " ") + '\t' + (data.inter_marks_obtained || " " ) + '\t' + 
                (data.inter_remarks || " " )  + '\t' + (data.grad_board || " " ) + '\t' + (data.grad_year || " " ) + '\t' + (data.grad_roll || " " ) + '\t' + 
                (data.grad_max_marks || " " ) + '\t' + (data.grad_marks_obtained || " ") + '\t' + (data.grad_remarks || " " ) + '\t' + 
                (data.post_grad_board || " " ) + '\t' + (data.post_grad_year || " " ) + '\t' + (data.post_grad_roll || " " ) + '\t' + 
                (data.post_grad_max_marks || " " ) + '\t' + (data.post_grad_marks_obtained || " ") + '\t' + (data.post_grad_remarks || " " ) + '\t' + 
                (data.other_board || " " ) + '\t' + 
                (data.others_year || " " )  + '\t' + (data.others_roll || " " ) + '\t' + (data.others_max_marks || " " ) + '\t' + (data.others_marks_obtained || " " ) + '\t' + 
                (data.others_remarks || " " ) +	'\t' + 
                
                (data.bed_board || " " ) + '\t' +  (data.bed_year || " " )  + '\t' + (data.bed_roll || " " ) + '\t' + 
                (data.bed_max_marks || " " ) + '\t' + (data.bed_marks_obtained || " " ) + '\t' + 
                (data.bed_remarks || " " ) +	'\t' +
                
                (data.fee_amount || " " ) + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + 
                (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
            
        };

        this.ngieGetAdmReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Year"  + "\t" + "Unique Id"  + "\t" + "Category" + "\t" 
            + "Original resident of" + "\t" + "Board" + "\t" + "Email" + "\t" + "Sex" + "\t" + "College Id" + "\t" + "Merit List No" + "\t"
            + "merit Serial No" + "\t" + "Registration No" + "\t" + "Merit Date" + "\t" + "Class" + "\t"  
            
            + "Subject 1" + "\t" + "Subject 2" + "\t" + "Subject 3" + "\t" + "Aadhar No" + "\t" + "Candidate Name" + "\t" 
            + "Father Name" + "\t" + "Father Occupation" + "\t" + "Annual Income" + "\t"
            + "Mother Name" + "\t" + "Mother Occupation" + "\t" + "Mobile No" + "\t" + "Date Of Birth" + "\t" 
            + "Nationality" + "\t" + "Religion" + "\t" 
            + "Minority Status" + "\t" + "Physical Handicapped" + "\t" + "Category" + "\t" + "Caste" + "\t"
            + "Original Resident Of" + "\t"  + "Address Permanent" + "\t" + "Address Present" + "\t"
            + "Parent/Guardian's Mobile No." + "\t" + "Student Mobile No." + "\t"
            
            + "High School Year" + "\t" + "High School Roll No." + "\t" + "High School School/Board" + "\t" + "High School Subject" + "\t" 
            + "High School Marks Obtained" + "\t" + "High School Max marks" + "\t" + "High School Percentage" + "\t" 

            + "Intermediate Year" + "\t" + "Intermediate Roll No."+ "\t" 
            + "Intermediate School/Board" + "\t" + "Intermediate Subject" + "\t" + "Intermediate Marks Obtained" + "\t" 
            + "Intermediate Max marks" + "\t" + "Intermediate Percentage" + "\t" 
            
            + "Graduation Year" + "\t" + "Graduation Roll No." + "\t" + "Graduation Board" + "\t" + "Graduation Subject"  + "\t" 
            + "Graduation Marks Obtained" + "\t" + "Graduation Max marks" + "\t" + "Graduation Percentage" + "\t" 
            
            + "Others Year" + "\t" + "Others Roll No." + "\t" + "Others Board" + "\t" 
            + "Others Subject" + "\t" + "Others Marks Obtained" + "\t" + "Others Max marks" + "\t" 
            + "Others Percentage" + "\t" 
            
            + "In Service" + "\t" + "Graduate From" + "\t" + "Spouse/Son/Daughter or Permanent employee " + "\t" 
            + "Rovers Rangers" + "\t" + "Scouts/Guides" + "\t" + "NSS" + "\t" + "NCC" + "\t" +"Caste category" + "\t" + "Sports Participant" + "\t"

            + "High School Marksheet Attached" + "\t" + "Intermediate Marksheet Attached" + "\t" + "Graduation Marksheet Attached" + "\t" 
            + "Aadhar Card Attached" + "\t" + "Medical Certificate by C.M.O" + "\t" 
            + "Category Marksheet Attached" + "\t" + "Character Marksheet Attached" + "\t" + "Transfer Marksheet Attached" + "\t" 
            + "Affidavit Attached" + "\t" + "Caste Certificate Attached" + "\t"  + "Domicile Certificate Attachment" + "\t"
            + "Migration Certificate Attached" + "\t" + "Weightage Certificate Attached" + "\t" + "Income Certificate Attached" + "\t"  
            + "Biometric Attached" + "\t" + "Placement Cell Attached" + "\t" + "TET Attached" + "\t"
            + "D.D.No. Attachment" + "\t" + "Other Attachment" + "\t"
            
            + "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.adm_year || " " ) + '\t' + (data.uniqueId || " " ) + '\t' + 
                (data.category || " " ) + '\t' + (data.orginal_res_of || " " )+ '\t' + (data.board || " " ) + '\t' + (data.email || " " ) + '\t' + 
                (data.sex || " " ) + '\t' + (data.college_id || " " ) + '\t' +	(data.merit_list_no || " " ) + '\t' + (data.merit_serial_no || " " ) + '\t' + 
                (data.en_no || " " ) + '\t' + (data.meritDate || " " ) + '\t' + (data.semester || " " ) + '\t' + 
                
                (data.subject_ba_1 || " " ) + '\t' + (data.subject_ba_2 || " " ) + '\t' + (data.subject_ba_3 || " " ) + '\t' + 
                (data.aadhar_no || " " )+ '\t' + (data.name || " " ) + '\t' + (data.father_name || " " ) + '\t' + (data.father_occ || " " ) + '\t' + 
                (data.father_income || " " ) + '\t' + (data.mother_name || " " ) + '\t' + (data.mother_occ || " " ) + '\t' + 
                (data.mobile || " " ) + '\t' + (data.dob || " " ) + '\t' + (data.nationality || " " ) + '\t' + (data.religion || " " ) + '\t' + 
                (data.minority_status || " " ) + '\t' + (data.Physical_status || " " ) + '\t' +
                (data.caste_category || " " ) + '\t' + (data.caste || " " ) + '\t' + (data.orginal_res_of || " " )+ '\t' + 
                (data.address || " " ) + '\t' + (data.address_Corres || " " ) + '\t' + (data.addr_phone || " " ) + '\t' + (data.addr_mob || " " ) + '\t' + 
                
                (data.hs_year || " " ) + '\t' + (data.hs_sno || " ") + '\t' + (data.hs_school || " " ) + '\t' + (data.hs_subject || " " ) + '\t' + 
                (data.hs_marks_obtained || " " )  + '\t' + (data.hs_marks || " " ) + '\t' + (data.hs_percentage || " " ) + '\t' + 

                (data.inter_year || " " ) + '\t' + (data.inter_sno || " " ) + '\t' + (data.inter_school || " " ) + '\t' + (data.inter_subject || " " ) + '\t' + 
                (data.inter_marks_obtained || " " ) + '\t' + (data.inter_marks || " " ) + '\t' + (data.inter_percentage || " " ) + '\t' + 
                
                (data.grad_year || " " ) + '\t' + (data.grad_sno || " " ) + '\t' + (data.grad_school || " " ) + '\t' + 
                (data.grad_subject || " " ) + '\t' + (data.grad_marks_obtained || " " ) + '\t' + (data.grad_marks || " " ) + '\t' + 
                (data.grad_percentage || " " ) + '\t' + 
                
                (data.other_year || " " ) + '\t' + (data.other_sno || " " ) + '\t' + (data.other_school || " " ) + '\t' + 
                (data.other_subject || " " ) + '\t' + (data.other_marks_obtained || " " ) + '\t' + (data.other_marks || " " ) + '\t' +
                (data.other_percentage || " " )  + '\t' +	
                
                (data.inService || " " ) + '\t' + (data.grad_from || " " )  + '\t' +
                (data.perm_emp || " " )  + '\t' + (data.rovers || " " )  + '\t' +	(data.scouts || " " )  + '\t' + (data.nss || " " ) + '\t' + 
                (data.ncc || " " ) + '\t' + (data.caste_category || " " )  + '\t' + (data.sports_participant || " " ) + '\t' + 

                (data.schoolMarkSheet || " " )  + '\t' + (data.interMarkSheet || " " ) + '\t' + (data.gradMarkSheet || " " )  + '\t' + 
                (data.aadhar_card || " " )  + '\t' + (data.medicalCert || " " )  + '\t' + (data.categoryCertificate || " " ) + '\t' + 
                (data.characterCertificate || " " )  + '\t' + (data.transferCertificate || " " ) + '\t' + (data.affidavit || " " )  + '\t' + 
                (data.casteCertificate || " " ) + '\t' + (data.domicileCertificate || " " )  + '\t' + (data.migrationCertificate || " " ) + '\t' +
                (data.weightageCertificate || " " )  + '\t' + (data.incomeCertificate || " " ) + '\t' + 

                (data.biometric || " " )  + '\t' + (data.placement_cell || " " ) + '\t' + (data.tet || " " )  + '\t' +
                (data.ddNo || " " )  + '\t' + (data.otherAttachment || " " ) + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
            
        };

        this.mmcGetRegReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Admission Unique Id" + "\t" + "Name of the Student" + "\t" 
            + "Father's Name" + "\t" + "Mother's Name" + "\t" + "Registration No" + "\t" + + "Mobile No" + "\t" 
            + "Course Category" + "\t" + "Class" + "\t" + "Year"  + "\t"
            + "Subject BA 1" + "\t" + "Subject BA 2" + "\t" + "Subject BA 3"
            + "Aadhar No" + "\t" + "Email Id" + "\t" + "Category" + "\t" + "Caste" + "\t" + "Sub Category" + "\t" 
            + "Gender" + "\t" + "Religion" + "\t"  
            
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.uniqueId || " " ) + '\t' + 
                (data.name || " " ) + '\t' + (data.father_name || " " ) + '\t' + (data.mother_name || " " ) + 
                (data.en_no || " " ) + '\t' +  (data.mobile || " " ) + '\t' + 
                (data.courseCategory || " " ) + '\t' + (data.semester || " " ) + '\t' +  (data.adm_year || " " ) + '\t' + 
                (data.subject_ba_1 || " " ) + '\t' + (data.subject_ba_2 || " " ) + '\t' + (data.subject_ba_3 || " " ) + '\t' + 
                (data.aadhar_no || " " )+ '\t' + (data.email || " " ) + '\t' + (data.caste_category || " " ) + '\t' + 
                (data.caste || " " )+ '\t' + (data.sub_category || " " ) + '\t' + (data.sex || " " ) + '\t' + 
                (data.religion || " " ) + '\t' + 

                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.gdcGetRegReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Name of the Student" + "\t" + "Father's Name" + "\t"
            + "Registration No" + "\t" + "Admission Unique Id" + "\t" + "Course" + "\t" + "\t" + "Part" + "\t" 
            + "Year"  + "\t" + "Subject 1" + "\t" + "Subject 2" + "\t" + "Subject 3" 
            + "Category" + "\t"  + "Gender" + "\t" + "Email Id" + "\t" + "Mobile No" + "\t" + "City" + "\t"
            
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.name || " " ) + '\t' + 
                (data.father_name || " " ) + '\t' + (data.en_no || " " ) + '\t' + (data.uniqueId || " " ) + '\t' + 
                (data.courseCategory || " " ) + '\t' + (data.part || " " ) + '\t' +  (data.adm_year || " " ) + '\t' + 
                (data.subject_ba_1 || " " ) + '\t' + (data.subject_ba_2 || " " ) + '\t' + (data.subject_ba_3 || " " ) + '\t' +
                (data.caste_category || " " ) + '\t' + (data.sex || " " ) + '\t' + 
                (data.email || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.city || " " ) + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.laxmiBaiGetAdmReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Name of the Candidate" + "\t" 
            + "Preference 1" + "\t" + "Preference 2" + "\t" + "Preference 3" + "\t" + "Part" + "\t" 
            + "Date of Birth" + "\t"  + "Father's Name" + "\t" + "Mother's Name" + "\t"
            + "Contact Address"  + "\t" + "Tel No." + "\t" + "E-mail ID" + "\t" + "Permanent Address" 
            + "Year of Passing" + "\t" + "Board/University" + "\t" + "Max. Marks" + "\t" + "Marks Obtained" + "\t" 
            + "%" + "\t" + "Subjects Studied" + "\t" + "Last Exam Cleared" + "\t" 
            + "If currently a student, name of institution and course" + "\t" 
            + "If employed, name of employer and designation (Attach No-objection certificate from the employer)" + "\t"

            + "Marks statement for Class 12 examination passed by the candidate" + "\t"
            + "No objection certificate from the Employer, if employed" + "\t"
            + "A copy of fee receipt from the bank" + "\t"
            + "A self-attested copy of Aadhar Card/Passport/Photo ID with address proof" + "\t"
            
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.name || " " ) + '\t' + 
                (data.preference1 || " " ) + '\t' + (data.preference2 || " " ) + '\t' +  (data.preference3 || " " ) + '\t' + 
                (data.dob || " " ) + '\t' + (data.father_name || " " ) + '\t' + (data.mother_name || " " ) + '\t' +
                (data.contact_address || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.email || " " ) + '\t' + 
                (data.perm_address || " " ) + '\t' + (data.hs_year || " " ) + '\t' + (data.hs_school || " " ) + '\t' + 
                (data.hs_max_marks || " " ) + '\t' + (data.hs_marks_obtained || " " ) + '\t' + (data.hs_percentage || " " ) + '\t' + 
                (data.hs_subjects || " " ) + '\t' + (data.lastExam || " " ) + '\t' + (data.inst_course || " " ) + '\t' + 
                (data.emp_designation || " " ) + '\t' + (data.schoolMarkSheet || " " ) + '\t' + (data.noObjectionCert || " " ) + '\t' + 
                (data.bankReceipt || " " ) + '\t' + (data.aadharCard || " " ) + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.getGmfReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Admission Type" + "\t" + "Name"  + "\t" 
            + "Father's Name"  + "\t" + "Roll No/Enrollment No" + "\t" + "University Registration No." + "\t" 
            + "Subject" + "\t" + "Year" + "\t" + "Semester" + "\t" + "Course" + "\t" + "Email" + "\t" + "Phone" + "\t" 
            + "City" + "\t" + "Query" + "\t" 
            +  "Fee Type" + "\t"
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + ("Fee-Payment " ) + '\t' + (data.adm_year || " " ) + '\t' + 
                (data.name || " " ) + '\t' + (data.father_name || " " ) + '\t' + 
                (data.enrollment_no || data.en_no || " " )+ '\t' + 
                (data.roll_no || " " ) + '\t' + (data.subject || " " ) + '\t' + (data.year || " " ) + '\t' + 
                (data.semester || " " ) + '\t' +	(data.course || " " ) + '\t' + (data.email || " " ) + '\t' +
                (data.mobile || " " ) + '\t' + (data.city || " " ) + '\t' + (data.query || " " ) + '\t' + 
                (data.fee.fees ? data.fee.fees[0].fee_name : " ") + '\t' +
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.getUOKGmfReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Name"  + "\t" + "Father's Name"  + "\t" 
            + "Application Id" + "\t" + "Subject" + "\t" + "Year" + "\t" + "Semester" + "\t" + "Course" + "\t" 
            + "Email" + "\t" + "Phone" + "\t"  +  "Fee Type" + "\t"
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + ("Fee-Payment " ) + '\t' + (data.name || " " ) + '\t' + 
                (data.father_name || " " ) + '\t' + (data.appID || data.en_no || " " )+ '\t' + 
                (data.subject || " " ) + '\t' + (data.year || " " ) + '\t' + 
                (data.semester || " " ) + '\t' +	(data.course || " " ) + '\t' + (data.email || " " ) + '\t' +
                (data.mobile || " " ) + '\t' + (data.fee.fees ? data.fee.fees[0].fee_name : " ") + '\t' +
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + 
                (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.dpgimtGetRegReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Category" + "\t" + "Email ID" + "\t" 
            + "Sex" + "\t" + "All India Jee (Mains) Rank/Marks" + "\t" + "Session" + "\t"
            + "Jee Roll Number" + "\t" + "Branches of Engg." + "\t" + "Other courses" + "\t" 
            + "Branch of Engg. (M.Tech)" + "\t" + "Aadhar No" + "\t"+ "Candidate Name" + "\t" 
            + "Father's Name" + "\t"  + "Mother's Name" + "\t" + "Occupation" + "\t" 

            + "Postal Address Line1" + "\t"+ "Postal Address city" + "\t" + "Postal Address State" + "\t" 
            + "Postal Address Pin" + "\t" + "Postal Address Fax" + "\t" + "Postal Address STD Code" + "\t" 

            + "Income per annum" + "\t"+ "Parent's Contact No." + "\t" + "Relative's Contact No." + "\t" 
            + "Date Of Birth" + "\t" + "Nationality" + "\t" + "Religion" + "\t" + "Address" + "\t" 
            + "Blood Group" + "\t" + "Region" + "\t" 
            
            + "Matric/10th Year" + "\t" + "Matric/10th Roll No." + "\t" 
            + "Matric/10th School/Board" + "\t" + "Matric/10th Subject" + "\t" + "Matric/10th Marks Obtained" + "\t" 
            + "Matric/10th Max marks" + "\t" + "Matric/10th Percentage" + "\t" 
            +"10+2/Diploma Year" + "\t" + "10+2/Diploma Roll No."+ "\t" + "10+2/Diploma School/Board" + "\t" 
            + "10+2/Diploma Subject" + "\t" + "10+2/Diploma Marks Obtained" + "\t" +  "10+2/Diploma Max marks" + "\t" 
            + "10+2/Diploma Percentage" + "\t" 
            + "Other Year" + "\t" + "Other Roll No." + "\t" + "Other School/Board" + "\t" + "Other Subject" + "\t" 
            + "Other Marks Obtained" + "\t" + "Other Max marks" + "\t" + "Other Percentage" + "\t"

            + "Have you ever been convicted for any criminal offense?" + "\t" + "Is there any case pending against you ?" + "\t" 

            + "Class 10 or equivalent mark sheet and certificate" + "\t" + "Class 12 or equivalent mark sheet and certificate" + "\t" 
            + "Admit card of JEE Main" + "\t" + "Jee Main Rank Card" + "\t" + "Certificate of Medical Fitness" + "\t" 
            + "Character Certificate" + "\t" + "Haryana Admission Allotment Number" + "\t" + "ADD for the specified fee" + "\t" 
            + "Aadhar Card" + "\t" + "Resident Certificate (for outside Haryana candidate)" + "\t"  
            + "Certificate from the Employer in the case of Employment of Government Of Haryana, members of All India Services  borne on Haryana Cadre" + "\t"
            + "Employee of Statutory Bodies/Corporation" + "\t" + "SC/OBC/PH certificate" + "\t" 
            + "Certificate from children and Grandchildren of Freedom Fighters" + "\t"  
            + "Certificate from wards of Deceased/Disabled/Discharged Military/Para-Military personnel/Ex-Servicemen or Ex-Personnel of Para Military Forces" + "\t" 
            + "Certificate from wards of ex-employee of Indian Defense Services/Para-Military Forces" + "\t" 
            + "Proof of annual parental income from all sources of TFW quota candidates and BC,SBC,EBP candidates)" + "\t"
             
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.category || " " ) + '\t' + 
                (data.email || " " ) + '\t' + (data.sex || " " ) + '\t' +	(data.merit_list_no || " " ) + '\t' + 
                (data.session || " " ) + '\t' + (data.merit_serial_no || " " ) + '\t' + 
                (data.semester || " " ) + '\t' + (data.other_courses || " " ) + '\t' + (data.branch_mtech || " " ) + '\t' +
                (data.aadhar_no || " " ) + '\t' + (data.name || " " ) + '\t' + (data.father_name || " " ) + '\t' +  
                (data.mother_name || " " ) + '\t' + (data.occ || " " ) + '\t' + 

                (data.postalAddrLine1 || " " ) + '\t' + (data.postalAddrCity || " " ) + '\t' + (data.postalAddrState || " " ) + '\t' + 
                (data.postalAddrPin || " " ) + '\t' + (data.postalAddrFax || " " ) + '\t' + (data.postalAddrStdCode || " " ) + '\t' + 
                
                (data.annual_income || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.rel_mobile || " " ) + '\t' + 
                (data.dob || " " ) + '\t' + (data.nationality || " " ) + '\t' + (data.religion || " " ) + '\t' + 
                (data.address || " " ) + '\t' + (data.blood_group || " " ) + '\t' + (data.region || " " ) + '\t' + 
                
                (data.hs_year || " " ) + '\t' + (data.hs_sno || " ") + '\t' + (data.hs_school || " " ) + '\t' + (data.hs_subject || " " ) + '\t' + 
                (data.hs_marks_obtained || " " )  + '\t' + (data.hs_marks || " " ) + '\t' + (data.hs_percentage || " " ) + '\t' + 
                
                (data.inter_year || " " ) + '\t' + (data.inter_sno || " " ) + '\t' + (data.inter_school || " " ) + '\t' + (data.inter_subject || " " ) + '\t' + 
                (data.inter_marks_obtained || " " ) + '\t' + (data.inter_marks || " " ) + '\t' + 
                (data.inter_percentage || " " ) + '\t' + 
                
                (data.others_year || " " )  + '\t' + (data.others_sno || " " ) + '\t' + (data.others_school || " " ) + '\t' + 
                (data.others_subject || " " ) + '\t' + (data.others_marks_obtained || " " ) + '\t' + 
                (data.others_marks || " " ) + '\t' + 

                (data.convicted || " " ) + '\t' + (data.legalCasePending || " " ) + '\t' + 

                (data.schoolMarkSheet || " " )  + '\t' + (data.interMarkSheet || " " ) + '\t' + (data.jeeAdmitCard || " " )  + '\t' + 
                (data.jeeMainRankCard || " " )  + '\t' + (data.certMedicalFitness || " " )  + '\t' + (data.characterCertificate || " " ) + '\t' + 
                (data.admAllotNo || " " )  + '\t' + (data.addSpeciedFee || " " ) + '\t' + (data.aadhar_card || " " )  + '\t' + 
                (data.resCert || " " ) + '\t' + (data.empCert || " " )  + '\t' + (data.empStatutory || " " ) + '\t' +
                (data.scObcPhCert || " " )  + '\t' + (data.certFreedomFighter || " " ) + '\t' + 
                (data.certWardsOf1 || " " )  + '\t' + (data.certWardsOf2 || " " ) + (data.incomeProof || " " )  + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.laxmiBaiAdmReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Examination Roll No." + "\t" 
            + "College Roll No." + "\t" + "Course" + "\t" + "Name of the Candidate" + "\t" + "Part" + "\t" 
            + "Date of Birth" + "\t"  + "Contact No." + "\t" + "Email Id" + "\t"
            + "Category"  + "\t" + "Medium" + "\t" + "Local Address" + "\t" + "Permanent Address" 
            + "Father's Name" + "\t" + "Father's Contact No." + "\t" + "Father's Email Id" + "\t" + "Mother's Name" + "\t" 
            + "Mother's Contact No." + "\t" + "Mother's Email Id" + "\t" + "Semester" + "\t" 

            + "Subject1 Marks(Semester 1)" + "\t" + "Subject2 Marks(Semester 1)" + "\t" 
            + "Subject3 Marks(Semester 1)" + "\t" + "Subjec4 Marks(Semester 1)" + "\t" 

            + "Subject1 Marks(Semester 2)" + "\t" + "Subject2 Marks(Semester 2)" + "\t" 
            + "Subject3 Marks(Semester 2)" + "\t" + "Subjec4 Marks(Semester 2)" + "\t" 

            + "Subject1 Marks(Semester 3)" + "\t" + "Subject2 Marks(Semester 3)" + "\t" 
            + "Subject3 Marks(Semester 3)" + "\t" + "Subjec4 Marks(Semester 3)" + "\t" 

            + "Subject1 Marks(Semester 4)" + "\t" + "Subject2 Marks(Semester 4)" + "\t" 
            + "Subject3 Marks(Semester 4)" + "\t" + "Subjec4 Marks(Semester 4)" + "\t" 
            
            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.en_no || " " ) + '\t' + 
                (data.uniqueId || " " ) + '\t' + (data.course || " " ) + '\t' +  (data.name || " " ) + '\t' + 
                (data.dob || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.email || " " ) + '\t' +
                (data.category || " " ) + '\t' + (data.medium || " " ) + '\t' + (data.local_address || " " ) + '\t' + 
                (data.permanent_address || " " ) + '\t' + (data.father_name || " " ) + '\t' + (data.f_mobile || " " ) + '\t' + 
                (data.f_email || " " ) + '\t' + (data.mother_name || " " ) + '\t' + (data.m_mobile || " " ) + '\t' + 
                (data.m_email || " " ) + '\t' + (data.semester || " " ) + '\t' + 
                
                
                (data.sem1_marks1 || " " ) + '\t' + (data.sem1_marks2 || " " ) + '\t' + (data.sem1_marks2 || " " ) + '\t' + 
                (data.sem1_marks2 || " " ) + '\t' + 
                (data.sem2_marks1 || " " ) + '\t' + (data.sem2_marks2 || " " ) + '\t' + (data.sem2_marks2 || " " ) + '\t' + 
                (data.sem2_marks2 || " " ) + '\t' + 
                (data.sem3_marks1 || " " ) + '\t' + (data.sem3_marks2 || " " ) + '\t' + (data.sem3_marks2 || " " ) + '\t' + 
                (data.sem3_marks2 || " " ) + '\t' + 
                (data.sem4_marks1 || " " ) + '\t' + (data.sem4_marks2 || " " ) + '\t' + (data.sem4_marks2 || " " ) + '\t' + 
                (data.sem4_marks2 || " " ) + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };

        this.sietAdmReport = (result) => {
            var report = "S.No." + "\t" + "Form for" + "\t" + "Course" + "\t" 
            + "Class" + "\t" + "Subject 1" + "\t" + "Subject 2" + "\t" + "Subject 3" + "\t" 
            + "Date of Birth" + "\t"  + "Contact No." + "\t" + "Email Id" + "\t"
            + "Aadhar No"  + "\t" + "Name" + "\t" + "Email" + "\t" + "Date of Birth" 
            + "Gender" + "\t" + "Mobile No." + "\t" + "Father's Name" + "\t" + "Mother's Name" + "\t" 
            + "Guardian's Mobile No." + "\t" + "Address" + "\t" + "PIN" + "\t" 

            + "Minority Status" + "\t" + "Religion" + "\t" + "Resident" + "\t" + "Category" + "\t"
            + "Intermediate/ Graduation Subject Stream Educational Information" + "\t" 
            + "Physically Handicapped - Yes (More than or equal to 40%) otherwise No" + "\t" 
            + "Is Father in Defence" + "\t" + "Sports Quota" + "\t" + "N.C.C." + "\t"
            + "N.S.S." + "\t" + "Scouts/Guides" + "\t" + "Rovers Rangers" + "\t"

            + "Matriculation Class" + "\t" + "Matriculation Board/Univ." + "\t" 
            + "Matriculation Year" + "\t" + "Matriculation Roll No." + "\t" + "Matriculation Marks Obtained" + "\t" 
            + "Matriculation Remarks" + "\t"  

            + "Intermediate Class" + "\t" + "Intermediate Board/Univ." + "\t" + "Intermediate Year" + "\t" 
            + "Intermediate Roll No." + "\t" + "Intermediate Marks Obtained" + "\t" + "Intermediate Remarks" + "\t" 

            + "Graduation Class" + "\t" + "Graduation Board/Univ." + "\t" + "Graduation Year" + "\t" 
            + "Graduation Roll No." + "\t" + "Graduation Marks Obtained" + "\t" + "Graduation Remarks" + "\t" 

            + "Post Graduation Class" + "\t" + "Post Graduation Board/Univ." + "\t" + "Post Graduation Year" + "\t" 
            + "Post Graduation Roll No." + "\t" + "Post Graduation Marks Obtained" + "\t" + "Post Graduation Remarks" + "\t" 

            + "M.Ed Entrance Class" + "\t" + "M.Ed Entrance Board/Univ." + "\t" + "M.Ed Entrance Year" + "\t" 
            + "M.Ed Entrance Roll No." + "\t" + "M.Ed Entrance Marks Obtained" + "\t" + "M.Ed Entrance Remarks" + "\t" 

            + "Any Other Class" + "\t" + "Any Other Board/Univ." + "\t" + "Any Other Year" + "\t" 
            + "Any Other Roll No." + "\t" + "Any Other Marks Obtained" + "\t" + "Any Other Remarks" + "\t" 

            +  "Fee Amount" + "\t" + "Fee Status" + "\t" + "TXNid" + "\t" + "Payment Date (MM-DD-YYYY)" +  "\n";
            for (var i = 0; i < result.length; i++) {
                var data = result[i];
                
                report += (i + 1) + '\t' + (data.form_for || " " ) + '\t' + (data.course || " " ) + '\t' + 
                (data.class_regular || class_private || " " ) + '\t' + (data.subject_ba_1 || " " ) + '\t' +  
                (data.subject_ba_2 || " " ) + '\t' + (data.subject_ba_3 || " " ) + '\t' + (data.aadhar_no || " " ) + '\t' + 
                (data.name || " " ) + '\t' + (data.email || " " ) + '\t' + (data.dob || " " ) + '\t' + 
                (data.gender || " " ) + '\t' + (data.mobile || " " ) + '\t' + (data.father_name || " " ) + '\t' + 
                (data.mother_name || " " ) + '\t' + (data.guardian_mobile || " " ) + '\t' + (data.address || " " ) + '\t' + 
                (data.pin || " " ) + '\t' + (data.category || " " ) + '\t' + (data.sub_category || " " ) + '\t' + 
                (data.res_of || " " ) + '\t' + (data.caste_category || " " ) + '\t' + (data.sub_stream || " " ) + '\t' + 
                (data.ph || " " ) + '\t' + (data.father_in_defence || " " ) + '\t' + (data.sports_quota || " " ) + '\t'
                (data.ncc || " " ) + '\t' + (data.nss || " " ) + '\t' + (data.scouts || " " ) + '\t'
                (data.rovers || " " ) + '\t' + 
                
                
                (data.mat_board || " " ) + '\t' + (data.mat_year || " " ) + '\t' + (data.mat_roll || " " ) + '\t' + 
                (data.mat_max_marks || " " ) + '\t' + (data.mat_marks_obtained || " " ) + '\t' + (data.mat_remarks || " " ) + '\t' + 
                
                (data.inter_board || " " ) + '\t' + (data.inter_year || " " ) + '\t' + (data.inter_roll || " " ) + '\t' + 
                (data.inter_max_marks || " " ) + '\t' + (data.inter_marks_obtained || " " ) + '\t' + (data.inter_remarks || " " ) + '\t' + 
                
                (data.grad_board || " " ) + '\t' + (data.grad_year || " " ) + '\t' + (data.grad_roll || " " ) + '\t' + 
                (data.grad_max_marks || " " ) + '\t' + (data.grad_marks_obtained || " " ) + '\t' + (data.grad_remarks || " " ) + '\t' + 

                (data.post_grad_board || " " ) + '\t' + (data.post_grad_year || " " ) + '\t' + (data.post_grad_roll || " " ) + '\t' + 
                (data.post_grad_max_marks || " " ) + '\t' + (data.post_grad_marks_obtained || " " ) + '\t' + 
                (data.post_grad_remarks || " " ) + '\t' + 

                (data.bed_board || " " ) + '\t' + (data.bed_year || " " ) + '\t' + (data.bed_roll || " " ) + '\t' + 
                (data.bed_max_marks || " " ) + '\t' + (data.bed_marks_obtained || " " ) + '\t' + (data.bed_remarks || " " ) + '\t' + 

                (data.other_board || " " ) + '\t' + (data.other_year || " " ) + '\t' + (data.other_roll || " " ) + '\t' + 
                (data.other_max_marks || " " ) + '\t' + (data.other_marks_obtained || " " ) + '\t' + (data.other_remarks || " " ) + '\t' + 
                
                (data.fee_amount || " ") + '\t' + (data.fee_status || " " ) + '\t' + (data.txnid || " " ) + '\t' + (data.payment_date || " " );
                /*if (data.payment_date) {
                    report += data.payment_date.split("T")[0];
                    }*/
                report += '\n';
                }
                
            return report
        };
    };

    module.exports = new ExcelReportSvc();        
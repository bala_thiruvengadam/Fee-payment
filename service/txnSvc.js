var dbUtil = require('../config/dbUtil');
var payU = require('../utils/payUUtils');
var payUConfig = require('../config/payU');
var Transaction = require('../models/transaction');
var ctx = require('../config/ctxconfig');
var $q = require('q');
const pug = require('pug');
var max_retry = 3;

module.exports.updateTxnObj = function(queryObj, updateObj, options) {
	var txnPromise = $q.defer();
	dbUtil.getConnection(function(db) {
		var collection = db.collection('transactions');
		collection.update(queryObj,{$set:{updateObj}},options,function(err,res){
			if(err){
				txnPromise.reject(err);
			}else{
				txnPromise.resolve(res);
			}
		});
	});
	return txnPromise.promise;
};


module.exports.saveTxnObj = function(ObjtoSave) {
	console.log("====" + ObjtoSave)
	var txnPromise = $q.defer();
	dbUtil.getConnection(function(db) {
		var collection = db.collection('transactions');
		collection.save(ObjtoSave,function(err,res){
			if(err){
				txnPromise.reject(err);
			}else{
				txnPromise.resolve(res);
			}
		});
	});
	return txnPromise.promise;
};

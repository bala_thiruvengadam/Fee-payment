var dbUtil = require('../config/dbUtil');
var payU = require('../utils/payUUtils');
var payUConfig = require('../config/payU');
var Transaction = require('../models/transaction');
var ctx = require('../config/ctxconfig');
var $q = require('q');
const pug = require('pug');
var ObjectID = require('mongodb').ObjectID;
var async = require('async');
var _ = require('lodash');

var _fee_collection_name = "fee";
var _student_fee_collection_name = "fee_student";



var validateFee = function(fee) {
	if (!fee.tenant) {
		return "tenant is missing";
	}
	if (!fee.fee_for) {
		return "Fee type against is missing";
	}
	if (_.isEmpty(fee.fees)) {
		return "No valid fees found";
	}
	return "1";
};

// default page settings


var FeeSvc = function() {
	this.createFee = (fee) => {

		var createFeePromise = new Promise((resolve, reject) => {
			var valid = validateFee(fee);
			if (valid != "1") {
				reject(valid);
			}

			// setting default
			fee.created_on = new Date();
			fee.deleted = false;
			fee.freezed = false;
			fee.fee_for = "misc";
			fee.fee_status = "UNPAID";

			async.retry(function(callback) {

				fee.fee_id = payU.generateRandom(5);

				dbUtil.getTenantCollection(fee.tenant, _fee_collection_name).then((collection) => {

					collection.save(fee, (err, result) => {

						callback(null, result);
					});
				}, (err) => {
					callback(err);
				});
			}, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(fee);
				}
			});
		});
		return createFeePromise;

	};


	this.updateFee = (tenant, queryObj, feeObj) => {
		//console.log("update Fee called");
		console.log("feeObj.disable_fee:: ", feeObj.disable_fee);
		var updateFeePromise = new Promise((resolve, reject) => {
			if (!queryObj.tenant || !feeObj.fee_id)
				reject("tenant missing");

			try {
				dbUtil.getTenantCollection(tenant, _fee_collection_name).then((collection) => {
					collection.update({
						fee_id: feeObj.fee_id
					}, {
						$set: {
							fees: feeObj.fees,
							partial_payment: feeObj.partial_payment,
							disabled: feeObj.disable_fee
						}
					}, (err, result) => {
						if (err) {
							reject(err);
						} else {
							resolve(result);
						}
					});
				}, (err) => {
					reject(err);
				});
			} catch (err) {
				reject(err);
			}
		});
		return updateFeePromise;
	};

	this.getFees = (fetchFor,tenant, userQuery) => {
		return new Promise((resolve, reject) => {
			if (!tenant)
				reject("tenant missing");

			var queryObj ;
			//FeeSvc.buildMiscFeeQueryObj(queryObj, userQuery);
				
		
			dbUtil.getTenantCollection(tenant, _fee_collection_name).then((collection) => {
				var cursor;
				if(fetchFor == 'mfs'){
					cursor = collection.find( {
						$and : [
							{ $or: [{ disabled: { $exists: false } },{ disabled: false },{ disabled: null }]},
							{ deleted: false }
						]
					} );
				}else if(fetchFor == 'misc-dash'){
					cursor = collection.find( {
						$and : [
							{ deleted: false }
						]
					} );
				}
				
				FeeSvc.getPageCursor(cursor, userQuery);

				cursor.toArray((err, result) => {
					if (err) {
						reject(err);
					} else {
						
						resolve(result);
					}
				})
			}, (err) => {
				reject(err);
			});
		});
	};

	this.getFee = (tenant, queryObj, options) => {
		return new Promise((resolve, reject) => {
			if (!tenant)
				reject("tenant missing");

			options = options || {};

			FeeSvc.buildPageOptions(options);

			queryObj.deleted = false;

			dbUtil.getTenantCollection(tenant, _fee_collection_name).then((collection) => {
				collection.findOne(queryObj, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};

	this.deleteFee = (tenant, queryObj) => {
		return new Promise((resolve, reject) => {
			//console.log("tenant", tenant, "queryIbj", queryObj)
			if (!tenant || !queryObj.fee_id)
				reject("tenant or fee to delete missing missing");

			dbUtil.getTenantCollection(tenant, _fee_collection_name).then((collection) => {
				collection.update(queryObj, {
					$set: {
						deleted: true
					}
				}, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};


	this.deletemany = (tenant, fee_ids) => {
		return new Promise((resolve, reject) => {
			if (!tenant || _.isEmpty(fee_ids))
				reject("tenant or fee to delete missing missing");

			var queryObj = {
				fee_id: {
					$in: fee_ids
				}
			};

			dbUtil.getTenantCollection(tenant, _fee_collection_name).then((collection) => {
				collection.update(queryObj, {
					$set: {
						deleted: true
					}
				}, {
					multi: true
				}, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};
	this.saveStudentFee = (tenant, student_fee) => {
		student_fee.fee_status = "UNPAID";
		return new Promise((resolve, reject) => {
			async.retry(function(callback) {

				student_fee.fee_id = payU.generateRandom(5);

				dbUtil.getTenantCollection(tenant, _student_fee_collection_name).then((collection) => {
					collection.save(student_fee, (err, result) => {
						callback(err, result);
					});
				}, (err) => {
					callback(err)
				});
			}, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(student_fee);
				}
			});

		});
	};

	this.getStudentFee = (tenant, queryObj) => {
		return new Promise((resolve, reject) => {
			if (!tenant)
				reject("tenant missing");

			//queryObj.deleted = false;

			//console.log
			dbUtil.getTenantCollection(tenant, _student_fee_collection_name).then((collection) => {
				collection.findOne(queryObj, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};

	this.getMiscFeeDetails = (tenant,  userQuery,options) => {
		var queryObj = {};
		options=options||{};
		FeeSvc.buildMiscFeeDetailsQueryObj(queryObj, userQuery);
		//console.log("Misc query ",queryObj);
		return new Promise((resolve, reject) => {
			dbUtil.getTenantCollection(tenant, _student_fee_collection_name).then((collection) => {
				
				var cursor = collection.find(queryObj).sort({'payment_date':-1});

				FeeSvc.getPageCursor(cursor, userQuery);

				cursor.toArray((err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};


	this.updateStudentFee = (tenant, queryObj, updObj) => {



		return new Promise((resolve, reject) => {
			if (!queryObj.fee_id)
				reject("tenant missing");

			dbUtil.getTenantCollection(tenant, _student_fee_collection_name).then((collection) => {
				collection.update(queryObj, {
					$set: updObj
				}, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err)
			});
		});
	};

	this.reqularFeeSearch = function(tenant, userQuery) {
		return new Promise((resolve, reject) => {
			if (!userQuery.tenant) {
				return reject("Tenant Id is missing");
			}
			var queryObj = {};

			FeeSvc.buildRegularFeeQueryObj(queryObj, userQuery);

			dbUtil.getConnection(function(db) {

				var collection = db.collection('T_' + tenant + '_userdetails');
				//console.log("Query Obj ", queryObj);
				var cursor = collection.find(queryObj).sort({'payment_date':-1});

				FeeSvc.getPageCursor(cursor, userQuery);


				cursor.toArray((err, stuFees) => {
					if (err) {
						reject(err);
					} else
						resolve(stuFees);
				});
			});
		});
	};
}

FeeSvc.getPageCursor = (cursor, options) => {
	var pgOptions = FeeSvc.buildPageOptions(options);
	//console.log("Page Ooptions : : ", pgOptions);
	if (pgOptions.offset != -1) {
		cursor.skip(pgOptions.offset);
	}
	if (pgOptions.limit != -1) {
		cursor.limit(pgOptions.limit);
	}
};

FeeSvc.pgSize = 100;
FeeSvc.pgNo = 1;
FeeSvc.buildPageOptions = function(options) {
	var pgOption = {};

	if (options.pgNo == -1 || options.pgSize == -1) {
		return {
			limit: -1,
			offset: -1
		};
	}

	pgOption.limit = options.pgSize || FeeSvc.pgSize;

	var pgNo = options.pgNo || FeeSvc.pgNo;
	pgOption.offset = (pgNo - 1) * pgOption.limit;
	return pgOption;
};

FeeSvc.buildMiscFeeQueryObj = function(queryObj, userQuery) {
	queryObj.tenant = userQuery.tenant;
	if (userQuery.fee_for) {
		queryObj.fee_for = userQuery.fee_for;
	}

	if (userQuery.startDate) {
		queryObj.created_on = {
			$gte: userQuery.startDate
		};
	}
	if (userQuery.endDate) {
		queryObj.created_on = queryObj.endDate || {};
		queryObj.created_on['$lt'] = userQuery.endDate;
	}
	if (userQuery.fee_id) {
		queryObj.fee_id = userQuery.fee_id;
	}
};


FeeSvc.buildMiscFeeDetailsQueryObj = function(queryObj, userQuery) {
	queryObj.tenant = userQuery.tenant;
	if (userQuery.fee_for) {
		queryObj.fee_for = userQuery.fee_for;
	}

	if (userQuery.startDate) {
		queryObj.created_on = {
			$gte: userQuery.startDate
		};
	}

	if (userQuery.endDate) {
		queryObj.created_on = queryObj.endDate || {};
		queryObj.created_on['$lt'] = userQuery.endDate;
	}

	if (userQuery.fee_id) {
		queryObj.fee_id = userQuery.fee_id;
	}
	if(userQuery.misc_fee_id){
		queryObj['fee.fee_id'] = userQuery.misc_fee_id;
	}

	if (userQuery.stream) {
		queryObj.stream = userQuery.stream;
	}
	if (userQuery.semester) {
		queryObj.semester = userQuery.semester;
	}

	if (userQuery.course) {
		queryObj.course = userQuery.course;
	}

	if (userQuery.name) {
		var regex = ".*" + userQuery.name + ".*";
		queryObj.name = {
			$regex: regex,
			$options: 'i'
		};
	}

	if (userQuery.father_name) {
		var regex = ".*" + userQuery.father_name + ".*";
		queryObj.father_name = {
			$regex: regex,
			$options: 'i'
		};
	}

	if (userQuery.fee_status)
		queryObj.fee_status = userQuery.fee_status;

	if (userQuery.txnid) {
		queryObj.txnid = userQuery.txnid;
	}

	if (userQuery.mobile) {
		queryObj.mobile = userQuery.mobile;
	}

	if (userQuery.roll_no)
		queryObj.roll_no = userQuery.roll_no;

	if (userQuery.enrollment_no)
		queryObj.enrollment_no = userQuery.enrollment_no;


};

FeeSvc.buildRegularFeeQueryObj = function(queryObj, userQuery) {
	if (userQuery.txnid) {
		queryObj.txnid = userQuery.txnid;
	}

	if (userQuery.mobile) {
		queryObj.mobile = userQuery.mobile;
	}

	if (userQuery.roll_no)
		queryObj.roll_no = userQuery.roll_no;

	if (userQuery.enrollment_no)
		queryObj.enrollment_no = userQuery.enrollment_no;

	if (userQuery.stream) {
		queryObj.stream = userQuery.stream;
	}
	if (userQuery.semester) {
		queryObj.semester = userQuery.semester;
	}

	if (userQuery.course) {
		var regex =  userQuery.course + ".*";
		queryObj.course = {
			$regex: regex,
			$options: 'i'
		};
	}

	if (userQuery.name) {
		var regex =  userQuery.name + ".*";
		queryObj.name = {
			$regex: regex,
			$options: 'i'
		};
	}


	if (userQuery.fee_status) {
		queryObj.status = userQuery.fee_status;
	}

	if (userQuery.startDate) {
		queryObj.payment_date = {
			$gte: userQuery.startDate
		};
	}
	if (userQuery.endDate) {
		queryObj.payment_date = queryObj.endDate || {};
		queryObj.payment_date['$lt'] = userQuery.endDate;
	}

}

module.exports = new FeeSvc();
var dbUtil = require('../config/dbUtil');
var payU = require('../utils/payUUtils');
var payUConfig = require('../config/payU');
var Transaction = require('../models/transaction');
var ctx = require('../config/ctxconfig');
var $q = require('q');
const pug = require('pug');
var ObjectID = require('mongodb').ObjectID;
var async = require('async');
var excelReportSvc = require('../service/excelReportSvc');
var _ = require('lodash');

var UserDetailsSvc = function() {
	var _user_accounts_collection_name = 'accounts';
	var _user_fee_student = 'fee_student';
	
	this.create = function(tenant, userDetails) {
		return new Promise((resolve, reject) => {
			if (_.isEmpty(tenant)) {
				reject("tenant id is missing");
			}
			if (_.isEmpty(userDetails) || !userDetails.enrollment_no || !userDetails.semester) {
				reject("Eithet enrollment_no or semester is missing");
			}
			var collectionName = 'T_' + tenant + '_userdetails';
			dbUtil.getCollection(collectionName).then((collection) => {
				collection.insertOne(userDetails, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(userDetails);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};

	this.update = function(tenant, queryObj, updObj) {
		return new Promise((resolve, reject) => {
			if (_.isEmpty(tenant)) {
				reject("tenant id is missing");
			}

			var collectionName = 'T_' + tenant + '_userdetails';
			dbUtil.getCollection(collectionName).then((collection) => {
				collection.update(queryObj, updObj, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};

	this.delete = function(tenant, queryObj, options) {
		return new Promise((resolve, reject) => {
			if (_.isEmpty(tenant)) {
				reject("tenant id is missing");
			}

			var collectionName = 'T_' + tenant + '_userdetails';
			dbUtil.getCollection(collectionName).then((collection) => {
				options = options || {};
				collection.remove(queryObj, options, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});

		});
	};

	this.find = function(tenant, queryObj, options) {
		return new Promise((resolve, reject) => {
			if (_.isEmpty(tenant)) {
				reject("tenant id is missing");
			}

			var collectionName = 'T_' + tenant + '_userdetails';
			dbUtil.getCollection(collectionName).then((collection) => {
				collection.find(queryObj, options).toArray((err, results) => {
					if (err) {
						reject(err);
					} else {
						resolve(results);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};

	this.findOne = function(tenant, queryObj) {
		return new Promise((resolve, reject) => {
			if (_.isEmpty(tenant)) {
				reject("tenant id is missing");
			}

			var collectionName = 'T_' + tenant + '_userdetails';
			dbUtil.getCollection(collectionName).then((collection) => {
				collection.find(queryObj, options, (err, result) => {
					if (err) {
						reject(err);
					} else {
						resolve(result);
					}
				});
			}, (err) => {
				reject(err);
			});
		});
	};

	this.findTenantUser = function(tenant){
		return new Promise((resolve,reject)=>{
			if(!tenant)
				reject("Tenant name can't be empty");

			dbUtil.getCollection(_user_accounts_collection_name).then((collection)=>{
				collection.findOne({username:tenant},(err,result)=>{
					if(err){
						reject(err);
					}else{
						resolve(result);
					}
				});
			},(err)=>{
				reject(err);
			});
		});
		
	};

	this.getAllGmfDetails = (tenant,queryObj) => {
		return new Promise((resolve, reject) => {
			if (!tenant)
				reject("tenant missing");
			
			dbUtil.getTenantCollection(tenant, _user_fee_student).then((collection) => {
				var cursor = collection.find({ "form_for": { $exists: false } });
				cursor.toArray((err, result) => {
					if (err) {
						reject(err);
					} else {
					
					if (result.length == 0)
						resolve(result);
					
					var report = ""

					console.log('result:: ', result)
					if(tenant == 'UOK')
						report = excelReportSvc.getUOKGmfReport(result)
					else
						report = excelReportSvc.getGmfReport(result)	
						
					resolve(report);
					}
				})
			}, (err) => {
				reject(err);
			});
		});
	};
	
	this.getAllRegUsersDetail = (tenant,queryObj,reportParam) => {
		return new Promise((resolve, reject) => {
			if (!tenant)
				reject("tenant missing");
			
			dbUtil.getTenantCollection(tenant, _user_fee_student).then((collection) => {
				var cursor = collection.find(queryObj);
				cursor.toArray((err, result) => {
					if (err) {
						reject(err);
					} else {
					
					if (result.length == 0)
						resolve(result);
					
					var report = ""

					console.log('queryObj.form_for::', queryObj.form_for)
					console.log('tenant:: ', tenant)
					console.log('reportParam:: ', reportParam)
						
					if(queryObj.form_for == "registration"){
						
						if(tenant == 'KMGGPGC')
							report = excelReportSvc.kmggpgcGetRegReport(result)
						else if(tenant == 'NGIE')
							report = excelReportSvc.ngieGetRegReport(result)
						else if(tenant == 'MMC')
							report = excelReportSvc.mmcGetRegReport(result)	
						else if(tenant == 'DPGIMT')
							report = excelReportSvc.dpgimtGetRegReport(result)	
						else if(tenant == 'GDC')
							report = excelReportSvc.gdcGetRegReport(result)		
						else{
							report = excelReportSvc.getRegReport(result)
						}			
					}
					else if(queryObj.form_for == "admission" || queryObj.form_for == "admission_gadm"){
						if(reportParam == 'JVJAIN')
							report = excelReportSvc.getAdmReport(result)
						else if(reportParam == 'KMGGPGC')
							report = excelReportSvc.kmggpgcGetAdmReport(result)
						else if(reportParam == 'NGIE')
							report = excelReportSvc.ngieGetAdmReport(result)
						else if(reportParam == 'LaxmiBai')
							report = excelReportSvc.laxmiBaiGetAdmReport(result)
						else if(reportParam == 'LaxmiBai_adm')
							report = excelReportSvc.laxmiBaiAdmReport(result)	
						else if(reportParam == 'siet_adm')
							report = excelReportSvc.sietAdmReport(result)	
					}
					resolve(report);
					}
				})
			}, (err) => {
				reject(err);
			});
		});
	};
};

module.exports = new UserDetailsSvc();
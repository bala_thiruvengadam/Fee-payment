var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    username: String,
    password: String,
    role:String,
    college:String,
    email:String,
    tenant:String,
    platform:String,
    rollno:String,
    firstname:String,
    studentRole:String,
    phone:String,
    userid:String
});

Account.plugin(passportLocalMongoose);
/*Account.plugin(passportLocalMongoose,{
	selectFields:'username password description image'
});*/

module.exports = mongoose.model('Account', Account);
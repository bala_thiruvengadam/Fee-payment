/*var express = require('express');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var xlsx = require('xlsx');
var dbUtil = require('../config/dbUtil');
var routeIndex = require('../routes/index');

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});

var upload = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) { //file filter
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {

            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');

//module.exports = function(app) {

//API path that will upload the files 
app.post('/upload', function (req, res) {

    console.log(req.body);

    var exceltojson;
    upload(req, res, function (err) {
        if (err) {
            res.json({
                error_code: 1,
                err_desc: err
            });
            return;
        }
        //Multer gives us file info in req.file object 
        if (!req.file) {
            res.json({
                error_code: 1,
                err_desc: "No file passed"
            });
            return;
        }
        //Check the extension of the incoming file and 
        //  use the appropriate module

        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }

        var workbook = xlsx.readFile(req.file.path);
        var sheets = workbook.SheetNames;
        //console.log(sheets);
        var tenant = req.user.username;
         //var tenant = "ABCD";

        function saveSheetData(index, sheets, tenant, cb) {
            console.log(index+" "+sheets.length);
            try {
                if (!(index < sheets.length)) {
                    console.log('returning');
                    return cb(null, true);
                }
                var sheetName = sheets[index];
                exceltojson({
                    input: req.file.path,
                    output: null,
                    sheet: sheetName,
                    lowerCaseHeaders: true
                }, function (err, result) {
                    if (err) {
                        return cb(err, null)
                    } else {

                        for (var j = 0; j < result.length; j++) {
                            result[j].stream = sheetName;
                        }
                        dbUtil.getConnection(function (db) {
                            var collection = db.collection('T_' + tenant + '_userdetails');

                            collection.insert(result, function (err, res1) {
                                console.log('data saved');
                                index++;
                                saveSheetData(index, sheets, tenant, cb);
                            });

                        });
                    }

                });
            } catch (e) {
                res.json({
                    error_code: 1,
                    err_desc: "Corrupted excel file"
                });
            }

        }

        saveSheetData(0, sheets, tenant, function (err, data) {

            if (err)
                return res.render('index', {message: 'Unable to save data'});
            if (data) {
                return res.render('index', {message: 'Data saved successfully'});
            }

        })


        })

});

//}
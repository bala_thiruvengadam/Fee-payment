const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Transaction = new Schema({
	tenant:{type:String,required:true},
	trnxid:{type:String, unique: true},	
	created_on:{type:Date,default:Date.now},
	updated_on :{type:Date,default:Date.now},
	email:String,
	uniqueId:String,
	fatherName:String,
	phone:String,
	amount:Number,
	status:String,
	first_name:String,
	last_name:String,
	description:String,
	udf_1:String, // en_no
	udf_2:String,  //semester
	action_user:{id:String,email:String,college:String,name:String,roll:String},
	fee_for:String,
	fee_id:String,
	member:String,
	enforce_paymethod:String,
	PG_name: String,
	fee_type: String
});

module.exports = mongoose.model('Transaction', Transaction);;
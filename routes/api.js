var fs = require('fs');
var multer = require('multer');
var extrequest = require('request');
var dbUtil = require('../config/dbUtil');
var json2xls = require('json2xls');

var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var xlsx = require('xlsx');

exports.studentdata = function(req, res) {
    console.log('Tenant == ', req.user.username)
    var tenant = req.user.username;
    dbUtil.getConnection(function(db) {
        var collection = db.collection('T_' + tenant + '_userdetails');

        collection.find().toArray(function(err, docs) {
            if (err) {
                console.log(err)
            }
            res.json(docs);
        });
    });
}

/*exports.studentpagedData = function(req, res) {
    var tenant = req.user.username;
    var pageNo = parseInt(req.query.pgNo || 1);
    var pageSize = parseInt(req.query.pgSize || 100);
    var skip = (pageNo - 1) * pageSize;


    dbUtil.getConnection(function(db) {
        var collection = db.collection('T_' + tenant + '_userdetails');

        collection.find({}).skip(skip).limit(pageSize).toArray(function(err, docs) {
            if (err) {
                console.log(err)
            }
            res.json(docs);
        });
    });
}*/

exports.adddata = function(req, res) {
    var tenant = req.user.username;
    dbUtil.getConnection(function(db) {
        var collection = db.collection('T_' + tenant + '_userdetails');
        var newEntry = req.body;
        collection.insert(newEntry, function(err, result) {
            if (err) {
                res.json({
                    "message": "error"
                });
            }
            res.json({
                "message": "success"
            });
        });
    });
};
exports.editdata = function(req, res) {
    var tenant = req.user.username;
    var query = req.body;
    dbUtil.getConnection(function(db) {
        var collection = db.collection('T_' + tenant + '_userdetails');
        collection.find(query).toArray(function(err, result) {
            if (err) console.log("Error edit data");
            res.json(result);
        });
    });
};

exports.getCurrentUser = function(req, res) {

    var username = req.user.username;
   
    /* dbUtil.getConnection(function(db) {
         var collection = db.collection('accounts');
         collection.findOne({username:username},{salt:0,hash:0},function(err,data){
           res.json(data);
         })
     });*/
    res.json(req.user);
};
exports.saveentry = function(req, res) {
    var tenant = req.user.username;
    var query = req.body;
    var enId = query.enrollment_no;
    var semester = query.semester;
    delete query['enrollment_no'];
    dbUtil.getConnection(function(db) {
        var collection = db.collection('T_' + tenant + '_userdetails');
        collection.findOneAndUpdate({
            "enrollment_no": enId,
            "semester": semester
        }, {
            $set: query
        }, function(err, doc) {
            if (err) {
                console.log("error on save edit");
            }
            res.json({
                "message": "success"
            });
        })
    });
};
exports.fetchStudent = function(req, res) {
    //var tenant =req.user.username ;
    console.log("user : ", req.user);
    var tenant = req.query.tenant;
    var enrollment_no = req.query.enrollment_no;
    var callback = req.query.callback;
    var semester = req.query.semester;
    var email = req.query.email;
    var roll_no = req.query.roll_no;
    console.log(req.query);
    console.log("tenant : " + tenant + " enrollment_no : " + enrollment_no + " semester : " + semester);
    var response = {};
    if (!tenant || !enrollment_no || !semester) {
        response.status = "error";
        response.message = "Required Params missing";
        if (callback) {
            res.jsonp(response);
        } else {
            res.json(response);
        }
    } else {
        var queryObj = {
            'enrollment_no': enrollment_no,
            'status': 'Not Paid'
        };
        if (semester) {
            queryObj.semester = semester;
        }
        if (email) {
            queryObj.email = email;
        }
        dbUtil.getConnection(function(db) {
            var collection = db.collection('T_' + tenant + '_userdetails');
            collection.findOne(queryObj, (err, result) => {
                if (err) {
                    response.status = "error";
                    response.message = "Something broken !!"
                    if (callback) {
                        res.jsonp(response);
                    } else {
                        res.json(response);
                    }
                } else {
                    response.status = "success";
                    response.data = result;
                    if (callback) {
                        res.jsonp(response);
                    } else {
                        res.json(response);
                    }
                }
            });
        });
    }
};
exports.deletedata = function(req, res) {
    var enId = req.body;
    dbUtil.getConnection(function(db) {
        var tenant = req.user.username;
        var collection = db.collection('T_' + tenant + '_userdetails');
        var bulk = collection.initializeUnorderedBulkOp();
        for (var idx = 0; idx < enId.length; idx++) {
            var selIdx = enId[idx].split(":");
            var enrollment_no = selIdx[0];
            var semester = selIdx[1];
            bulk.find({
                enrollment_no: enrollment_no,
                semester,
                semester
            }).remove();
        }

        var results = bulk.execute(function(err, results) {
            console.log("build results", results);
            res.json({
                "message": "success",
                docs_failed: results.writeErrors,
                docs_deleted: results.nRemoved
            });
        });


        /* collection.remove({
             'enrollment_no': {
                 '$in': enId
             }
         }, function(err, result) {
             if (err) {
                 res.json({
                     "message": "error"
                 });
             }
             res.json({
                 "message": "success"
             });
         });*/
    });
};
exports.gettenant = function(req, res) {
    console.log("gettenant = " + req.user.username);
    var tenant = req.user.username;
    if (req.user.role == "STUDENT")
        tenant = req.user.tenant;
    res.json({
        "tenant": tenant
    });
}



function makeDirRecursively(baseDir, subdirs) {
    var subdirArr = subdirs.split('/');
    var currentDir = baseDir;
    subdirArr.forEach(function(subDir) {
        currentDir = currentDir + "/" + subDir;
        if (!fs.existsSync(currentDir)) {
            fs.mkdirSync(currentDir);
        }
    });
}

function getFileExtn(fileName) {
    var lastdotIndex = fileName.lastIndexOf(".");
    if (lastdotIndex != -1) {
        return fileName.substring(lastdotIndex + 1);
    }
    return "png";
}
var logoStorage = multer.diskStorage({
    destination: function(req, file, cb) {
        var tenant = req.body.tenant;
        if (!tenant) {
            cb(new Error("Tenant missing !"));
        } else {
            var dir = './public/' + tenant;
            makeDirRecursively("./public", "/" + tenant);

            cb(null, dir);
        }
    },
    filename: function(req, file, cb) {
        cb(null, "logo." + getFileExtn(file.originalname));
    }
});

var logoUpload = multer({ //multer settings
    storage: logoStorage
}).single("logo");


var buildLogoURL = function(tenant, file) {
    return "/" + tenant + "/logo." + getFileExtn(file.originalname);
};

exports.updateTenant = function(req, res) {

    console.log("body tenant", req.body);
    var logoURL;
    var updateObject = {};
    logoUpload(req, res, function(err) {
        //console.log("Fields ", req.fields);
        var tenant = req.body.tenant;
        console.log('======>>', tenant);

        if (err) {
            res.status(501).send("Failed to upload the logo");
            return;
        } else if (req.file) {
            logoURL = buildLogoURL(tenant, req.file);
            console.log("logoURL", logoURL);
            updateObject.logoURL = logoURL;
        }

        if (req.body.headerCSS) {
            updateObject.headerCSS = req.body.headerCSS;
        }
        if (req.body.college) {
            updateObject.college = req.body.college;
        }
        if (req.body.addressLine1) {
            updateObject.addressLine1 = req.body.addressLine1;
        }
        if (req.body.addressLine2) {
            updateObject.addressLine2 = req.body.addressLine2;
        }

        dbUtil.getConnection(function(db) {
            db.collection("accounts").update({
                username: tenant
            }, {
                $set: updateObject
            }, function(err, updResult) {
                if (err) {
                    res.status(501).send("Failed to update the tenant details.");
                } else {
                    res.send("Tenant details updated");
                }

            });
        });

    });
};

var xlsStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        var tenant = req.body.tenant;
        if (!tenant) {
            cb(new Error("Tenant missing !"));
        } else {
            var dir = './public/' + tenant;
            makeDirRecursively("./public", "/" + tenant);

            cb(null, dir);
        }
    },
    filename: function(req, file, cb) {
        cb(null, "validaion." + getFileExtn(file.originalname));
    }
  })
   
  var xlsUpload = multer({ storage: xlsStorage}).single('validationFile')

  exports.validationList = function(req, res) {

    console.log("body tenant", req.body);
    var fileURL;
    var updateObject = {};
    var exceltojson;
		
    xlsUpload(req, res, function(err) {
        //console.log("Fields ", req.fields);
        var tenant = req.body.tenant;
        if (err) {
            res.status(501).send("Failed to upload the logo");
            return;
        }

        if (req.body.validationField) {
            updateObject.validationField = req.body.validationField;
        }
        if (req.body.formUrl) {
            updateObject.formUrl = req.body.formUrl;
        }
        if (req.body.tenant) {
            updateObject.tenant = req.body.tenant;
        }
        if (req.body.message) {
            updateObject.message = req.body.message;
        }
        
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }

        var workbook = xlsx.readFile(req.file.path);
			var sheets = workbook.SheetNames;
            var tenant = req.body.tenant;
            

        function saveSheetData(index, sheets, updateObject , cb) {
            try {
                if (!(index < sheets.length)) {
                    return cb(null, true);
                }
                var sheetName = sheets[index];

                exceltojson({
                    input: req.file.path,
                    output: null,
                    sheet: sheetName,
                    lowerCaseHeaders: true
                }, function(err, result) {

                    if (err) {
                        return cb(err, null)
                    } else {
                         dbUtil.getConnection(function(db) {

                            db.collection("validation").update({
                                formUrl: updateObject.formUrl
                            },
                             {
                                tenant: updateObject.tenant,
                                validationField: updateObject.validationField,
                                formUrl: updateObject.formUrl,
                                validationList: result,
                                message: updateObject.message
                            },
                            {upsert: true},
                             function(err, updResult) {
                                if (err) {
                                    res.status(501).send("Failed to update the tenant Validations.");
                                } else {
                                    res.send("Validations updated");
                                }
                            });
                        });
                    }
                });
            } catch (e) {
                res.send('exception: ', e);
            }
        }

        saveSheetData(0, sheets, updateObject, function(err, data) {
            if (err) {
                res.send('err: ', err);
            }
            if (data) {
                res.send(data);
            }
        })
    });
};


exports.fetchTenantDetails = function(req, res) {
    var tenant = req.query.tenant;
    if (!tenant) {
        res.status(403).send("Bad request");
    }
    dbUtil.getConnection(function(db) {
        var collection = db.collection('accounts');
        collection.findOne({
            username: tenant
        }, {
            salt: 0,
            hash: 0,
            enforce_paymethod: 0
        }, function(err, data) {
            res.json(data);
        })
    });
};

exports.name = function(req, res) {
    res.json({
        name: 'Bob'
    });
};
exports.metadata = function(req, res) {
    console.log("token inside metadata: " + req.user.token);
    var folder = "F:/AK/Projects/COMET/NodeProject/LGKPortal/";
    var name = req.params.name;
    var obj = JSON.parse(fs.readFileSync(folder + name + '.json', 'utf8'));
    res.json(obj);
};
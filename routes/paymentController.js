var dbUtil = require('../config/dbUtil');
var payU = require('../utils/payUUtils');
var payUConfig = require('../config/payU');
var mobiKwik = require('../utils/mobiUtils');
var mobiConfig = require('../config/mobiConf');
var Transaction = require('../models/transaction');
var ctx = require('../config/ctxconfig');
var html2pdf = require('html-pdf');
var emaiUtil = require('../utils/emailUtil');
var feeSvc = require('../service/feeSvc');
var userSvc = require('../service/userDetailsSvc');

var q = require('q');
const pug = require('pug');
var max_retry = 3;
Date.prototype.format = function() {
	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var dateStr = this.getDate() + ' ' + months[this.getMonth()] + ', ' + this.getYear() % 100;
	return dateStr;
}

var PaymentMode = {
	CC: "Credit Card",
	DC: "Debit Card",
	NB: "Net Banking"
}
var PaymentStatus = {
	init: "initiated",
	cancel: "cancelled",
	success: "success",
	failure: "failure"
};
module.exports.controller = function(app) {
	function calculateFee(data) {
		var fee = parseFloat(data.admission_fee || 0) + parseFloat(data.tuition_fee || 0) + parseFloat(data.hostel_fee || 0) + parseFloat(data.mess_fee || 0) + parseFloat(data.lab_fee || 0) + parseFloat(data.late_fee || 0) + parseFloat(data.other_fee || 0) + parseFloat(data.others || 0);
		return fee;
	}


	var __delta = 0.5;

	function validateTranx(trans, tenant) {
						
		var validPromise = q.defer();

		if (!trans.fee_for || trans.fee_for != "misc") {
			dbUtil.getConnection(function(db) {
				var queryObj = {
					'enrollment_no': trans.udf_1,
					'semester': trans.udf_2
				};
				db.collection('T_' + tenant + '_userdetails').findOne(queryObj, function(err, data) {
					if (data.status == 'Not Paid') {
						var fee = calculateFee(data);
						if (Math.abs(fee - trans.amount) > __delta) {
							validPromise.reject(false);
						} else {
							validPromise.resolve(true);
						}
					}
					validPromise.reject(false);
				});
			});
		} else if (trans.fee_for == "misc") {
			feeSvc.getStudentFee(tenant, {
				fee_id: trans.fee_id
			}).then((fee) => {
				if (fee.fee_status == "UNPAID" || fee.fee_status == "PARTIALY_PAID") {
					feeSvc.getFee(tenant, {
						fee_id: fee.fee.fee_id
					}).then((masterFee) => {
						if (masterFee.freezed == true)
							validPromise.reject("Freezed ..");
						if (!masterFee.partial_payment || masterFee.partial_payment != true) {

							var fee = calculateMissceleniousFee(masterFee.fees);
							if (Math.abs(fee - trans.amount) > __delta) {
								validPromise.reject("amount validation failed");
							} else {
								validPromise.resolve(true);
							}
						}else{
							validPromise.resolve(true);
						}
					}, (err) => {
						console.log("reject 22");
						validPromise.reject(err);
					});
				} else {
					validPromise.reject("Already paid");
				}
			}, (err) => {
				validPromise.reject(err);
			});
		}
		return validPromise.promise;
	};

	function calculateMissceleniousFee(stuFees) {
		var amount = 0;
		stuFees.forEach((item) => {
			amount += item.fee_amount;
		});
		return amount;
	}

	var txnSaveRetry = function(trans, maxRetry, retryCnt, req, res) {
		trnxId = payU.generateRandom(8);
		trans.trnxid = trnxId;
		console.log('trans::::::::::', trans)
		trans.save(function(err, trnx) {
			var response = {};
			if (err) {
				if (maxRetry < retryCnt) {
					response.message = "Error occured while generating transaction Id";
					response.status = "error";
					response.data = "";
					res.json(response);
					//return;
				}
				retryCnt = retryCnt + 1;
				txnSaveRetry(trans, maxRetry, retryCnt, req, res);
			} else {
				response.status = "success";
				response.message = "";
				response.data = trans;
				res.json(response);
			}
		});

	};
	/**
	    Initiate the transaction 
	    @return the saved transaction
	*/
	app.post(ctx.path_context + "/ctrnx", function(req, res) {
		var tenant = req.query.tenant || req.body.tenant;
		var action_user = req.body.action_user;
		var email = req.body.email
			
		if (!action_user || !action_user.userid) {
			if (req.user && req.user.role == 'STUDENT') {
				action_user = {
					id: req.user.userid,
					email: req.user.username,
					name: req.user.firstnames
				};
			}
		}
		
		console.log('req.body.fathername:: ', req.body.father_name)
		var trans = new Transaction({
			tenant: tenant,
			amount: req.body.amount,
			email: email,
			first_name: req.body.firstname,	
			last_name: req.body.lastname,
			fatherName: req.body.father_name,
			description: req.body.description,
			phone: req.body.phone,
			status: 'Initiated',
      		uniqueId: req.body.uniqueId,
			udf_1: req.body.enrollment_no || req.body.en_no,
			udf_2: req.body.semester,
			year: req.body.year,
			action_user: action_user,
			fee_for: req.body.fee_for, 
			fee_id: req.body.fee_id,
			member:req.body.member,
			PG_name: req.body.PG_name,
			fee_type: req.body.description
		});
		var response = {};
		console.log('trans### ', trans)
		
		validateTranx(trans, tenant).then((valid) => {
			payUConfig.payUConfig().then((config) => {
				trans.additional_charges = payU.calculateAddionalCharges(trans, config);
				var retryCnt = 0;
				var trnxId = 0;
				var max_retry = max_retry;

				userSvc.findTenantUser(tenant).then((tetantDetails)=>{
					if(tetantDetails.enforce_paymethod){
						trans.enforce_paymethod = tetantDetails.enforce_paymethod;
					}
					txnSaveRetry(trans, max_retry, retryCnt, req, res);

				},(err)=>{

				});

			}, function(err) {

				response.message = "Error occured at transaction Id creation";
				response.status = "error";
				response.data;
				res.json(response);
			});
		}, (err) => {
			console.log("error", err);
			response.message = "Error occured at transaction Id creation";
			response.status = "error";
			response.data;
			res.json(response);
		});
		//console.log(isValid);


	});
	app.get(ctx.path_context + "/initMobiPay", function(req, res) {
		var trnxid = req.query.trnxid;
		var payment_mode = req.query.paymentMode;
		dbUtil.getConnection(function(db) {
			var collection = db.collection('transactions');
			collection.findOne({
				trnxid: trnxid
			}, {
				pg_response: 0
			}, function(err, docs) {
				if (err) {
					console.log(err)
				}
				if (!docs) {
					console.log("mobiConfig :#: 404 ");
					res.status(404).send("Invalid Transaction");
					//return;
				}
				console.log("payment_mode:  ", payment_mode)
				mobiConfig.mobiConfig().then((config) => {  
					docs.extra5 = mobiKwik.getAddionalCharges(docs, config, payment_mode);
					docs.currency = config.currency;
					docs.merchantIdentifier = config.merchantIdentifier;
					docs.secret_key = config.secret_key;
					docs.returnUrl = config.returnUrl;	
					docs.txnType = config.txn_type[payment_mode].value;
					docs.currency = config.currency;
					docs.orderId = trnxid;
					docs.amount = docs.amount * 100;
					docs.hash = mobiKwik.createHMAC(docs.amount, docs.email, docs.first_name, docs.phone,
						config.currency, config.merchantIdentifier, trnxid, config.secret_key, config.returnUrl, 
						config.txn_type[payment_mode].value, docs.udf_1, docs.udf_2 , docs.tenant ,
						docs.fee_type, docs.extra5);
					
					console.log("Hash:  ", docs.hash)
					collection.update({
						_id: docs._id
					}, {
						$set: {
							status: "PaymentInitiated"
						}
					}, function(err, ressult) {
						res.render("mobiPay/mobiPay", docs);
					});

				}, (err) => {
					res.status(500).send("something went wrong");
				});

			});
		});
	});
	app.get(ctx.path_context + "/initpay", function(req, res) {
		//console.log("payUConfig :: ", payUConfig);

		var trnxid = req.query.trnxid;
		dbUtil.getConnection(function(db) {
			var collection = db.collection('transactions');
			collection.findOne({
				trnxid: trnxid
			}, {
				pg_response: 0
			}, function(err, docs) {
				if (err) {
					console.log(err)
				}
				if (!docs) {
					res.status(404).send("Invalid Transaction");
					//return;
				}
				payUConfig.payUConfig().then((config) => {
					console.log('config=>',config);
					docs.additional_charges = payU.calculateAddionalCharges(docs, config);
					docs.curl = config.curl;
					docs.furl = config.furl;
					docs.surl = config.surl;
					docs.key = config.key;
					docs.aurl = config.aurl;
					docs.hash = payU.createHash(true, trnxid, docs.amount, docs.description, docs.first_name, 
						docs.email, docs.udf_1, docs.udf_2, docs.tenant, null, null, docs.additional_charges, 
						config.key, config.salt);
					collection.update({
						_id: docs._id
					}, {
						$set: {
							status: "PaymentInitiated"
						}
					}, function(err, ressult) {
						
					console.log('docs=>',docs);
						res.render("payu/payu", docs);
					});

				}, (err) => {
					res.status(500).send("something went wrong");
				});

			});
		});
	});
	app.post(ctx.path_context + "/payucallback", function(req, res) {

		var payUResponse = req.body;
		var txnid = payUResponse.txnid;

		//req.user ={username:"hiiibdh@gmail.com"};
		//console.log("Response = ",payUResponse);
		var tenant = payUResponse.udf3;
		payUResponse.key = null; // don't save key to database
		payUResponse.hash = null;
		var obj = {
			txnid: txnid,
			status: payUResponse.status
		};
		if (req.user) {
			obj.role = req.user.role;
		}else{
			obj.role = "guest";
		}

		dbUtil.getConnection(function(db) {
			var trnsCollection = db.collection('transactions');

			trnsCollection.findOne({
				trnxid: txnid
			}, function(err, txnObj) {
				if(txnObj.member){
					obj.member=txnObj.member;
				}
				
				if (err) {
					console.log(err);
					obj.status = "failure";
					res.render("payu/blank", obj);
				} else {
					trnsCollection.update({
						trnxid: txnid
					}, {
						$set: {
							status: payUResponse.status,
							pg_response: payUResponse
						}
					}, function(err, updResult) {
						if (err) {
							console.log(err);
							obj.status = "failed";
							res.render("payu/blank", obj);
						} else {
							if (payUResponse.status == "success") {

								if (!txnObj.fee_for || txnObj.fee_for != "misc") {
									dbUtil.getConnection(function(db) {
										var userDetailsCollection = db.collection('T_' + tenant + '_userdetails');
										var userQuery = {
											'enrollment_no': payUResponse.udf1,
											'semester': payUResponse.udf2
										};
										userDetailsCollection.update(userQuery, {
											$addToSet: {
												"trnxid": txnid
											},
											$set: {
												status: "Paid",
												txnid: txnid,
												payment_date: txnObj.created_on
											}
										}, (err, result) => {
											if (err) {
												console.log(err);
												obj.status = "failed";
												res.render("payu/blank", obj);

											} else {

												sendInvoiceOverEmail(txnObj);
												res.render("payu/blank", obj);
											}
										});
									});
								} else if (txnObj.fee_for && txnObj.fee_for == "misc") {
									feeSvc.getStudentFee(txnObj.tenant, {
										fee_id: txnObj.fee_id
									}).then((stuFee) => {
										stuFee.fee_status = "PAID";
										stuFee.fee.fees.forEach(function(fee) {
											fee.fee_status = "PAID";
										});
										if(stuFee.fee.partial_payment && stuFee.fee.partial_payment ==true){
											stuFee.fee_status = "PARTIALY_PAID";
										}

										stuFee.txnid = txnid;
										stuFee.payment_date = txnObj.created_on;

										feeSvc.updateStudentFee(txnObj.tenant, {
											fee_id: stuFee.fee_id
										}, stuFee);
										sendInvoiceOverEmail(txnObj);
										res.render("payu/blank", obj);

									}, (err) => {
										obj.status = "failed";
										// handle error
										res.render("payu/blank", obj);
									});
								}
							} else {
								console.log("failure");
								obj.status = "failed";
								res.render("payu/blank", obj);
							}
						}
					});
				}
			});

		});
	});

	var compiledInvoice = pug.compileFile('./views/payu/invoice.pug');
	var compiledMissInvoice = pug.compileFile('./views/payu/miscinvoice.pug');

	var getCompiledInvoice = (invoice) => {
		if (invoice.fee_for && invoice.fee_for == "misc") {
			invoice.fee_name = invoice.fees[0].fee_name;
			invoice.fee_amount = invoice.fees[0].fee_amount;
			invoice.fee_amount = invoice.fee_amount;

			return compiledMissInvoice(invoice);
		} else {
			return compiledInvoice(invoice);
		}
	}

	function sendInvoiceOverEmail(txnObj) {
		dbUtil.getConnection(function(db) {
			var trnsCollection = db.collection('transactions');

			trnsCollection.findOne({
				_id: txnObj._id
			}, (err, txn) => {
				if (err) {
					console.log("Mail Sending Error ", err);
				} else {

					saveInvoice(txn).then(function(invoice) {
						var html = getCompiledInvoice(invoice);
						var subject = "Payment SuccessFul | TxnId : " + txn.pg_response.txnid;
						var emailData = {
							from: "noreply@i-made.in",
							body: html,
							subject: subject,
							to: txn.pg_response.email
						};
						emaiUtil.sendEmail(emailData);
					});
				}
			});
		});
	}

	function createInvoiceObj(txn) {



		return new Promise((resolve, reject) => {


			dbUtil.getConnection(function(db) {
				var transCollection = db.collection("transactions");
				transCollection.findOne({
					trnxid: txn.trnxid
				}, function(err, txn) {


					if (!err) {
						var invObj = {
							name: txn.pg_response.firstname
						};


						invObj.phone = txn.pg_response.phone;

						invObj.created_on = new Date(txn.created_on).format();

						invObj.udf_1 = txn.udf_1;
						invObj.amount = txn.amount;

						invObj.additional_charges = txn.pg_response.additionalCharges;

						invObj.txnid = txn.trnxid;
						invObj.invoiceId = payU.generateRandom(5);

						invObj.action_user = txn.action_user;

						invObj.fee_for = txn.fee_for;
						invObj.fee_id = txn.fee_id;

						invObj.mode = PaymentMode[txn.pg_response.mode];

						console.log("outside misc")

						if (txn.fee_for && txn.fee_for == "misc") {

							feeSvc.getStudentFee(txn.tenant, {
								fee_id: txn.fee_id
							}).then((stuFee) => {
								//invObj.total_fee = calculateMissceleniousFee(stuFee.fee.fees);

								invObj.fees = stuFee.fee.fees;
								invObj.total_fee = txn.amount;
								invObj.email = stuFee.email;
								invObj.stream = stuFee.stream;
								invObj.course = stuFee.course;

								invObj.year = stuFee.year;
								invObj.semester = stuFee.semester;

								invObj.enrollment_no = stuFee.enrollment_no;
								console.log("misc")
								resolve(invObj);
							}, (err) => {
								reject(err);
							})
						} else {



							dbUtil.getConnection(function(db) {
								var collection = db.collection('T_' + txn.tenant + '_userdetails');
								collection.findOne({
									enrollment_no: txn.pg_response.udf1,
									semester: txn.pg_response.udf2
								}, function(err, user) {
									if (err) {
										reject(err);
									} else {
										//invObj.fees = stuFee.fee.fees;

										invObj.mess_fee = user.mess_fee;
										invObj.tuition_fee = user.tuition_fee;
										invObj.lab_fee = user.lab_fee;
										invObj.admission_fee = user.admission_fee;
										invObj.other_fee = user.other_fee;
										invObj.others = user.others;
										invObj.late_fee = user.late_fee;
										invObj.hostel_fee = user.hostel_fee;
										invObj.total_fee = txn.amount;
										invObj.name = user.name;

										invObj.email = user.email;
										invObj.stream = user.stream;
										invObj.course = user.course;

										invObj.year = user.year;
										invObj.semester = user.semester;

										invObj.enrollment_no = user.enrollment_no;
										resolve(invObj);
									}
								});
							});
						}

					} else {
						reject(err);
					}
				})

			})
		});
	}


	function saveInvoice(txn) {

		var invDeffered = q.defer();
		var saveInvoiceToDB = function(invObj, currentRetry, maxRetry) {
			if (currentRetry > maxRetry) {
				invDeffered.reject("Something went wrong");
				return;
			}
			invObj.invoiceId = payU.generateRandom(5);
			invObj.uniqueId = txn.uniqueId
			invObj.tenant = txn.tenant
			invObj.fatherName = txn.fatherName
			
			dbUtil.getConnection(function(db) {
				var invoiceCollection = db.collection("invoices");
				invoiceCollection.save(invObj, function(err, invoice) {
					if (err) {
						//invDeffered.reject(err);
						currentRetry = currentRetry + 1;
						saveInvoiceToDB(invObj, currentRetry, maxRetry);
					} else {
						invDeffered.resolve(invObj);
					}
				});
			});
		};

		dbUtil.getConnection(function(db) {
			var invoiceCollection = db.collection("invoices");
			
			invoiceCollection.findOne({
				txnid: txn.trnxid
			}, function(err, invoice) {
				if (!invoice) {
					createInvoiceObj(txn).then((invObj) => {
						dbUtil.getConnection(function(db) {
							var accountColl = db.collection("accounts");
							accountColl.findOne({
								username: txn.tenant,
								role: "TENANT"
							}, function(err, tenantUser) {
								invObj.company_name = tenantUser.college;
								invObj.addressLine1 = tenantUser.addressLine1;
								invObj.addressLine2 = tenantUser.addressLine2;
								invObj.city = tenantUser.city;
								var currentRetry = 0;
								console.log('saveInvoiceToDB3:::::::::::::::::::',txn.subject2)
								saveInvoiceToDB(invObj, currentRetry, max_retry);
							});
						});

					}, (err) => {
						invDeffered.reject(err);
					});



				} else {
					invDeffered.resolve(invoice);
				}

			});

		});
		return invDeffered.promise;
	};

	app.get(ctx.path_context + "/invoicehtml", (req, res) => {
		var txnid = req.query.txnid;
		if (!txnid) {
			res.status(403).json({
				message: "Transaction Id Missing",
				status: "error"
			});

		}
		dbUtil.getConnection((db) => {
			var invCollection = db.collection('invoices');
			invCollection.findOne({
				txnid: txnid
			}, (err, invoice) => {
				if (err) {
					res.status(503).send("Something went wrong !");
				} else {
					var html = getCompiledInvoice(invoice);
					res.status(200).send(html);
				}

			});
		});

	});


	app.get(ctx.path_context + "/invoice", function(req, res) {

		var txnid = req.query.txnid;
		if (!txnid) {
			res.status(403).json({
				message: "Transaction Id Missing",
				status: "error"
			});

		}
		
		dbUtil.getConnection(function(db) {dbUtil.getConnection(function(db) {
			var trnxCollection = db.collection('transactions');
			trnxCollection.findOne({
			  trnxid: txnid
			}, function(err, txn) { //console.log("TXN :: " + JSON.stringify(txn));
			  if (!txn || txn.status != "success") {
				res.status(403).send("Transaction was failed");
			  }
			  		  
				var collection = db.collection(txn.tenant + '_fee_student');
				collection.findOne({
					txnid: txnid,
				}, function(err, userDetails) {
					if (err) {
					console.log("Payment | Error | ", err);
					res.status(503).send("User not found in system");
					}
				
				saveInvoice(txn).then(function(invoice) {
				if(userDetails)	{
					if(txn.tenant == 'MMC'){
						invoice.subject1 = userDetails.subject_ba_1
						invoice.subject2 = userDetails.subject_ba_2
						invoice.subject3 = userDetails.subject_ba_3
						}
	
					if(txn.tenant == 'LaxmiBai'){
						invoice.collegeId = userDetails.uniqueId
						invoice.enrollNo = userDetails.en_no
					}	
					invoice.year = userDetails.year || userDetails.adm_year
				}
				
				var html = getCompiledInvoice(invoice);
				html2pdf.create(html).toStream(function(err, stream) {
							if (err) {
							console.log(err);
							res.send(err);
							return;
							} else {
							stream.pipe(res);
							stream.on('end', function() {
								console.log('inside invoice stream end');
								res.end();
							});
							}
						});
					}).catch(function(err) {
					res.status(500).send(err);
					});
				});
			});
		  });
		});  
	});

	app.get(ctx.path_context + "/txns", function(req, res) {
		var userId = req.query.user_id;

		var response = {};
		if (!userId && req.user) {
			userId = req.user.userid;
		}
		if (!userId) {
			response.status = 'error';
			response.message = 'something went wrong';
			res.json(response);
			return;
		}
		var queryObj = {
			"action_user.id": userId,
			"status": "success"
		};
		dbUtil.getConnection(function(db) {
			var trnxCollection = db.collection('transactions');
			trnxCollection.find(queryObj).toArray(function(err, txns) {
				if (err) {
					response.status = 'error';
					response.message = 'something went wrong';
				} else {
					response.status = 'success';
					response.data = txns;
				}
				res.json(response);
			});
		});
	});
	app.get(ctx.path_context + "/tenant/txns", function(req, res) {
		var userId = req.query.tenant;

		var response = {};

		if (!userId) {
			userId = req.user.username;
		}

		if (!userId) {
			response.status = 'error';
			response.message = 'something went wrong';
			res.json(response);
		}
		dbUtil.getConnection(function(db) {
			var trnxCollection = db.collection('transactions');
			trnxCollection.find({
				"tenant": tenant
			}).toArray(function(err, txns) {
				//txns = dbUtil.walkCursor(txns);
				if (err) {
					response.status = 'error';
					response.message = 'something went wrong';
				} else {
					response.status = 'success';
					response.data = txns;
				}
				res.json(response);
			});
		});
	});



	app.get(ctx.path_context + "/txn", function(req, res) {
		var txnid = req.query.txnid;
		var response = {};
		dbUtil.getConnection(function(db) {
			var trnxCollection = db.collection('transactions');

			trnxCollection.findOne({
				trnxid: txnid
			}, {
				"pg_response.hash": 0,
				"pg_response.card_num": 0,
				"pg_response.field1": 0,
				"pg_response.field2": 0,
				"pg_response.field3": 0
			}, function(err, txn) {
				//txns = dbUtil.walkCursor(txns);
				if (err) {
					response.status = 'error';
					response.message = 'something went wrong';
				} else {
					response.status = 'success';
					response.data = txn;
				}
				res.json(response);
			});
		});
	});
}
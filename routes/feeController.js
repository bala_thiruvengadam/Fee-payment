var dbUtil = require('../config/dbUtil');
var payU = require('../utils/payUUtils');
var payUConfig = require('../config/payU');
var Transaction = require('../models/transaction');
var express = require('express'),
	ensureLoggedIn = require('../auth/ensureloggedin');
var feeSvc = require('../service/feeSvc');
var userDetailsSvc = require('../service/userDetailsSvc');
var ctx = require('../config/ctxconfig');
var json2xls = require('json2xls');
var fs = require('fs');
const pug = require('pug');
var html2pdf = require('html-pdf');
const multer = require('multer');

module.exports.controller = function(app) {
	//app.use(ctx.path_context + "/tenant/fees", feeRouter);
	//	app.use(, stuFeeRouter);
	var buildUserQuery = function(query) {
		query = query || {};
		var resQuery = {};
		var queryField = "fee.created_on";
		if (query.startDate && query.endDate) {
			resQuery[queryField] = {
				$gte: query.startDate,
				$lte: query.endDate
			};
		} else if (query.endDate) {
			resQuery[queryField] = {
				$lte: query.endDate
			};
		} else if (query.startDate) {
			resQuery[queryField] = {
				$gte: query.startDate
			};
		}
		if (query.status) {
			resQuery.status = query.status;
		}
		return resQuery;
	}


	app.get(ctx.path_context + "/tenant/miscfees", function(req, res) {

		var q = JSON.parse(req.query.q);
		//console.log("q",q);
		var userQuery = q;

		if (!userQuery.tenant)
			res.status(403).send("Tenant Id is missing");

		feeSvc.getFees('misc-dash', userQuery.tenant, userQuery).then(function(fees) {
			res.send(fees);
		}, function(err) {
			res.status(501).send(err);
		});
	});

	app.get(ctx.path_context + "/tenant/miscfees/fee", function(req, res) {
		if (!req.query.tenant)
			res.status(403).send("Tenant Id is missing");
		feeSvc.getFee(req.query.tenant, {
			fee_id: req.query.fee_id
		}).then(function(fees) {
			res.send(fees);
		}, function(err) {
			res.status(501).send(err);
		});
	});



	app.post(ctx.path_context + "/tenant/miscfees/save", function(req, res) {
		if (!req.body.tenant) {
			res.status(403).send("Tenant Id is missing");
		}

		if (req.body._id) {
			feeSvc.updateFee(req.body.tenant, {
				fee_id: req.body.fee_id
			}, req.body).then(function(result) {
				res.send(result);

			}, function(err) {
				//console.log(err);
				res.status(501).send(err);
			});
		} else {
			feeSvc.createFee(req.body).then(function(result) {
				res.send(result);

			}, function(err) {
				//console.log(err);
				res.status(501).send(err);
			});
		}
	});

	app.post(ctx.path_context + "/tenant/miscfees/delete", function(req, res) {
		if (!req.body.tenant || !req.body.fee_id) {
			res.status(403).send("Bad request : tenant or fee id missing");
		}

		//console.log("body req", req.body);
		feeSvc.deleteFee(req.body.tenant, req.body).then(function(result) {
			res.send(result);
		}, function(err) {
			res.status(501).send(err);
		});
	});

	app.post(ctx.path_context + "/tenant/miscfees/deletemany", function(req, res) {
		if (!req.body.tenant || !req.body.fee_ids) {
			res.status(403).send("Bad request : tenant or fee id missing");
		}

		feeSvc.deletemany(req.body.tenant, req.body.fee_ids).then(function(result) {
			res.send(result);

		}, function(err) {
			res.status(501).send(err);
		})
	});

	app.post(ctx.path_context + "/tenant/miscfees/update", function(req, res) {
		if (!req.body.tenant || !req.body.fee_id) {
			res.status(403).send("Bad request : tenant or fee id missing");
		}

		feeSvc.updateFee(req.body.tenant, {
			feed_id: req.body.fee_id,
			tenant: req.body.tenant
		}, req.body).then(function(result) {
			res.send(result);
		}, function(err) {
			res.status(501).send(err);
		})
	});

	app.post(ctx.path_context + "/miscfees/save", function(req, res) {
		//console.log("request received to save", req.body);
		if (!req.body.tenant) {
			res.status(403).send("Tenant Id is missing");
		}
		
		feeSvc.saveStudentFee(req.body.tenant, req.body).then(function(result) {
			res.send(result);

		}, function(err) {
			//console.log(err);
			res.status(501).send(err);
		});
	});

	app.post(ctx.path_context + "/validationList", function(req, res) {
		var query = req.body.url.split('feepayment')[1];
		var tenant = req.body.tenant
		console.log("formUrl", query);

		dbUtil.getConnection(function(db) {
			var collection = db.collection('validation');
			
			collection.findOne({
				formUrl: new RegExp(query),
				tenant: tenant
			}, (err, result) => {
				console.log('list >> ', result)
				if(result == null)
					res.send(null);
				else
					res.send(result);
				
				
			});
		});
	});


	app.get(ctx.path_context + "/tenant/miscfees/details", function(req, res) {
		var userQuery = JSON.parse(req.query.q);

		feeSvc.getMiscFeeDetails(userQuery.tenant, userQuery).then(function(miscFees) {
			res.send(miscFees);
		}, function(err) {
			res.status(501).send(err);
		});

	});


	app.get(ctx.path_context + "/tenant/regfees", function(req, res) {
		var userQuery = req.query.q;
		var q = JSON.parse(userQuery);

		feeSvc.reqularFeeSearch(q.tenant, q).then(function(miscFees) {
			res.send(miscFees);
		}, function(err) {
			res.status(501).send(err);
		});

	});



	app.get(ctx.path_context + "/tenant/regfees/download", function(req, res) {

		var userQuery = req.query.q;
		var q = JSON.parse(userQuery);
		q.pgSize = -1;
		q.pgNo = -1;

		feeSvc.reqularFeeSearch(q.tenant, q).then(function(miscFees) {


			res.xls("Regular_fee_details.xlsx", miscFees).catch((err) => {
				console.log(err);
			});
		}).catch(function(err) {
			console.log(err);
			res.status(501).send(err);
		});

	});


	var miscDetailsFields = ['fee_status', 'enrollment_no', 'name', 'father_name','course', 'year', 'semester', 'email', 'mobile', 'fee.fees[0].fee_name', 'fee_amount', 'txnid', 'payment_date'];
	var miscDetailsCaptions = ['Fee Status', 'Enrollment No', 'Name', 'Father Name', 'Course', 'Year', 'Semester', 'Email', 'Mobile', 'Fee Name', 'Fee Amount', 'Transaction Id', 'Transaction Date'];
	app.get(ctx.path_context + "/tenant/miscfees/details/download", function(req, res) {
		var userQuery = JSON.parse(req.query.q);
		userQuery.pgSize = -1;
		userQuery.pgNo = -1;

		feeSvc.getMiscFeeDetails(userQuery.tenant, userQuery).then(function(miscFees) {
			res.xls("misc_fee_" + userQuery.misc_fee_id + ".xlsx", miscFees, {
				fields: miscDetailsFields,
				captions: miscDetailsCaptions
			})
		}).catch(function(err) {
			//console.log(err);
			res.status(501).send(err);
		});

	});



	app.get(ctx.path_context + "/fees/miscfees", function(req, res) {
		if (!req.query.tenant) {
			res.status(403).send("Tenant param missing");
		}

		feeSvc.getFees('mfs',req.query.tenant, {
			tenant: req.query.tenant,
			freezed: false
		}).then(function(result) {
			res.send(result);
		}, function(err) {
			res.status(501).send(err);
		})
	});

	app.post(ctx.path_context + "/fees/update", function(req, res) {
		if (!req.body.tenant || !req.body.fee_id) {
			res.status(403).send("Bad request : tenant or fee id missing");
		}

		feeSvc.updateStudentFee(req.body.tenant, req.body).then(function(result) {
			res.send(result);
		}, function(err) {
			res.status(501).send(err);
		})
	});

	app.get(ctx.path_context + "/regUsers/:query", function(req, res) {
		var query = req.params.query;
		var formFor = query.split('_')[0]
		var tenant = query.split('_')[1]
		var adm =  query.split('_')[2]

		if (!tenant) {
			res.status(403).send("Tenant param missing");
		}
		
		var queryObj = {};
		
		if(formFor == "gadm" && adm == 'adm'){
			queryObj.form_for = "admission_gadm";
		}else if(formFor == "gadm"){
			queryObj.form_for = "admission";
		}else if(formFor == "gsdf"){
			queryObj.form_for = "registration";
		}
		
		if(formFor == "gmf"){
			userDetailsSvc.getAllGmfDetails(tenant,queryObj).then(function(report) {
				console.log("## report.length " + report.length);
				if (report.length == 0)
					return res.json({
						success: true,
						message: 'No data found for this tenant.'
					});
				var file = __dirname + '/../public/reg_user_details.xls';
	
				var writeStream = fs.createWriteStream(file);
				writeStream.write(report);
				writeStream.close();
				writeStream.on('close', function () {
					res.download(file);
				});
				writeStream.on('error', function () {
					res.json({
						success: false,
						message: 'Unable to get details.'
					});
				});
				
				//res.xls('data.xlsx', result);
			}, function(err) {
				res.status(501).send(err);
			})
		}
		else{
			var reportParam = query.substr(5)
			userDetailsSvc.getAllRegUsersDetail(tenant,queryObj,reportParam).then(function(report) {
				console.log("## report.length " + report.length);
				if (report.length == 0)
					return res.json({
						success: true,
						message: 'No data found for this tenant.'
					});
				
				var file = __dirname + '/../public/reg_user_details.xls';
	
				var writeStream = fs.createWriteStream(file);
				writeStream.write(report);
				writeStream.close();
				writeStream.on('close', function () {
					res.download(file);
				});
				writeStream.on('error', function () {
					res.json({
						success: false,
						message: 'Unable to get details.'
					});
				});
				
				//res.xls('data.xlsx', result);
			}, function(err) {
				res.status(501).send(err);
			})
		}
	});

	app.get(ctx.path_context + "/userDetails", function(req, res) {
		
		var txnid = req.query.txnid;
		console.log('txnid:: ', txnid)
		if (!txnid) {
			res.status(403).json({
				message: "Transaction Id Missing",
				status: "error"
			});
		}

		dbUtil.getConnection(function(db) {
			var trnxCollection = db.collection('transactions');
			trnxCollection.findOne({
				trnxid: txnid
			}, function(err, txn) { //console.log("TXN :: " + JSON.stringify(txn));
				if (txn.status != "success") {
					return res.status(403).send("Transaction was failed");
				}
				console.log('tenant:: ', txn.tenant )
				console.log('en_no:: ', txn.udf_1)
				console.log('semester:: ', txn.udf_2)

				var collection = db.collection(txn.tenant + '_fee_student');
				collection.findOne({
					
					semester: txn.udf_2,
					mobile: txn.phone
				}, function(err, user) {
					if (err) {
						console.log("Payment | Error | ", err);
						return res.status(503).send("User not found in system");
					}
					dbUtil.getConnection(function(db) {
						var accountColl = db.collection("accounts");
						accountColl.findOne({
							username: txn.tenant,
							role: "TENANT"
						}, function(err, tenantUser) {
							var pdfFor = txn.tenant
							if(user){
								user.company_name = (tenantUser.college != null) ? tenantUser.college + ',':'';
								user.addressLine1 = tenantUser.addressLine1 ? tenantUser.addressLine1 + ',':'';
								user.addressLine2 = tenantUser.addressLine2 ? tenantUser.addressLine2 + ',':'';
								user.city = tenantUser.city ? tenantUser.city :'';

								if(user.form_for == 'admission_gadm'){
									pdfFor = 'gadm_' + txn.tenant 
								}
							}

							console.log('pdfFor ', pdfFor)
							console.log('user ', user)
							
							var compiledUserDetail = pug.compileFile('./views/formPDF/userDetails_'+ pdfFor +'.pug');
							var html = compiledUserDetail(user);
							//var html = getCompiledUserDetails(user);
							html2pdf.create(html).toStream(function(err, stream) {
								if (err) {
									console.log(err);
									return res.send(err);
									
								} else {
									stream.pipe(res);
									console.log('piped');
									stream.on('end', function() {
										console.log('streaming end');
										res.end();
									});
								}
							});
						});
					});
				});
			});
		});
	});

	// var compiledUserDetails = pug.compileFile('./views/feepayment/userDetails.pug');
	// var getCompiledUserDetails = (user) => {
	// 	return compiledUserDetails(user);
	// }

};
const crypto = require('crypto');
const dbUtil = require('./../config/dbUtil');
const ctx = require('./../config/ctxconfig');

module.exports.controller = function (app) {
    app.get(ctx.path_context + '/wallet/mobikwik/getchecksum', function (req, res) {
        let salt = mobikwik_configs.secret_key;
        if (req.query.type == 'si') {
            salt = mobikwik_configs.secret_key_SI;
        }
        res.send(crypto.createHmac('sha256', salt)
            .update(req.query.key || '').digest('hex'));
    });
    app.post(ctx.path_context + '/wallet/mobikwik/redirectcallback', function (req, res) {
        var dataToBeInserted = req.body;
        var customStatus = 'failed';
        if (dataToBeInserted && dataToBeInserted.statuscode){
            if (dataToBeInserted.statuscode == '0') {
                customStatus = 'success';
            }
            dbUtil.getConn()
            .then(db => {
                dataToBeInserted.dateAdded = new Date();
                dataToBeInserted.customStatus = customStatus;
                return db.collection('mobikwikTransactions').insert(dataToBeInserted);
            })
            .then(()=>{
                res.redirect(ctx.path_context + '/wallet/mobikwik/redirectcallback' + '?customStatus=' + customStatus);
            })
            .catch(e => {
                res.send(e);
            });
        } else {
            res.redirect(ctx.path_context + '/wallet/mobikwik/redirectcallback' + '?customStatus=' + customStatus);
        }
    });
    app.get(ctx.path_context + '/wallet/mobikwik/redirectcallback', function (req, res) {
        res.end();
    });

}
var loginModule = angular.module('login',[]);

var loginCtrl = loginModule.controller('LoginCtrl',['$scope','$http',function($scope,$http){
	$scope.role='TENANT';
	$scope.fetchTenents=function(){
		$http({
			method:"GET",
			url:'/feepayment/fetchTenents',
		}).then(function(res){
			$scope.tenents = res.data.data;
		},function(err){
			console.log(err);
		});
	};
	$scope.getURL=function(){
		if($scope.role=='TENANT'){
			return "/feepayment/login/tenant";
		}
		if($scope.role=='STUDENT'){
			return "/feepayment/login/student";	
		}
	}
	$scope.fetchTenents();
}]);
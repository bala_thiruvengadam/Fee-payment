var guestApp = angular.module("guestApp", ['ngRoute', 'myApp.controllers', 'myApp.filters', 'feeApp', 'FeeModule', 'feeControllers','guestPayControllers', 'infinite-scroll']).config(['$routeProvider', '$httpProvider', '$locationProvider', function($routeProvider, $httpProvider, $locationProvider) {

	$routeProvider.when('/txnsuccess', {
		templateUrl: context + '/feepayment/guest-txn-success',
		controller: 'TransSummary',
		controllerAs: 'transSummary'
    });

    $routeProvider.when('/trxnsuccess', {
		templateUrl: context + '/feepayment/txnSuccess',
		controller: 'TransSummary',
		controllerAs: 'transSummary'
	});

	$routeProvider.when('/trxnfailure', {
		templateUrl: context + '/feepayment/txnFailure',
		controller: 'TransSummary',
		controllerAs: 'transSummary'
	});

	$routeProvider.when('/txnfailure', {
		templateUrl: context + '/feepayment/guest-txn-failure',
		controller: 'transFailure',
		controllerAs: 'transFailure'
	});
	
	$routeProvider.when("/gsd", {
		templateUrl: context + '/feepayment/guest-stu-details',
		controller: 'GuestDetailsCtrl'
	});

	$routeProvider.when("/mfs", {
		templateUrl: context + '/feepayment/guest-misc-fee-select',
		controller: 'MiscFeeSelCtrl'
	});
    $routeProvider.when("/gmf", {
        templateUrl: context + '/feepayment/guest-web-misc',
        controller: 'GuestDetailsCtrl'
    });
    
    $routeProvider.when("/gsdf/", {
        templateUrl: context + '/feepayment/guest-stu-detail_form',
        controller: 'GuestDetailsCtrl'
    });
    
    $routeProvider.when("/kmggpgc/", {
        templateUrl: context + '/feepayment/guest-stu-kmggpgc_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/ngie/", {
        templateUrl: context + '/feepayment/guest-stu-ngie_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/dpgitm/", {
        templateUrl: context + '/feepayment/guest-stu-dpgitm_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/mmc/", {
        templateUrl: context + '/feepayment/guest-stu-mmc_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/laxmibai/", {
        templateUrl: context + '/feepayment/guest-stu-lakshmi_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/gdc/", {
        templateUrl: context + '/feepayment/guest-stu-gdc_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/gadm", {
        templateUrl: context + '/feepayment/guest-stu-adm_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/gadm_laxmibai", {
        templateUrl: context + '/feepayment/guest-laxmibai-adm_form',
        controller: 'GuestDetailsCtrl'
    });
    
    $routeProvider.when("/gadm_siet", {
        templateUrl: context + '/feepayment/guest-siet-adm_form',
        controller: 'GuestDetailsCtrl'
    });

    $routeProvider.when("/student",{
		templateUrl: context + '/feepayment/student',
		controller: 'FeeCtrl',
		controllerAs: 'feeCtrl'
   });

   $routeProvider.when("/feedetails",{
    templateUrl: context + '/feepayment/fee_details',
	controller: 'FeeDetailsCtrl',
	controllerAs: 'fdCtrl'
    });

    $routeProvider.when('/paymethod', {
        templateUrl: context + '/feepayment/paymethod',
        controller: 'PayCtrl',
        controllerAs: 'payCtrl'
    });

	$httpProvider.defaults.timeout = 5000;
}]);


guestApp.directive('myEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

guestApp.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];
        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});
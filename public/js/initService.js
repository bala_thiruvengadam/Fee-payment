var initServiceMod = angular.module("initServiceMod", []);
initServiceMod.service('initService', ['$http','$rootScope' function($http,$rootScope) {
    var ctx = '/feepayment';
    var InitFn = function() {
        this.getTenant = function(role22) {
            $http.get(context + role22 + '/api/gettenant').success(function(data) {
                $rootScope.tenant = data;
            }).error(function(data) {
                alert("error");
            });
        };
        this.getCurrentUser = function(role22) {
            $http.get(context + role22 + '/api/getcurrentUser').success(function(data) {
                $rootScope.user = data;
            }).error(function(data) {
                alert("error");
            });
        };
    }
}]);
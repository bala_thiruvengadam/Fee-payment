'use strict';
// Declare app level module which depends on filters, and services
var context = '/feepayment';
var tenant = angular.element("#tenant").val();

var myApp = angular.module('myApp', ['ngRoute', 'myApp.controllers', 'myApp.userControllers', 'myApp.filters', 'myApp.services', 'myApp.directives', 'feeApp', 'FeeModule', 'UserModule', 'feeControllers','guestPayControllers', 'infinite-scroll']).
config(['$routeProvider', '$httpProvider', '$locationProvider', function($routeProvider, $httpProvider, $locationProvider) {

	$routeProvider.when('/home', {
		templateUrl: context + '/partials/home',
		controller: 'HomeCtrl'
	});
	$routeProvider.when('/login', {
		templateUrl: context + '/partials/login',
		controller: 'LoginCtrl'
	});
	$routeProvider.when('/upload', {
		templateUrl: context + '/partials/upload',
		controller: 'UploadCtrl'
	});
	$routeProvider.when('/edit', {
		templateUrl: context + '/partials/edit',
		controller: 'EditCtrl'
	});
	$routeProvider.when('/student', {
		templateUrl: context + '/feepayment/student',
		controller: 'FeeCtrl',
		controllerAs: 'feeCtrl'
	});
	$routeProvider.when('/feedetails', {
		templateUrl: context + '/feepayment/fee_details',
		controller: 'FeeDetailsCtrl',
		controllerAs: 'fdCtrl'
	});
	$routeProvider.when('/paymethod', {
		templateUrl: context + '/feepayment/paymethod',
		controller: 'PayCtrl',
		controllerAs: 'payCtrl'
	});
	$routeProvider.when('/txns', {
		templateUrl: context + '/feepayment/txns_history',
		controller: 'transHistory',
		controllerAs: 'transHistory'
	});
	$routeProvider.when('/txnsuccess', {
		templateUrl: context + '/feepayment/txn_success',
		controller: 'TransSummary',
		controllerAs: 'transSummary'
	});
	$routeProvider.when('/txnfailure', {
		templateUrl: context + '/feepayment/txn_failure',
		controller: 'transFailure',
		controllerAs: 'transFailure'
	});
	$routeProvider.when('/app/:appid/:pageid', {
		templateUrl: context + '/partials/app',
		controller: 'AppCtrl'
	});
	$routeProvider.when("/misc-dash", {
		templateUrl: context + '/feepayment/misc-dash',
		controller: 'MiscFeeDashCtrl'
	});
	$routeProvider.when("/misc-fee-details/:tenantid/:fee_id", {
		templateUrl: context + '/feepayment/misc-fee-details',
		controller: 'MiscFeeDetailsCtrl'
	});
	$routeProvider.when("/misc-add", {
		templateUrl: context + '/feepayment/misc-add',
		controller: 'MiscFeeAddCtrl'
	});
	$routeProvider.when("/guest/gsd",{
		 templateUrl: context + '/feepayment/guest-stu-details',
		 controller: 'GuestDetailsCtrl'
	});

	 $routeProvider.when("/guest/mfs",{
		 templateUrl: context + '/feepayment/guest-misc-fee-select',
		 controller: 'MiscFeeSelCtrl'
	});
	$routeProvider.when("/userManagement",{
		templateUrl: context + '/feepayment/user-management',
		controller: 'userManagementCtrl'
	});
	$routeProvider.when('/usersUpload', {
		templateUrl: context + '/partials/userMgmtUpload',
		controller: 'UploadCtrl'
	});
	$routeProvider.when('/usersEdit', {
		templateUrl: context + '/partials/userMgmtEdit',
		controller: 'userEditCtrl'
	});

	$routeProvider.otherwise({
		redirectTo: '/home'
	});
	$httpProvider.defaults.timeout = 5000;
}]);

myApp.directive('myEnter', function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if (event.which === 13) {
				scope.$apply(function() {
					scope.$eval(attrs.myEnter);
				});

				event.preventDefault();
			}
		});
	};
});

myApp.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];
		if (angular.isArray(items)) {
			items.forEach(function(item) {
				var itemMatches = false;

				var keys = Object.keys(props);
				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
}); 
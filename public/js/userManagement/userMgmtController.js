'use strict';
/* Controllers */
var context = '/feepayment';
var role22 = '/' + angular.element("#role22").val().toLowerCase();
var myapp = angular.module('myApp.userControllers', []).
controller('userManagementCtrl', ['$rootScope', '$scope', '$http', '$location', '$window', '$route', '$compile','UserMgmtSvc',function($rootScope, $scope, $http, $location, $window, $route, $compile,UserMgmtSvc) {
	$rootScope.hideFirstTime = false;
	$scope.editable = false;
	$scope.role = 'Student';
	$scope.allSelected = false;
	$scope.toggleAll = function() {
		if (!$scope.allSelected) {
			$scope.users.fees.data.forEach(function(student, index) {
				$scope.formData.checkboxes[index] = true;
			});
		} else {
			$scope.formData.checkboxes = [];
		}
	}

	$scope.headers = [
		{
			name: 'Email',
			search: '',
			mappingKey: 'email'
		},
		{
			name: 'Mobile Number',
			search: '',
			mappingKey: 'phone'
		},
		{
			name: 'Roll Number',
			search: '',
			mappingKey: 'unique_id'
		},
		{
			name: 'First Name',
			search: '',
			mappingKey: 'firstname'
		},
		{
			name: 'Last Name',
			search: '',
			mappingKey: 'lastname'
		},
		{
			name: 'Role',
			search: '',
			mappingKey: 'role'
		},
		{
			name: 'Course',
			search: '',
			mappingKey: 'course'
		}
	];

	$scope.validationProperties = [
		{
			name: 'Email',
			value: true,
			mappingKey: 'email'
		},
		{
			name: 'Mobile Number',
			value: true,
			mappingKey: 'phone'
		},
		{
			name: 'Roll Number',
			value: true,
			mappingKey: 'unique_id'
		},
	];

	$http.get(context + '/tenant/api/gettenant').success(function(data) {
		$rootScope.tenant = data;
	}).error(function(data) {
		alert("error");
	});


	$scope.users = UserMgmtSvc;

	$scope.nextPage = function() {

		$scope.users.studentFeeNext({tenant:tenant}).then(function(res){

		},function(err){

		});
	
	};


	$scope.formData = {
		checkboxes: {}
	};
	var JsonObj = $scope.formData.checkboxes;

	$scope.editData = function() {
		var JsonObj = $scope.formData.checkboxes;
		var editData = [];
		for (var key in JsonObj) {
			if (JsonObj[key]) {
				editData.push(key);
			}
		}
		if (editData.length == 0) {
			alert("Please select a single user for editing");
		} else if (editData.length == 1) {
			$rootScope.editData = $scope.users.fees.data[editData];
			$location.path("/usersEdit");
			$scope.editable = true;
			//$('#myModal').modal('show');
			//$('#\\#myModal').modal('show');
		} else {
			alert("Please select a single user for editing");
		}
	};
	$scope.deleteData = function() {
		var JsonObj = $scope.formData.checkboxes;
		//alert(JSON.stringify(JsonObj));
		var deleteData = {
			users: []
		};
		for (var key in JsonObj) {
			if (JsonObj[key]) {
				deleteData.users.push($scope.users.fees.data[key]._id);
			}
		}
		// alert("JSON : "+JSON.stringify(deleteData));
		if (deleteData.length == 0) {
			alert("Select atleast one user to proceed");
			return false;
		} else {
			$http.post(context + "/tenant/deleteUser", deleteData).success(function(result) {
				document.location.reload();
			}).error(function(data) {
				console.log("Error " + data);
			});
		}
	};
	$scope.reupload = function() {
		//var r = confirm("Your old data will be removed. Do you want to proceed?");
		//if (r == true) {
		$location.path("/usersUpload");
		//} else {}
	}
	$scope.syncToIDP = function(){
				
		$http.get(context + '/tenant/api/syncToIDP').success(function(data) {
			//alert("data " + data);
		}).error(function(data) {
			console.log(data);
		});
		
	}
	
	$scope.exportExcel = function() {
		//$scope.stuFeeSvc.fees.reset();

		var a = document.createElement('a');
		a.href = context + '/tenant/fetchUserList?download=true&q=' + UserMgmtSvc.fees.buildSearch();
		console.log('url=>',a.href);
		a.download = 'student_fee.xlsx';
		document.body.appendChild(a);
		
		a.click();
		a.remove();
	};
	$scope.addNewEntry = function() {
		var invalidData = false;
		for (var i in UserMgmtSvc.validationProperties) {
			if ($scope[UserMgmtSvc.validationProperties[i]] == "" || $scope[UserMgmtSvc.validationProperties[i]] == null) {
				invalidData = true;
				break;
			}
		}
		if (invalidData) {
			alert(UserMgmtSvc.validationProperties.toString(', ') + " are mandatory");
		} else {
			var users = {
				role: $scope.role,
				email: $scope.email,
				unique_id: $scope.unique_id,
				phone: $scope.phone,
				firstname: $scope.firstname,
				lastname: $scope.lastname,
				course: $scope.course
			};

			$http.post(context + "/tenant/addNewUser", users).success(function(result) {
				angular.element('.modal-backdrop').hide();
				$scope.email = $scope.course = $scope.unique_id = $scope.phone = $scope.firstname = $scope.lastname = '';
				$scope.role = 'Student';
				swal({
					title: 'Good Job!',
					text: "User added successfully",
					type: 'success',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'Ok'
				  }).then((result) => {
					document.location.reload();
				  })
			}).error(function(data) {
				console.log("Error " + data);
			});
		}
	};
	$scope.updateValidationProperties = function(){
		var validationProperties = [];
		$scope.validationProperties.forEach(function(property, index){
			if (property.value)
				validationProperties.push(property.mappingKey);
		});
		UserMgmtSvc.updateValidationProperties({validationProperties})
		.success(function(result){
			angular.element('#myAuthKeyModal').modal('hide');
			$scope.initValidationProperties();
		})
		.error(function(err){
			angular.element('#myAuthKeyModal').modal('hide');
			alert(err.message);
			$scope.initValidationProperties();
		});
	};
	$scope.initValidationProperties = function(){
		UserMgmtSvc.getValidationProperties()
			.success(function(result){
				$scope.validationProperties.forEach(function(property, index){
					if (UserMgmtSvc.validationProperties.indexOf(property.mappingKey) > -1)
						$scope.validationProperties[index].value = true;
					else 
						$scope.validationProperties[index].value = false;
				});
			}).error(function(err){
				alert(err.message);
			});
	};
	$scope.initValidationProperties();
	
}]).
controller('userEditCtrl', function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
	// write Ctrl here
	var editData = $rootScope.editData;
	$scope._id = editData._id;
	$scope.email = editData.email;
	$scope.firstname = editData.firstname;
	$scope.lastname = editData.lastname;
	$scope.role = editData.role;
	$scope.course = editData.course;
	$scope.unique_id = editData.unique_id;
	$scope.phone = editData.phone;
	$scope.saveEntry = function() {
		var dataEdited = {
			_id: $scope._id,
			role: $scope.role,
			email: $scope.email,
			unique_id: $scope.unique_id,
			phone: $scope.phone,
			firstname: $scope.firstname,
			lastname: $scope.lastname,
			course: $scope.course
		};
		$http.put(context + "/tenant/updateUser", dataEdited).success(function(result) {
			if (result.success) {
				swal({
					title: 'Good Job!',
					text: result.data,
					type: 'success',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'Ok'
				  }).then((result) => {
					window.location = window.location.protocol + window.location.host + '/feepayment/#/userManagement';
				  })
			}
		}).error(function(data) {
			swal({
				title: 'Failed!',
				text: data.message,
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Ok'
			  })
		});
	}
});
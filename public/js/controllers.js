'use strict';
/* Controllers */
var context = '/feepayment';
var role22 = '/' + angular.element("#role22").val().toLowerCase();

var myapp = angular.module('myApp.controllers', []).
controller('AppCtrl', function($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce) {
    try {
        console.log("App ctrl called");
        console.log($routeParams.appid + " : " + $routeParams.pageid);
        $rootScope.appid = $routeParams.appid;
        $rootScope.pageid = $routeParams.pageid;
        var pageDefinition = function(appDefinition, pageId) {
            var pageDef;
            var i;
            for (i = 0; i < appDefinition.pages.length; i++) {
                var tempPage = appDefinition.pages[i]
                if (tempPage.pageid == pageId) {
                    //alert (tempApp.name);
                    pageDef = tempPage;
                    break;
                }
            }
            return pageDef;
        };
        $scope.init = function(app, appid1, pageid1) {
            console.log("Init called : " + appid1 + " : " + pageid1);
            console.log(app.title);
            if (app.appdef) {
                setTimeout(function() {
                    console.log(app.appdef.name);
                    var pagedef = app.appdef.pages[0];
                    if (appid1 == app.appdef.name) {
                        console.log("FOUND : " + appid1);
                        pagedef = pageDefinition(app.appdef, pageid1);
                    }
                    var appid = app.appdef.name;
                    $("#widget" + appid).html("");
                    console.log("page Def : " + pagedef);
                    var updatedprocessor = pagedef.pageprocessor.split("#appContent").join("#widget" + appid);
                    window.eval(updatedprocessor);
                    var pageProcessorName = 'pageprocessor' + pagedef.pageid;
                    if (window[pageProcessorName] != undefined) {
                        window[pageProcessorName](pagedef, $scope, $routeParams, $compile, $http, $rootScope, $sce, $window, $location);
                        $scope.$apply();
                    }
                }, 100);
            }
        }
    } catch (ex) {
        console.log(ex);
    }
}).
controller('HomeCtrl', ['FeeService', '$rootScope', '$scope', '$http', '$location', '$window', '$route', '$compile','FeeSvc',function(feeService, $rootScope, $scope, $http, $location, $window, $route, $compile,feeSvc) {

    $rootScope.hideFirstTime = true;
    $scope.editable = false;
    $scope.allSelected = false;
    $scope.toggleAll = function() {
        var isAllSelected = $scope.allSelected;
        var checkboxes = $scope.formData.checkboxes;
        var studentdata
        var reqData = {}
        reqData.tenant = $rootScope.tenant.tenant;
        feeService.fetchAllStudentDetails(reqData)
            .then(function(stuData) {
                if (stuData) {
                    studentdata = stuData;

                    if (!isAllSelected) {
                        studentdata.forEach(function(student) {
                            var key = student.enrollment_no + ":" + student.semester;
                            checkboxes[key] = true;
                        });
                    } else {
                        angular.forEach(checkboxes, function(value, key) {
                            //alert(key,value);
                            delete checkboxes[key];
                        }); 
                    }
                } else {   
                    alert("PLEASE CONTACT YOUR COLLEGE ADMIN, IF YOU HAVE NOT PAID THE FEE!");
                }
        }, function(err) {
            alert("Error : " + err);
        });

        // var studentdata = $rootScope.studentdata;
        //alert(isAllSelected);
        
    }

    $http.get(context + '/tenant/api/gettenant').success(function(data) {
        $rootScope.tenant = data;
    }).error(function(data) {
        alert("error");
    });


    $scope.stuFeeSvc = feeSvc;

   /* if($scope.stuFeeSvc.fees){
        $scope.stuFeeSvc.fees.reset();
    }
*/
    $scope.nextPage = function() {

        $scope.stuFeeSvc.studentFeeNext({tenant:tenant}).then(function(res){

        },function(err){

        });
    
    };


    $scope.formData = {
        checkboxes: {}
    };
    var JsonObj = $scope.formData.checkboxes;

    $scope.editData = function() {
        var JsonObj = $scope.formData.checkboxes;
        var editData = [];
        for (var key in JsonObj) {
            if (JsonObj[key]) {
                editData.push(key);
            }
        }
        if (editData.length == 0) {
            alert("Please select a single user for editing");
        } else if (editData.length == 1) {
            $rootScope.editId = editData[0];
            $location.path("/edit");
            $scope.editable = true;
            //$('#myModal').modal('show');
            //$('#\\#myModal').modal('show');
        } else {
            alert("Please select a single user for editing");
        }
    };
    $scope.deleteData = function() {
        var JsonObj = $scope.formData.checkboxes;
        //alert(JSON.stringify(JsonObj));
        var deleteData = [];
        for (var key in JsonObj) {
            if (JsonObj[key]) {
                deleteData.push(key);
            }
        }
        // alert("JSON : "+JSON.stringify(deleteData));
        if (deleteData.length == 0) {
            alert("Select atleast one user to proceed");
            return false;
        } else {
            $http.post(context + "/tenant/api/deletedata", deleteData).success(function(result) {
                if (result.message == "success") {
                    $route.reload();
                }
            }).error(function(data) {
                console.log("Error " + data);
            });
        }
    };
    $scope.reupload = function() {
        //var r = confirm("Your old data will be removed. Do you want to proceed?");
        //if (r == true) {
        $location.path("/upload");
        //} else {}
    }
    $scope.exportExcel = function() {
        //$scope.stuFeeSvc.fees.reset();

        var a = document.createElement('a');
        a.href = context + '/tenant/regfees/download?q='+$scope.stuFeeSvc.fees.buildSearch();
        a.download = 'student_fee.xls';
        document.body.appendChild(a);
        
        a.click();
        a.remove();
    };
    $scope.addNewEntry = function() {
        if ($scope.enrollment == "" || $scope.enrollment == null || $scope.name == "" || $scope.name == null || $scope.course == "" || $scope.course == null || $scope.year == "" || $scope.year == null || $scope.email == "" || $scope.email == null || $scope.mobile == "" || $scope.mobile == null || $scope.feeFor == "" || $scope.feeFor == null || $scope.rollNumber == "" || $scope.rollNumber == null || $scope.admissionFee == "" || $scope.admissionFee == null || $scope.tutionFee == "" || $scope.tutionFee == null || $scope.hostelFee == "" || $scope.hostelFee == null || $scope.messFee == "" || $scope.messFee == null || $scope.labFee == "" || $scope.labFee == null || $scope.lateFee == "" || $scope.lateFee == null || $scope.otherFee == "" || $scope.otherFee == null || $scope.others == "" || $scope.others == null) {
            alert("All fields are mandatory");
        } else {
            var newEnrty = {
                "mobile": $scope.mobile,
                "status": $scope.status,
                "enrollment_no": $scope.enrollment,
                "name": $scope.name,
                "course": $scope.course,
                "year": $scope.year,
                "semester": $scope.semester,
                "email": $scope.email,
                "fee_for": $scope.feeFor,
                "roll_no": $scope.rollNumber,
                "admission_fee": $scope.admissionFee,
                "tuition_fee": $scope.tutionFee,
                "hostel_fee": $scope.hostelFee,
                "mess_fee": $scope.messFee,
                "late_fee": $scope.lateFee,
                "lab_fee": $scope.labFee,
                "other_fee": $scope.otherFee,
                "others": $scope.others,
                "stream": $scope.stream
            };

            $http.post(context + "/tenant/api/adddata", newEnrty).success(function(result) {
                if (result.message == "success") {
                    //angular.element('#myModal').modal('hide');
                    alert("Added successfully");
                    $scope.status = "";
                    $scope.enrollment = "";
                    $scope.name = "";
                    $scope.course = "";
                    $scope.year = "";
                    $scope.semester = "";
                    $scope.email = "";
                    $scope.mobile = "";
                    $scope.feeFor = "";
                    $scope.rollNumber = "";
                    $scope.admissionFee = "";
                    $scope.tutionFee = "";
                    $scope.hostelFee = "";
                    $scope.messFee = "";
                    $scope.labFee = "";
                    $scope.lateFee = "";
                    $scope.otherFee = "";
                    $scope.others = "";
                    $scope.stream = "";
                    //$route.reload();
                }
            }).error(function(data) {
                console.log("Error " + data);
            });
        }
    };
}]).
controller('EditCtrl', function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    var editId = "";
    editId = $rootScope.editId;
    var obj = editId.split(":");
    $http.post(context + "/tenant/api/editdata", {
        "enrollment_no": obj[0],
        semester: obj[1]
    }).success(function(result) {
        $scope.editenrollment = result[0].enrollment_no;
        $scope.editstatus = result[0].status;
        $scope.editname = result[0].name;
        $scope.editcourse = result[0].course;
        $scope.edityear = result[0].year;
        $scope.editsemester = result[0].semester;
        $scope.editemail = result[0].email;
        $scope.editmobile = result[0].mobile;
        $scope.editfeeFor = result[0].fee_for;
        $scope.editrollNumber = result[0].roll_no;
        $scope.editadmissionFee = result[0].admission_fee;
        $scope.edittutionFee = result[0].tuition_fee;
        $scope.edithostelFee = result[0].hostel_fee;
        $scope.editmessFee = result[0].mess_fee;
        $scope.editlabFee = result[0].lab_fee;
        $scope.editlateFee = result[0].late_fee;
        $scope.editotherFee = result[0].other_fee;
        $scope.editothers = result[0].others;
        $scope.editstream = result[0].stream;
        if ($scope.editstatus == "Paid" || $scope.editstatus == "paid") {
            $scope.statusPaid = true;
            $scope.statusNotPaid = false;
        } else {
            $scope.statusNotPaid = true;
            $scope.statusPaid = false;
        }
        $scope.$apply();
    }).error(function(data) {
        alert("Error on edit data api");
    });
    $scope.saveEntry = function() {
        var dataEdited = {
            "mobile": $scope.editmobile,
            "status": $scope.editstatus,
            "enrollment_no": $scope.editenrollment,
            "name": $scope.editname,
            "course": $scope.editcourse,
            "year": $scope.edityear,
            "semester": $scope.editsemester,
            "email": $scope.editemail,
            "fee_for": $scope.editfeeFor,
            "roll_no": $scope.editrollNumber,
            "admission_fee": $scope.editadmissionFee,
            "tuition_fee": $scope.edittutionFee,
            "hostel_fee": $scope.edithostelFee,
            "mess_fee": $scope.editmessFee,
            "late_fee": $scope.editlateFee,
            "lab_fee": $scope.editlabFee,
            "other_fee": $scope.editotherFee,
            "others": $scope.editothers,
            "stream": $scope.editstream
        };
        $http.post(context + "/tenant/api/saveEntry", dataEdited).success(function(result) {
            if (result.message == "success") {
                $location.path("/home");
                $route.reload();
            }
        }).error(function(data) {
            alert("Error on edit data api");
        });
    }
}).
controller('AdminCtrl', function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    console.log("AdminCtrl called compile");
    $scope.selectApp = function() {
        alert("called  " + $scope.seletedapp);
        $http.get(context + "/api/kryptos/livedata/" + $scope.seletedapp).success(function(data) {
            console.log(data);
            $scope.selectedappdata = data;
        }).error(function(data) {
            console.log("Error " + data);
        });
    }
    $http.get(context + "/tenant/api/kryptos/listMyApps").success(function(data) {
        console.log(data);
        $scope.myapps = data;
    }).error(function(data) {
        console.log("Error " + data);
    });
});
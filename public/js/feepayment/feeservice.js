var feeModule = angular.module("FeeModule", []);

var context = "/feepayment";
var role = angular.element("#role22").val().toLowerCase();
var tenant = angular.element("#tenant").val();
var role22 = '/' + role;
var URLPrefix = context + role22;


feeModule.factory("FeeSearch", [function() {

	var FeeSearch = function(feeObj) {
		feeObj = feeObj || {};
		var searchQuery = this;
		searchQuery.feeType = feeObj.feeType || "reqular";
		searchQuery.startDate = feeObj.startDate || '';
		searchQuery.endDate = feeObj.endDate || '';
		searchQuery.email = feeObj.email || '';
		searchQuery.roll_no = feeObj.roll_no || '';
		searchQuery.mobile = feeObj.mobile || '';
		searchQuery.fee_status = feeObj.fee_status || '';
		searchQuery.name = feeObj.name || '';
		searchQuery.father_name = feeObj.father_name || '';
		searchQuery.course = feeObj.course || '';
		searchQuery.fee_for = feeObj.fee_for || '';
		searchQuery.fee_id = feeObj.fee_id || '';
		searchQuery.txnid = feeObj.txnid || '';
		searchQuery.tenant = feeObj.tenant;
		searchQuery.steam = feeObj.stream || '';

		searchQuery.misc_fee_id = feeObj.misc_fee_id;

		// pass -1 to fetch all the results 

		searchQuery.pgNo = feeObj.pgNo || 1;
		searchQuery.pgSize = feeObj.pgSize || 10000;
		searchQuery.data = feeObj.data || [];
		searchQuery.hasMore = true;
		searchQuery.busy = false;

		searchQuery.reset = function() {
			searchQuery.pgNo = 1;
			searchQuery.pgSize = 10000;
			searchQuery.data = [];
			searchQuery.hasMore = true;
			searchQuery.busy = false;
		}

		searchQuery.deleteBlanks = function() {
			var clonedSearch = angular.copy(searchQuery);
			delete clonedSearch.data;
			Object.keys(clonedSearch).forEach(function(key){
				if(!clonedSearch[key] || clonedSearch[key]==''){
					delete clonedSearch[key];
				}
			});
			return clonedSearch;
		}

		searchQuery.buildSearch = function() {

			return encodeURIComponent(JSON.stringify(searchQuery.deleteBlanks()));
		};
	};

	return FeeSearch;
}]);

feeModule.service("FeeSvc", ["$http", "$filter", "$rootScope", "FeeSearch", function($http, $filter, $rootScope, FeeSearch) {


	var FeeSvc = function() {
		var that = this;


		that.studentFeeNext = (feeObj) => {
			if (!that.fees) {
				if (!feeObj || !feeObj.tenant) {
					throw new Error("Tenant is mandatory");
				}
				that.fees = new FeeSearch(feeObj);
			}

			return new Promise((resolve, reject) => {
				if (!that.fees.hasMore || that.fees.busy == true) {
					resolve(false);
				}
				that.fees.busy = true;
				$http.get(URLPrefix + "/regfees?q=" + that.fees.buildSearch()).success(function(data) {

					that.fees.data = that.fees.data.concat(data);

					if (data.length < that.fees.pgSize) {
						that.fees.hasMore = false;
					}
					that.fees.busy = false;
					that.fees.pgNo += 1;
					resolve(true);
				}).error(function(data) {
					that.fees.busy = false;
					reject("Something went wrong");
				});

			});

		};

		that.search = function() {

			that.fees.reset();
			that.studentFeeNext();
		}

		that.editFee = function(feeObj) {

			return new Promise((resolve, reject) => {
				$http.post(URLPrefix + "/api/saveEntry", feeObj).then(function(res) {
					if (res.message == "success")
						resolve(res);
					else
						reject(res);
				}, function(err) {
					reject(err);
				});
			});

		};

		that.deleteFee = function(feeObj) {
			return new Promise((resolve, reject) => {
				$http.post(URLPrefix + "/api/deletedata", feeObj).then(function(res) {
					if (res.message == "success") {
						resolve(res);
					} else {
						reject(res);
					}
				}, function(err) {
					reject(err);
				})
			});
		};
	};

	return new FeeSvc();
}]);

feeModule.service("MiscFeeSvc", ["$http", "$filter", "$rootScope", "FeeSearch", function($http, $filter, $rootScope, FeeSearch) {
	var MiscFeeSvc = function() {
		var that = this;

		that.miscFeeNext = (feeObj) => {
			if (!that.fees) {
				that.fees = new FeeSearch(feeObj);
			}

			//var tenant = that.fees.tenant || $rootScope.tenant.tenant;

			return new Promise((resolve, reject) => {
				if (!that.fees.hasMore || that.fees.busy == true) {
					return resolve(false);
				}
				that.fees.busy = true;
				$http.get(URLPrefix + "/miscfees?&q=" + that.fees.buildSearch()).success(function(data) {

					that.fees.data = that.fees.data.concat(data);

					that.fees.pgNo += 1;
					if (data.length < that.fees.pgSize) {
						that.fees.hasMore = false;
					}
					that.fees.busy = false;

					resolve(true);
				}).error(function(data) {
					that.fees.busy = false;
					reject("Something went wrong");
				});

			});

		};

		that.editMiscFee = function(miscFeeObj) {

			return new Promise((resolve, reject) => {
				$http.post(URLPrefix + "/miscfees/update", miscFeeObj).then(function(res) {
					resolve(res);
				}, function(err) {
					reject(err);
				})
			});
		};
		that.exportMisFeesDetails = function(miscFeeObj) {
			//var feeSearch = new FeeSearch(miscFeeObj);
			if (!that.miscFeeDetailsS) {
				that.miscFeeDetailsS = new FeeSearch(miscFeeObj);
			}

			var url = URLPrefix + "/miscfees/details/download?q=" + that.miscFeeDetailsS.buildSearch();

			var a = document.createElement('a');
			a.href = url;
			a.download = 'fee_' + miscFeeObj.fee_id + '.xls';
			document.body.appendChild(a);
			console.log("clicked");
			a.click();
			a.remove();
		};

		that.deleteMiscFee = function(miscFeeObj) {
			return new Promise((resolve, reject) => {
				$http.post(URLPrefix + "/miscfees/delete", miscFeeObj).then(function(res) {
					resolve(res);
				}, function(err) {
					reject(err);
				});
			});

		};

		that.miscFeeDetailsSearch = function(miscFeeObj) {
			if (!that.miscFeeDetailsS) {
				that.miscFeeDetailsS = new FeeSearch(miscFeeObj);
			}

			return new Promise((resolve, reject) => {
				if (!that.miscFeeDetailsS.hasMore || that.miscFeeDetailsS.busy == true) {
					return resolve(false);
				}
				that.miscFeeDetailsS.busy = true;
				$http.get(URLPrefix + "/miscfees/details?q=" + that.miscFeeDetailsS.buildSearch()).success(function(data) {

					that.miscFeeDetailsS.data = that.miscFeeDetailsS.data.concat(data);
					that.miscFeeDetailsS.pgNo += 1;

					if (data.length < that.miscFeeDetailsS.pgSize) {
						that.miscFeeDetailsS.hasMore = false;
					}
					that.miscFeeDetailsS.busy = false;
					resolve(true);
				}).error(function(data) {
					that.miscFeeDetailsS.busy = false;
					reject("Something went wrong");
				});

			});

		};

		that.searchFeeDetails = function(miscFeeObj) {
			that.miscFeeDetailsS.reset();
			that.miscFeeDetailsSearch() 	
		}
	};



	return new MiscFeeSvc();
}]);
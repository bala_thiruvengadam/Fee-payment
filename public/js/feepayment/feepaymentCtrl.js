var feeMod = angular.module('feeApp', []);
feeMod.constant('ctx', '/feepayment');
var role = angular.element("#role22").val().toLowerCase();
var role22 = '/' + role;
feeMod.controller('FeeCtrl', ['FeeService', '$scope', "$routeParams",'$rootScope', '$location', '$http', '$route', function(feeService, $scope, $routeParams, $rootScope, $location, $http, $route) {
    var feectrl = this;
    var tenant = $routeParams.t
    $scope.fetchTenantDetails = function(tenant){
		$http.get(context+'/details/t?tenant='+tenant).then(function(res){
			$rootScope.td = res.data;
			
			$rootScope.homepageCSS = $sce.trustAsHtml($rootScope.td.headerCSS);
			if(!$rootScope.td)
				throw new Error("Account not found");
		},function(err){
			console.log(err);
		});
	};
	$scope.fetchTenantDetails(tenant);
    
    $http.get(context + role22 + '/api/gettenant').success(function(data) {
        $rootScope.tenant = data;
    }).error(function(data) {
        alert("error");
    });

    $http.get(context + role22 + '/api/getcurrentUser').success(function(data) {
        $rootScope.user = data;
    }).error(function(data) {
        alert("error");
    });

    /*  if(!$scope.stuForm.$valid){
          alert("All fields are mandatory.");
      }*/
    feectrl.fetchStudent = {};
    feectrl.studentData = $rootScope.studentData;

    feectrl.fetchStudentDetails = function() {
        
        if (!$scope.stuForm.$valid) {
            return;
        }
        if (feectrl.fetchStudent.started == true) {
            return;
        }
        feectrl.fetchStudent.started = true;
        $rootScope.studentData = {};

        var reqData = feectrl.student;
        reqData.tenant = $rootScope.tenant.tenant ||  $routeParams.t;
        feeService.fetchStudentDetails(reqData).then(function(stuData) {
            feectrl.fetchStudent.started = false;
            feectrl.studentData = stuData;

            if (stuData) {
                $rootScope.studentData = stuData;
                //alert("dfdd"+JSON.stringify($rootScope.studentData));
                $location.path("/feedetails");
                $route.reload();
                
            } else {
                alert('PLEASE CONTACT YOUR COLLEGE ADMIN, IF YOU HAVE NOT PAID THE FEE!');
            }
        }, function(err) {
            feectrl.fetchStudent.started = false;
            alert("Error : " + err);
        });
    };

}]);

feeMod.controller('FeeDetailsCtrl', ['FeeService', '$scope', '$routeParams', '$rootScope', '$location', function(feeService, $scope, $routeParams, $rootScope, $location) {
    var feectrl = this;
    feectrl.fetchStudent = {};
    feectrl.studentData = $rootScope.studentData;

    feectrl.fetchStudentDetails = function() {
        if (feectrl.fetchStudent.started == true) {
            return;
        }
        feectrl.fetchStudent.started = true;
        //$rootScope.studentData = {};

        var reqData = {
            enrollment_no: $rootScope.studentData.enrollment_no,
            semester: $rootScope.studentData.semester
        };
        reqData.tenant = $rootScope.tenant.tenant || $routeParams.t;
        feeService.fetchStudentDetails(reqData).then(function(stuData) {
            feectrl.fetchStudent.started = false;
            feectrl.studentData = stuData;
            if (stuData) {
                $rootScope.studentData = stuData;
                //$location.path("/feedetails");
            }
        }, function(err) {
            feectrl.fetchStudent.started = false;
            alert("Error : " + err);
        });
    };
    feectrl.paymethod = function() {
        $location.path('/paymethod');
    };

    feectrl.totalFeePayable = function(studentData) {
        var totalFee = 0;
        totalFee = parseFloat(studentData.admission_fee || 0) + parseFloat(studentData.hostel_fee || 0) + parseFloat(studentData.tuition_fee || 0) + parseFloat(studentData.mess_fee || 0) + parseFloat(studentData.lab_fee || 0) + parseFloat(studentData.late_fee || 0) + parseFloat(studentData.other_fee || 0) + parseFloat(studentData.others || 0);
        $rootScope.totalFee = totalFee;
        return totalFee;
    };
    feectrl.fetchStudentDetails();
}]);
feeMod.controller('PayCtrl', ['FeeService', '$scope', '$routeParams', '$rootScope', '$location', '$window', 'ctx', function(feeService, $scope, $routeParams, $rootScope, $location, $window, ctx) {
    feectrl = this;
    var launchInApp = function(txnid) {
        var url = ctx + '/initpay?trnxid=' + txnid;
        console.log("launchInApp====feepayment===" +txnid)
        $window.location.href = url;
    };
    feectrl.initPay = function() {
        var stuData = $rootScope.studentData;
        var data = {
            amount: $rootScope.totalFee,
            firstname: stuData.name,
            email: stuData.email,
            phone: stuData.mobile,
            en_no: stuData.enrollment_no,
            semester: stuData.semester,
            tenant: $rootScope.tenant.tenant || $routeParams.t,
            description: "Fee Payment ==> " + stuData.enrollment_no,
        };
        if($routeParams.t)
            data.member = 'guest'
        if ($rootScope.user) {
            data.action_user = {
                id: $rootScope.user.user_id,
                email: $rootScope.user.username,
                name: $rootScope.user.name,
                college: $rootScope.user.college,
                roll: $rootScope.user.roll_no
            };
        }
        feectrl.initTxn = {};
        if (feectrl.initTxn.started == true)
            return;
        feectrl.initTxn.started = true;
        feeService.initTxn(data).then(function(data) {
            //var resData = res.data;
            $rootScope.trnxid = data.trnxid;
            $rootScope.created_on = data.created_on;
            feectrl.initTxn.started == false;
            launchInApp(data.trnxid);
        }, function(err) {
            feectrl.initTxn.started == false;
            alert(error)

        });
    };
}]);

feeMod.controller("transHistory", ["$rootScope", "$location", "$scope", "$http", "ctx", "$routeParams", function($rootScope, $location, $scope, $http, ctx, $routeParams) {
    $http.get(context + role22 + '/api/getcurrentUser').success(function(data) {
        $rootScope.user = data;

        var url = ctx + "/txns?user_id=" + $rootScope.user.userid;
        $http.get(url).then(function(res) {
            var data = res.data;
            if (data.status == "success") {
                $scope.txns = data.data;
            }
        }, function(err) {

        });
    }).error(function(data) {
        alert("error");
    });

    $scope.txnMenu = function(txn, $event) {
        /* var $txnMenu = angular.element('#txnMenu');
         $txnMenu.find('a.download:first').attr('href',"/feepayment/invoice?txnid="+txn.trnxid).attr('download',trnxid+'.pdf'); 
         $txnMenu.find('a.file:first').attr('href',"/feepayment/invoice?txnid="+txn.trnxid);*/
        //TODO share
        //  $txnMenu.
        //alert("togglling"+ JSON.stringify($event.target));
        /*var targettop =*/
        $($event.target).find(".txn-menu").toggleClass("hide");

        /*var targetleft = $($event.target).position().left;
        $txnMenu.css({top:(targettop+15)+'px',left:(targetleft-100)+'px'});*/

    };

    $scope.openView=function($event){
        $event.stopPropagation();
        //$($event.currentTarget).trigger("click");
    };

   

    $scope.goHome = function() {
        $location.path("/student");
    };
}]);

feeMod.controller("transFailure", ["$rootScope", "$location", "$scope", "$http", "ctx", "$routeParams", function($rootScope, $location, $scope, $http, ctx, $routeParams) {
    $http.get(context + role22 + '/api/getcurrentUser').success(function(data) {
        $rootScope.user = data;

        var url = ctx + "/txns?user_id=" + $rootScope.user.userid;
        $http.get(url).then(function(res) {
            var data = res.data;
            if (data.status == "success") {
                $scope.txns = data.data;
            }
        }, function(err) {

        });
    }).error(function(data) {
        alert("error");
    });

   
    $scope.fetchTransaction = function() {
        var txnid = $routeParams.txnid;
        var url = ctx + "/txn?txnid=" + txnid;
        $http.get(url).then(function(res) {
            var data = res.data;
            if (data.status == "success") {
                $scope.txn = data.data;
            }
        }, function(err) {
            console.log("Error : ", err);
        });
    };
    $scope.fetchTransaction();

    $scope.goHome = function() {
        $location.path("/student");
    };
}]);
feeMod.controller("TransSummary", ["$rootScope", "$location", "$scope", "$http", "ctx", "$routeParams","$filter", function($rootScope, $location, $scope, $http, ctx, $routeParams,$filter) {
    $http.get(context + role22 + '/api/getcurrentUser').success(function(data) {
        var tenant = $rootScope.tenantName ;
        $scope.valid = false;
        var tenantsToDownloadForm =  [{name : "KMGGPGC"},{name : "NGIE"},{name : "JDC"},
        {name : "DNC"},{name : "DAV"},{name : "IDC"},{name : "GDC"}]

        if($filter("filter")(tenantsToDownloadForm, {name:tenant}).length > 0)
            $scope.valid = true 
        else
            $scope.valid = false

            console.log('valid:: ', $scope.valid)

        $rootScope.user = data;
    }).error(function(data) {   
        alert("error");
    });



    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return (false);
    }

        
    $scope.goHome = function() {
        $location.path("/student");
    };

    $scope.downloadInvoice = function() {
        var url = ctx + "/invoice?txnid=" + $scope.txn.trnxid;
        var a = document.createElement('a');
        a.href = url;
        a.download = $scope.txn.trnxid + ".pdf";
        document.body.appendChild(a);
        console.log("clicked");
        a.click();
        a.remove();
    }

    $scope.downloadForm = function() {
        var url = ctx + "/userDetails?txnid=" + $scope.txn.trnxid;
        var a = document.createElement('a');
        a.href = url;
        a.download = $scope.txn.trnxid + ".pdf";
        document.body.appendChild(a);
        console.log("clicked");
        a.click();
        a.remove();
    }
    
    $scope.transHistory = function() {
        $location.path("/txns");
    }

    $scope.fetchTransaction = function() {
        var txnid = $routeParams.txnid;
        var url = ctx + "/txn?txnid=" + txnid;
        $http.get(url).then(function(res) {
            var data = res.data;
            $rootScope.tenantName = res.data.data.tenant
            if (data.status == "success") {
                $scope.txn = data.data;
                $scope.fetchTenantDetails(txn.tenant);
            }
        }, function(err) {
            console.log("Error : ", err);
        });
    };
    $scope.fetchTransaction();

    $scope.fetchTenantDetails = function(tenant){
        $http.get(context+'/details/t?tenant='+tenant).then(function(res){
            $rootScope.td = res.data;
            if(!$rootScope.td)
                throw new Error("Account not found");
        },function(err){
            console.log(err);
        });
    };
}]);

feeMod.service('FeeService', ['$window', '$http', '$q', 'ctx', function($window, $http, $q, ctx) {
    var feeService = function() {
        this.fetchStudentDetails = function(reqData) {
            return new Promise((resolve,reject)=>{
                $http({
                    url: ctx + '/api/student',
                    params: reqData,
                    method: 'GET'
                }).then(function(res) {
                    
                    var resData = res.data;
                    if (resData.status == 'success') {
                        resolve(resData.data);
                    } else {
                        reject(resData.data);
                    }
                }, function(err) {
                   reject(err);                    
                });  
            })
           
        };

        this.initTxn = function(reqData) {
            var txnDataPromise = $q.defer();
            $http({
                url: ctx + '/ctrnx',
                data: reqData,
                method: 'POST'
            }).then(function(res) {
            	var resData = res.data;
                if (resData.status == 'success') {
                    txnDataPromise.resolve(resData.data);
                } else {
                    txnDataPromise.reject(resData.data);
                }
            }, function(err) {
                txnDataPromise.reject(err);
            });
            return txnDataPromise.promise;
        };

        this.fetchAllStudentDetails = function(reqData) {
            var studentDetailsPromise = $q.defer();
            $http({
                url: ctx + '/tenant/api/studentdata',
                params: reqData,
                method: 'GET'
            }).then(function(res) {
                var resData = res.data;
                studentDetailsPromise.resolve(resData);
            }, function(err) {
                studentDetailsPromise.reject(err);
            });   
            return studentDetailsPromise.promise;    
        };
    };

    this.txnsHistory = function() {

    };
    this.txnMenu = function(txn, $event) {
        /* var $txnMenu = angular.element('#txnMenu');
         $txnMenu.find('a.download:first').attr('href',"/feepayment/invoice?txnid="+txn.trnxid).attr('download',trnxid+'.pdf'); 
         $txnMenu.find('a.file:first').attr('href',"/feepayment/invoice?txnid="+txn.trnxid);*/
        //TODO share
        //  $txnMenu.
        alert("togglling");
        /*var targettop =*/
        $($event.target).find(".txn-menu").toggleClass("hide");

        /*var targetleft = $($event.target).position().left;
        $txnMenu.css({top:(targettop+15)+'px',left:(targetleft-100)+'px'});*/

    };
    return new feeService();
}]);
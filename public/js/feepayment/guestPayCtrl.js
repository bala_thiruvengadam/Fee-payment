var feeControllers = angular.module("guestPayControllers", ['FeeModule']);
var context = "/feepayment";
var tenant = angular.element("#tenant").val().toLowerCase();


feeControllers.controller("GuestDetailsCtrl", ["$scope", "$routeParams", "$compile", "$http", "$rootScope", "$window", "$location", "$sce", "MiscFeeSvc","$filter", function($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce, miscFeeSvc,$filter) {

	var tenants =  [{name : "DCL"},{name: "JVJAIN"},{name : "MECW" },{name: "GDC"},{name: "KPGC"},
					{name : "GWPGC"},{name : "GMV"},{name: "ASPG"},{name : "MLJNKGC"},{name: "SDC"},{name : "MMC"},{name : "SSVC"},
					{name : "RKPG"} ,{name: "JDC"},{name: "MMDC"},{name : "GACWAV"},{name: "KLSSMV"},{name: "GCCV"},{name: "DAV"},
					{name: "DNC"},{name: "DPBS"},{name: "CHANAKYA"},{name: "SDPGC"},{name: "IDC"},{name: "INMANTEC"},{name: "NREC"},
					{name : "AKPG"},{name: "JSS"},{name: "SSDC"},{name : "HABIBI"},{name: "GCB"},{name : "IAMRGZB"},
					{name : "ITMM"},{name: "SBMT"},{name: "SVBIT"},{name: "SDCDE"},{name : "NGIE"},{name : "SEEMA"},
					{name : "GVN"},{name : "KNIT"},{name : "SVBIT"},{name : "SMMS"},{name : "CSSSPG"},
					{name : "KMGGPGC"},{name : "DPGITM"},{name : "LaxmiBai"},{name : "SIET"}]
	
	if($location.absUrl().search("gadm_laxmibai") > -1){
		$window.location.href = '/feepayment/guest/#/gmf?t=LaxmiBai'
	}	
	var tenant = $routeParams.t;
	$rootScope.tenant = tenant;
	var url = $location.absUrl().split('?')[0]

	var validationReq = {}
	validationReq.tenant = tenant;
	validationReq.url = url;
	
	$scope.valid = false
	if($filter("filter")(tenants, {name:tenant}).length > 0)
		$scope.valid = true
	else
		$scope.valid = false
			
	if(url.search("/gmf") > -1 || url.search("/mmc") > -1 || url.search("/gdc") > -1 || 
	url.search("/laxmibai") > -1 || url.search("/gadm_laxmibai") > -1 )
		$rootScope.declaration = false
	else
		$rootScope.declaration = true
	
	$scope.fetchTenantDetails = function(tenant){
		$http.get(context+'/details/t?tenant='+tenant).then(function(res){
			$rootScope.td = res.data;
			
			$rootScope.homepageCSS = $sce.trustAsHtml($rootScope.td.headerCSS);
			if(!$rootScope.td)
				throw new Error("Account not found");
		},function(err){
			console.log(err);
		});
	};
	$scope.fetchTenantDetails(tenant);
	//console.log("GuestDetailsCtrl loaded");
	$scope.guestDetails={};

	$scope.getYearSelected = function(sItem){

		$scope.regReq = false
		$scope.meritListReq = false
		$scope.meritSlReq = false
		$scope.meritDateReq = false
		$scope.collegeReq = false

		if(sItem != null && sItem == 'I'){
			$scope.regReq = true
			$scope.meritListReq = true
			$scope.meritSlReq = true
			$scope.meritDateReq = true
			$scope.collegeReq = false
		}
		
		if(sItem != null && (sItem == 'II' || sItem == 'III')){
			$scope.regReq = false
			$scope.meritListReq = false
			$scope.meritSlReq = false
			$scope.meritDateReq = false
			$scope.collegeReq = true
		}
	}

	$scope.getSemesterVal = function(sItem){
		$scope.req1 = false
		$scope.req2 = false
		$scope.req3 = false
		$scope.req4 = false

		if(sItem != null && sItem === "III"){
			$scope.req1 = true
			$scope.req2 = true
		}else if(sItem != null && sItem === "V"){
			$scope.req1 = true
			$scope.req2 = true
			$scope.req3 = true
			$scope.req4 = true
		}

	}

	$scope.getClassSelected = function(sItem){

		$scope.baReq = false
		$scope.maReq = false
		$scope.bedReq = false
		if(sItem != null && (sItem.search("B.Sc") > -1 || sItem.search("B.Com") > -1 || sItem.search("B.A.") > -1
				|| sItem.search("L.L.B") > -1 )){
			$scope.baReq = true
		}
	
		if(sItem != null && sItem.search("B.Ed.") > -1 ){
			$scope.bedReq = true
		}		
		
		if(sItem != null && (sItem.search("M.Sc") > -1 || sItem.search("M.Com") > -1 || sItem.search("M.A.") > -1 
				|| sItem.search("M.Ed.") > -1 )){
			$scope.maReq = true
		}	
	}

	$scope.numValidate = function(evt){
		var theEvent = evt || window.event;
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode(key);
		var regex = /[0-9]|\./;
		if (!regex.test(key) && !(evt.keyCode >= 96 && evt.keyCode <= 105) && !((evt.keyCode == 8)  || 
		(evt.keyCode == 46) || (evt.keyCode == 9))) {
			theEvent.returnValue = false;
			if (theEvent.preventDefault) {
				theEvent.preventDefault();
			}
		}
	}

	$scope.alphaValidate = function(e){
		var key = e.keyCode;
			if (!((key == 8) || (key == 32) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || 
			(key >= 65 && key <= 90))) {
				e.preventDefault();
		}
	}
		
	$scope.next = function(isValid) {
		$scope.payThroughPayu = false
		$scope.payThroughMobikwik = false

		var mobiTenants =  [{name : "MECW"}]
		var tenant = $rootScope.tenant;

		if($filter("filter")(mobiTenants, {name:tenant}).length > 0){
			$rootScope.payThroughMobikwik = true;
			$rootScope.payThroughPayu = false;
		}
		else{
			$rootScope.payThroughPayu = true;
			$rootScope.payThroughMobikwik = false;
		
		}
			
		var chkselct1 = $scope.chkselct1
		var chkselct2 = $scope.chkselct2
		var formFor = $scope.guestdetails.form_for
		
		var declaration1 = false
		var declaration2 = $rootScope.declaration
		
		if(declaration2){
			if(formFor == "admission")
				declaration1 = true
		}

		if(!isValid){
			alert("Please fill all the required fields in the form marked with (*) correctly.")
			return
		}

		if (declaration2 && (chkselct2 == false || chkselct2 == undefined)){
			if(declaration1 && (chkselct1 == false || chkselct1 == undefined)){
				alert("Please confirm the declaration mentioned above & below in the form.")
			}else
				alert("Please confirm the declaration mentioned below in the form.")
			return
		}else if(declaration1 && (chkselct1 == false || chkselct1 == undefined)){
			alert("Please confirm the declaration mentioned above in the form.")
			return
		}

		$http.post	(context +"/validationList", validationReq).then(function(data){
			if(data.data == null || data.data == "" ){
				$rootScope.guestDetails = $scope.guestdetails;
				$location.path("/mfs");
			}
			console.log('data:: ', data.data);
			var validationFieldName = data.data.validationField
			var valList = data.data.validationList

			var fields = $scope.guestdetails
			if(fields[validationFieldName] === undefined){
				$rootScope.guestDetails = $scope.guestdetails;
				$location.path("/mfs");
			}else {
				isValid = false;
				for (i = 0; valList.length > i; i += 1) {
					if (valList[i].validationlist === fields[validationFieldName]) {
						isValid = true;
						break;
					}
				}

				if(!isValid){
					alert(data.data.message);
					return
				}else{
					$rootScope.guestDetails = $scope.guestdetails;
					$location.path("/mfs");
				}
			}	
		},function(err){
			console.log("Error  : "+ err);
		})
	}
}]);

feeControllers.controller("MiscFeeSelCtrl", ["$scope", "$routeParams", "$compile", "$http", "$rootScope", "$window", "$location", "$sce", 'MiscFeeSvc', function($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce, miscFeeSvc) {
	var tenant = $rootScope.tenant || $routeParams.t;
	
	$scope.fetchMiscFee = function() {
	
		$http.get(context + "/fees/miscfees?tenant="+tenant).then(function(res) {
			$scope.miscfees = res.data;

		}, function(err) {
			window.alert("something went wrong");
		});
	};
	$scope.fetchMiscFee();

	$scope.selectFee = function(){
		$scope.misc= $scope.miscfee;
		$scope.amount = $scope.misc.fees[0].fee_amount;
	}

	var launchInApp = function(txnid) {
		var url = context + '/initpay?trnxid=' + txnid;
		$window.location.href = url;
	};

	var launchMobikwikInApp = function(txnid,paymentMode) {
		var url = context + '/initMobiPay?trnxid=' + txnid +'&paymentMode='+paymentMode;
		$window.location.href = url;
	};

	$scope.totalFee = function() {
		var total = 0;
		$scope.misc.fees.forEach(function(fee) {
			total += fee.fee_amount;
		});
		return total;
	};
	var toDOBStr=function(dob){
		if(dob){
			return dob.mm+"/"+dob.dd+"/"+dob.yyyy;
		}
	}	
	
	$scope.paymentMode = function(paymentMode){
		$rootScope.paymentMode = paymentMode;
		$(".well").each(function() {
		    $(this).attr('class', 'well icon-box icon-box-unselected')
		    
		});
		$('#'+paymentMode).attr('class', 'well icon-box icon-box-selected')
	}

	var toMeritStr=function(merit){
		if(merit){
			return merit.mm+"/"+merit.dd+"/"+merit.yyyy;
		}
	}	

	var getSemester=function(semesterA,semesterB,courseCategory){
		if(courseCategory === '(B) Self Finance (Professional) courses')
			return semesterB
		else if(courseCategory === '(A) Govt. Aided(Traditional) courses')
			return semesterA
	}	

	$scope.pay = function() {
		
		var guestDetails = $rootScope.guestDetails;
		$scope.misc.fees[0].fee_amount= $scope.amount;
		guestDetails.fee = $scope.misc;
		guestDetails.tenant = tenant;
		guestDetails.dob = toDOBStr(guestDetails.dob);	
		if(guestDetails.date)
			guestDetails.date = toDOBStr(guestDetails.date);	
		guestDetails.meritDate = toMeritStr(guestDetails.meritDate);	
		guestDetails.fee_amount = $scope.totalFee();
		guestDetails.miscfee = stuForm.miscfee.selectedOptions[0].textContent

		if(tenant === 'MMC')
			guestDetails.semester = getSemester(guestDetails.semesterA,guestDetails.semesterB,guestDetails.courseCategory)

		$http.post(context +"/miscfees/save",guestDetails).then(function(data){
    	$scope.createTransaction(data.data);
		},function(err){
			alert("Error  : "+err);
		})
	};

	$scope.payByMobikwik = function() {
		if($rootScope.paymentMode == '' || $rootScope.paymentMode == undefined){
			alert('Please select a payment mode');
			return false;
		}
		var guestDetails = $rootScope.guestDetails;
        $scope.misc.fees[0].fee_amount= $scope.amount;
		guestDetails.fee = $scope.misc;
		guestDetails.tenant = tenant;
		guestDetails.dob=toDOBStr(guestDetails.dob);	
		guestDetails.fee_amount = $scope.totalFee();
		guestDetails.paymentMode = $rootScope.paymentMode;
		guestDetails.miscfee = stuForm.miscfee.selectedOptions[0].textContent
		console.log('guestDetails::: ',guestDetails)
		$http.post(context +"/miscfees/save",guestDetails).then(function(data){
			console.log('data::: ',data)
			$scope.createMobikwikTransaction(data.data);
		},function(err){
			alert("Error  : "+err);
		})
	};

	$scope.createMobikwikTransaction = function(stuFee) {
		var data = {
			fee_for: "misc",
			fee_id: stuFee.fee_id,
			firstname:stuFee.name,
			lastname:stuFee.lastname,
			amount:stuFee.amount,
			email:stuFee.email,
			year:stuFee.year,
			phone:stuFee.mobile,
			tenant:stuFee.tenant,
			enrollment_no : stuFee.enrollment_no,
			semester : stuFee.semester,
			course:stuFee.course,
			member:"guest",
			amount:stuFee.fee_amount,
			description:"payment for "+stuFee.fee.fees[0].fee_name,
			PG_name: 'mobikwik',
			paymentMode: $rootScope.paymentMode,
			fee_type: stuFee.miscfee,
			father_name:stuFee.father_name
		};
		$http.post(context + "/ctrnx", data).then(function(res,status) {
			//alert(JSON.stringify(res) + " status = "+status);
			var trns = res.data.data;
			
			launchMobikwikInApp(trns.trnxid,$rootScope.paymentMode);
		}, function(err) {
			console.log(err);
			alert("Error in initiating payment"+err);
		});


	};

	$scope.createTransaction = function(stuFee) {
		var data = {
			fee_for: "misc",
			fee_id: stuFee.fee_id,
			firstname:stuFee.name,
			father_name:stuFee.father_name,
			fee_type: stuFee.miscfee,
			uniqueId: stuFee.uniqueId,
			lastname:stuFee.lastname,
			amount:stuFee.amount,
			email:stuFee.email,
			year:stuFee.year,
			phone:stuFee.mobile,
			tenant:stuFee.tenant,
			enrollment_no : stuFee.enrollment_no || stuFee.en_no,
			semester : stuFee.semester,
			course:stuFee.course,
			member:"guest",
			amount:stuFee.fee_amount,
			description:"payment for "+stuFee.fee.fees[0].fee_name,
			PG_name: 'payU'
		};
		console.log('stuFee:: ',  data)
		$http.post(context + "/ctrnx", data).then(function(res,status) {
			//alert(JSON.stringify(res) + " status = "+status);
			var trns = res.data.data;
			launchInApp(trns.trnxid);
		}, function(err) {
			console.log(err);
			alert("Error in initiating payment"+err);
		});
	};

	//$scope.getTransactionDetails


}]);


var feeControllers = angular.module("feeControllers", ['FeeModule']);
var context = "/feepayment";
var role = angular.element("#role22").val().toLowerCase();
var role22 = '/' + role;

feeControllers.controller("MiscFeeDashCtrl", ["$scope", "$routeParams", "$compile", "$http", "$rootScope", "$window", "$location", "$sce", "MiscFeeSvc", function($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce, miscFeeSvc) {

	$scope.getTenant = function() {
		return new Promise((resolve, reject) => {
			if (!$rootScope.tenant) {
				$http.get(context + role22 + '/api/gettenant').success(function(data) {
					$rootScope.tenant = data;
					resolve(data);
				}).error(function(data) {
					reject(data);
				});
			} else {
				resolve($rootScope.tenant);
			}
		});
	}
	$scope.getCurrentUser = function() {
		return new Promise((resolve, reject) => {
			if (!$rootScope.user) {
				$http.get(context + role22 + '/api/getcurrentUser').success(function(data) {
					$rootScope.user = data;
					resolve(data);
				}).error(function(data) {
					reject(data);
				});
			} else {
				resolve($rootScope.user);
			}
		});
	}

	$scope.miscFee = {};


	Promise.all([$scope.getTenant(), $scope.getCurrentUser()]).then((values) => {
		$scope.getMiscFees();
	}).catch((err) => {
		console.log(err);
	})

	$scope.miscFeeSvc = miscFeeSvc;

	if ($scope.miscFeeSvc.fees) {
		$scope.miscFeeSvc.fees.reset()
	}
	$scope.getMiscFees = function() {
		$scope.miscFeeSvc.miscFeeNext({
			tenant: tenant,
			'fee_for': 'misc'
		});
	};

	$scope.miscFeeDetails = function(fee) {
		var tenant = fee.tenant;
		var fee_id = fee.fee_id;
		$location.path("/misc-fee-details/" + tenant + "/" + fee_id);

	};

	$scope.addMiscFee = function() {
		var data = $scope.miscFee;
		data.tenant = tenant;
		data.fee_for = "misc";

		$http.post(context + role22 + "/miscfees/save", data).then((data) => {
			$scope.miscFeeSvc.fees.data.push(data.data);
			$("#addMiscFee").modal("hide");
		}, (err) => {
			alert("Error" + JSON.stringify(err));
		});

	}

	$scope.addMiscFeeModal = function(miscFee) {
		if (!miscFee) {
			$scope.miscFee = {
				fees: [{}]
			};
		} else {
			$scope.miscFee = miscFee;
		}

		$("#addMiscFee").modal("show");
	}

	$scope.checkboxes = {};

	$scope.editMiscFeeModal = function() {
		var checkedCnt = 0;
		var _id;
		for (var key in $scope.checkboxes) {
			if ($scope.checkboxes[key] == true) {
				if (checkedCnt >= 1) {
					alert("You can edit one item at a time");
					return;
				}
				checkedCnt++;
				_id = key;
			}
		}
		if (checkedCnt == 0) {
			alert("No fee has been selected");
			return;
		}


		$scope.miscFeeSvc.fees.data.forEach(function(item) {
			if (item._id == _id) {
				$scope.miscFee = item;
				$("#editMiscFee").modal("show");
			}
		});
	}

	$scope.editMiscFee = function() {
		var data = $scope.miscFee;
		$http.post(context + role22 + "/miscfees/update", data).then(function(data) {
			// updated successfully 
			$("#editMiscFee").modal("hide");
		}, function(err) {
			alert(err);
			$("#editMiscFee").modal("show");
		})
	}

	$scope.deleteMiscFeeModal = function() {
		var checkedCnt = 0;
		var _id;
		for (var key in $scope.checkboxes) {
			if ($scope.checkboxes[key] == true) {
				if (checkedCnt >= 1) {
					alert("You can edit one item at a time");
					return;
				}
				checkedCnt++;
				_id = key;
			}
		}
		if (checkedCnt == 0) {
			alert("No fee has been selected");
			return;
		}
		$scope.fee_to_delete = undefined;
		$scope.miscFeeSvc.fees.data.forEach(function(item) {
			if (item._id == _id) {
				$scope.fee_to_delete = item;
				$("#deleteMiscFee").modal("show");
			}
		});
	}


	$scope.deleteMiscFee = function() {
		var fee_to_delete = $scope.fee_to_delete;
		var data = {
			tenant: $rootScope.tenant.tenant,
			fee_id: fee_to_delete.fee_id
		};
		$http.post(context + role22 + "/miscfees/delete", data).then(function(res) {
			console.log("deleted");
			// remove from the list
			var miscFees = $scope.miscFeeSvc.fees.data;
			for (var i = 0; i < miscFees.length; i++) {
				if (miscFees[i].fee_id, fee_to_delete.fee_id) {
					miscFees.splice(i, 1);
					break;
				}
			}
			delete $scope.checkboxes[fee_to_delete._id];
			$("#deleteMiscFee").modal("hide");
		}, function(err) {
			alert(err);
			$("#deleteMiscFee").modal("hide");
			console.log("failed to delete");
		});
	}
}]);

feeControllers.controller("MiscFeeDetailsCtrl", ["$scope", "$routeParams", "$compile", "$http", "$rootScope", "$window", "$location", "$sce", 'MiscFeeSvc', function($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce, miscFeeSvc) {

	var fee_id = $routeParams.fee_id;
	var tenantid = $routeParams.tenantid;

	$scope.miscFeeSvc = miscFeeSvc;


	$scope.nextMiscFeeDetailsPage = function() {
		$scope.miscFeeSvc.miscFeeDetailsSearch({
			misc_fee_id: fee_id,
			tenant: tenant
		});
	};

	$scope.miscFeeDetailsDownload = function() {
		$scope.miscFeeSvc.exportMisFeesDetails({
			misc_fee_id: fee_id,
			tenant: tenant
		});
	};

	$scope.searchFeeDetails = function() {
		$scope.miscFeeSvc.searchFeeDetails({
			misc_fee_id: fee_id,
			tenant: tenant
		});
	}

}]);

feeControllers.controller("MiscFeeAddCtrl", ["$scope", "$routeParams", "$compile", "$http", "$rootScope", "$window", "$location", "$sce", function($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce) {
	$scope.miscFee = {};

	$scope.addMiscFee = function() {
		var data = $scope.miscFee;
		data.tenant = $rootScope.tenant.tenant;
		data.fee_for = "misc";

		$http.post(context + role22 + "/miscfees/save", data).then((data) => {
			$("#addMiscFee").modal("hide");
			$location.path("/misc-dash");
			$scope.$apply();
			// close modal
			// change location
		}, (err) => {
			// close the modal
		});

	};

	$scope.addMiscFeeModal = function() {
		$scope.miscFee = {
			fees: [{}]
		};
		$("#addMiscFee").modal("show");
	}
}]);